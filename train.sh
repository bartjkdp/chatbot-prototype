#!/bin/bash

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/*
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

# Load up .env
set -o allexport; source .env; set +o allexport 
# Forced retraining of Rasa NLU + Core
docker run \
  -v $(pwd)/training:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom \
  rasa/rasa:$RASA_VERSION \
  train \
    -vv \
    --data data \
    --debug-plots \
    --force \

echo "Training done"