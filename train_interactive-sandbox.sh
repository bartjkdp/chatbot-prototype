#!/bin/bash

# Load up .env
set -o allexport; source .env; set +o allexport
# Start Rasa in Interactive training mode
echo To start interactive training make sure you "run_action_server.sh"
echo Visit http://localhost:5005/visualization.html to view models
docker run -it -p 5006:5006 -v $(pwd)/training-sandbox:/app -v $(pwd)/models:/app/models -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:$RASA_VERSION interactive --debug --endpoints endpoints-interactive.yml --model models --data data/stories
