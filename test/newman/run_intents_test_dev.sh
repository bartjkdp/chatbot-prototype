#!/bin/bash
POSTMAN_ENVIRONMENT="chatbot_dev.postman_environment.json"
POSTMAN_COLLECTION="chatbot_intents.postman_collection.json"
POSTMAN_ITERATION_DATA="testdata_intents.csv"
REPORT_PREFIX="testdata_intents"

# Define a timestamp function
timestamp() {
  date +"_%Y%m%d-%H%M"
}

REPORT_NAME="$REPORT_PREFIX$(timestamp)"
#newman run chatbot_callscripts.postman_collection.json -e chatbot_dev.postman_environment.json -d testdata_callscripts.csv
docker run --network=my-rasa-network -v $(pwd)/collections:/etc/newman --entrypoint /bin/sh -t postman/newman:alpine \
    -c "npm i -g newman-reporter-htmlextra; \
    newman run $POSTMAN_COLLECTION \
    --environment=$POSTMAN_ENVIRONMENT \
    --reporters="json,htmlextra,cli" \
    --reporter-json-export=\"reports/$REPORT_NAME.json\" \
    --reporter-htmlextra-export=\"reports/$REPORT_NAME.html\" \
    --iteration-data=$POSTMAN_ITERATION_DATA" 

open $(pwd)/collections/reports/$REPORT_NAME.html
