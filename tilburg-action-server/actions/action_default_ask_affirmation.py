import logging
from datetime import datetime
from typing import Text, Dict, Any, List
import json
import pandas as pd

from rasa_sdk import Action, Tracker, ActionExecutionRejection
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    FollowupAction,
    Form,
)

logger = logging.getLogger(__name__)


class ActionDefaultAskAffirmation(Action):
    """Default implementation which asks the user to affirm his intent.
       It is suggested to overwrite this default action with a custom action
       to have more meaningful prompts for the affirmations. E.g. have a
       description of the intent instead of its identifier name.
    """

    def name(self) -> Text:
        return "action_default_ask_affirmation"
        # return ACTION_DEFAULT_ASK_AFFIRMATION_NAME

    def __init__(self) -> None:
        return

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List["Event"]:
      
        logger.info(tracker.latest_message)
        intent_ranking = tracker.latest_message.get("intent_ranking", [])

        if len(intent_ranking) == 0:

            dispatcher.utter_template("utter_ask_rephrase", tracker)
            return []

        if len(intent_ranking) > 1:
            diff_intent_confidence = intent_ranking[0].get(
                "confidence"
            ) - intent_ranking[1].get("confidence")
            if diff_intent_confidence < 0.2:
                intent_ranking = intent_ranking[:2]
            else:
                intent_ranking = intent_ranking[:1]
        first_intent_names = [
            intent.get("name", "")
            for intent in intent_ranking
            if intent.get("name", "") != "out_of_scope"
        ]

        entities = tracker.latest_message.get("entities", [])
        entities = {e["entity"]: e["value"] for e in entities}

        entities_json = json.dumps(entities)

        buttons = []
        buttons_test = buttons
        for intent in first_intent_names:
            logger.info(intent)
            logger.info(entities)
            buttons.append(
                {
                    "title": "/{}{}".format(intent, entities_json),
                    "payload": "/{}{}".format(intent, entities_json),
                }
            )

        buttons.append({"title": "Iets anders", "payload": "/{}".format("negative")})

        dispatcher.utter_button_template("utter_ask_affirmation", buttons, tracker)
        
        return []
  
