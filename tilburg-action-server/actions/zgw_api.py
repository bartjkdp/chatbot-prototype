import logging
import os
import requests

logger = logging.getLogger(__name__)

CLIENT_ID = "chatbot-4s0oF15PqmLR"
SECRET = "FMPvT4hzRlSx5Whz3rCkGfzd6vUMiMtg"

# Let op: Er zitten 2 verschillende MOR Zaaktypes in het ZTC, dus de oude URL heb ik niet gebruikt.
# Onderstaande verwijst naar het door mij aangemaakte Verhuizen zaaktype in het TEST catalogus
ZAAKTYPE_URL = "https://ref.tst.vng.cloud/ztc/api/v1/catalogussen/f7afd156-c8f5-4666-b8b5-28a4a9b5dfc7/zaaktypen/51f89538-6920-4175-8669-47d16806b354"

# Ingesteld voor ZRC + permissies + ZAAKTYPE_URL + openbaar via: https://ref.tst.vng.cloud/tokens/set-auth/
jwt_token = os.environ.get("ZGW_JWT_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImNsaWVudF9pZGVudGlmaWVyIjoiY2hhdGJvdC00czBvRjE1UHFtTFIifQ.eyJpc3MiOiJjaGF0Ym90LTRzMG9GMTVQcW1MUiIsImlhdCI6MTU2MTQ2MzE2NSwiY2xpZW50X2lkIjoiY2hhdGJvdC00czBvRjE1UHFtTFIifQ.fwj5ocAXjAKQTIGWp3NKIFsEPGHdc_mVAZT_ULMZYTM")
headers = {"Accept-Crs": "EPSG:4326",
           "Content-Crs": "EPSG:4326",
           "Content-Type": "application/json",
           "Authorization": "Bearer {}".format(jwt_token)}
COORDS = [5.0883271, 51.5539926] # Coordinaten van Tilburg

class ZGWAPI(object):

    def get_zaaktype(self):
        response = requests.get(ZAAKTYPE_URL, headers=headers).json()

    def zaak_toevoegen(self, omschrijving, toelichting, startdatum, geo=None,
                       archiefstatus="nog_te_archiveren", betalingsindicatie="nvt",
                       vertrouwelijkheidsaanduiding="openbaar", organisatie="601521894"):
        if not geo:
            geo = COORDS
        logger.info("In ZGWAPI zaak_toevoegen")
        url = os.environ.get("ZGW_API_URL", 'https://ref.tst.vng.cloud/zrc/api/v1/zaken')
        data = {
            "bronorganisatie": organisatie,
            "omschrijving": omschrijving,
            "toelichting": toelichting,
            "zaaktype": ZAAKTYPE_URL,
            "verantwoordelijkeOrganisatie": organisatie,
            "startdatum": startdatum.strftime("%Y-%m-%d"),
            "vertrouwelijkheidsaanduiding": vertrouwelijkheidsaanduiding,
            "betalingsindicatie": betalingsindicatie,
            "archiefstatus": archiefstatus,
            "zaakgeometrie": {
                "type": "Point",
                "coordinates": geo,
            }
        }
        logger.info(url)
        logger.info(data)
        logger.info(headers)
        try:
            response_data = requests.post(url, json=data, headers=headers).json()
        except Exception as e:
            logger.warning(e)
            return None
        logger.info(response_data)
        return response_data
