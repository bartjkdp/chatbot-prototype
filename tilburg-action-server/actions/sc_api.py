import logging
import requests
import xml.etree.ElementTree as ET

logger = logging.getLogger(__name__)

class SamenwerkendeCatalogiAPI(object):

    def required_slots(self, tracker, product_type):
        product = tracker.get_slot("product")
        if product:
            response = self.get(product)
            if response["generic"][product_type] == True:
                return ["product",]
        return ["product", "municipality"]

    def getAanvraagURL(self, product, municipality=None):
        aanvraagURL = None
        if product == "verhuismelding": # Product heet anders in SC
            product = "verhuizing"
        logging.info("Slot: %s", municipality)
        logging.info("Slot: %s", product)
        #if municipality != None and product != None:
        response = requests.get('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
          params={'version':'1.2', 
                  'operation':'searchRetrieve',
                  'x-connection':'sc',
                  'recordSchema':'sc4.0',
                  'startRecord':'1',
                  'maximumRecords':'10',
                  'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,product)}
        ) #make an api call
        logging.info('SC.XML response request url: %s',response.request.url)
        logging.info('SC.XML response: %s',response.text)
        root = ET.fromstring(response.content)
        #logging.info('SC.XML response tag: %s',root.tag)
        ns = {'test': 'http://www.loc.gov/zing/srw/',
              'dcterms': 'http://purl.org/dc/terms/',
              'overheid': 'http://standaarden.overheid.nl/owms/terms/',
              'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
              'sru': 'http://standaarden.overheid.nl/sru',
              'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

        recordcount = root.find("test:numberOfRecords", ns)
        if int(recordcount.text) > 0:      
          element = root.find("test:records", ns)
          element = element.find("test:record", ns)
          element = element.find("test:recordData", ns)
          element = element.find("sru:gzd", ns)
          element = element.find("sru:originalData", ns)
          element = element.find("overheidproduct:scproduct", ns)
          element = element.find("overheidproduct:meta", ns)
          elementSCMeta = element.find("overheidproduct:scmeta", ns)
          elementOnlineAanvragen = elementSCMeta.find("overheidproduct:onlineAanvragen", ns)
          if elementOnlineAanvragen.text in ["ja", "digid"]:
            elementSCMetaAanvraagURL = elementSCMeta.find("overheidproduct:aanvraagURL", ns)
            elementSCMetaAanvraagURL = elementSCMetaAanvraagURL.attrib
            elementSCMetaAanvraagURL = elementSCMetaAanvraagURL.get("resourceIdentifier", None)

            if elementSCMetaAanvraagURL is not None:
              aanvraagURL = elementSCMetaAanvraagURL
                         
        return aanvraagURL
    
    
    
    def getProductURL(self, product, municipality=None):
        productURL = None
        if product == "verhuismelding": # Product heet anders in SC
          product = "verhuizing"

        #if municipality != None and product != None:
        response = requests.get('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
          params={'version':'1.2', 
                  'operation':'searchRetrieve',
                  'x-connection':'sc',
                  'recordSchema':'sc4.0',
                  'startRecord':'1',
                  'maximumRecords':'10',
                  'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,product)}
        ) 
        # DEBUG 
        logging.info('SC.XML response request url: %s',response.request.url)
        logging.info('SC.XML response: %s',response.text)
        # DEBUG END
        root = ET.fromstring(response.content)
        ns = {'test': 'http://www.loc.gov/zing/srw/',
              'dcterms': 'http://purl.org/dc/terms/',
              'overheid': 'http://standaarden.overheid.nl/owms/terms/',
              'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
              'sru': 'http://standaarden.overheid.nl/sru',
              'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
        
        recordcount = root.find("test:numberOfRecords", ns)
        if int(recordcount.text) > 0:      
          element = root.find("test:records", ns)
          element = element.find("test:record", ns)
          element = element.find("test:recordData", ns)
          element = element.find("sru:gzd", ns)
          element = element.find("sru:originalData", ns)
          element = element.find("overheidproduct:scproduct", ns)
          element = element.find("overheidproduct:meta", ns)
          element = element.find("overheidproduct:owmskern", ns)
          element = element.find("dcterms:identifier", ns)

          if element is not None:
            productURL = element.text
        return productURL