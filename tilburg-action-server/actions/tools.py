import logging
import os
import requests
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

logger = logging.getLogger(__name__)


class Tools(object):

    @staticmethod
    def isdigidchannel(tracker: Tracker):
        if "RASA_DIGID_CHANNELS" in os.environ:
            digidchannels = os.environ['RASA_DIGID_CHANNELS'].split()
            return tracker.get_latest_input_channel() in digidchannels
        else:
            return True