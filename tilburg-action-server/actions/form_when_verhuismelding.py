import datetime
import isodate
import logging
import os
import requests

from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI
from .form_when import WhenFormAction

logger = logging.getLogger(__name__)

class VerhuismeldingWhenFormAction(WhenFormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_when_verhuismelding"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill
        content_api = ContentAPI()
        required_slots = content_api.required_slots(tracker, "channels_register")"""
        required_slots = ["time"]

        return required_slots 

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "time": [self.from_entity(entity="time")]
        }
        
    def validate_verhuismelding_datum(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate date"""

        if value == None:
            dispatcher.utter_template("utter_ask_verhuismelding_datum", tracker)
            return {"time": value}
        else:
            datum = None
            ducktime = next(tracker.get_latest_entity_values("time"), None)
            if ducktime is not None:
                # If detected: Iso-formatted datetime: 2019-09-03T20:56:35.450686 + timezone stuff
                try:
                    datum = datetime.datetime.strptime(ducktime[:10], "%Y-%m-%d").date()
                except Exception as e:
                    logger.exception(e)
                    return {"time": None} #Should never happen since ducktime is detected
            else:
                try:
                    datum = datetime.datetime.strptime(value, "%Y-%m-%d").date()
                except Exception as e:
                    try:
                        datum = datetime.datetime.strptime(value, "%d-%m-Yd").date()
                    except Exception as e:
                        logger.exception(e)
                        dispatcher.utter_template("utter_warning_verhuismelding_datum_invalid", tracker)
                        return {"time": None}
            return {"time": datum.__str__()}

    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled -->
            Bereken de afstand"""

        content_api = ContentAPI()
        response = content_api.get("verhuizing")
        datum = tracker.get_slot("time")

        if datum == None:
            dispatcher.utter_message(response["term"]["info"])
        else:
            try:
                datum = datetime.datetime.strptime(datum[:10], "%Y-%m-%d").date()
            except Exception as e:
                logger.exception(e)
                return {"time": None} #Should never happen since ducktime is detected

            # Determine the valid date-range based on the duration-fields for the product via
            # the Content API
            within_before = response["term"]["within_before"]
            within_after = response["term"]["within_after"]

            today = datetime.date.today()

            before_date = datum - isodate.parse_duration(within_before)
            after_date = datum + isodate.parse_duration(within_after)
            five_days_ago = today - isodate.parse_duration(within_after)
            logger.info("Aangegeven datum: {}".format(datum))
            logger.info("Dit mag tussen {} en {}".format(before_date, after_date))

            if before_date < today < after_date:
                dispatcher.utter_message("Dat moet dan voor {0}".format(after_date.strftime("%d-%m-%Y")))
            elif before_date < today:
                dispatcher.utter_message("Oei, dat moest uiterlijk {0}. Geef het dan vandaag nog door. Je gebruikt als verhuisdatum {1}.".format(after_date.strftime("%d-%m-%Y"),five_days_ago.strftime("%d-%m-%Y")))
            else:
                dispatcher.utter_message("Dat moet dan tussen {0} en {1}".format(before_date.strftime("%d-%m-%Y"),after_date.strftime("%d-%m-%Y")))
        return []

