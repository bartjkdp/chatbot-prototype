import random
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher

class ActionRollDice(Action):
  def name(self):
    return "action_roll_dice"

  def run(self, dispatcher, tracker, domain):
    dice_type = tracker.get_slot("dice_type")
    if dice_type != None:
      die = dice_type[3:]
    else:
      die = tracker.get_slot("die")
          
    if die != None:
      diceresult = random.randint(1,int(die))
      dispatcher.utter_template("utter_die_result", tracker, result="{0}".format(diceresult))
      
    dispatcher.utter_template("utter_ask_die", tracker)
    return []
