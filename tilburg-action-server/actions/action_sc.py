import requests
import logging
import xml.etree.ElementTree as ET
from rasa_sdk import Action

class ActionGetProductURL(Action):
  def name(self):
    return "action_productURL"

  def run(self, dispatcher, tracker, domain):
    # TODO: add checks for input slots => change CustomAction to a ActionForm
    municipality = tracker.get_slot("municipality")
    product = tracker.get_slot("product")

    logging.info("Slot: %s", municipality)
    logging.info("Slot: %s", product)

    if product == "verhuismelding": # Product heet anders in SC
      product = "verhuizing"
      #product = "verhuismelding"

    elif product == "melding-openbare-ruimte": # Product heet anders in SC
      product = "melding openbare ruimte"
      #product = "melding_openbare_ruimte_(algemeen)" # UPL
      
    elif product == "geboorteaangifte":
      product = "geboorteaangifte"
      
    elif product == "rijbewijs":
      product = "rijbewijs" 
    
    elif product == "id-kaart":
      product = "identiteitskaart"  
      
    elif product == "brp-uittreksel":
      product = "uittreksel" 

    elif product == "uittreksel-verhuismelding":
      product = "uittreksel"
    
    elif product == "belasting-kwijtschelding":
      product = "kwijtschelding" 
      #product = "belastingkwijtschelding" # UPL

    #if municipality != None and product != None:
    # Option: replace keyword with uniformeProductnaam to switch to us
    response = requests.get('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
      params={'version':'1.2', 
              'operation':'searchRetrieve',
              'x-connection':'sc',
              'recordSchema':'sc4.0',
              'startRecord':'1',
              'maximumRecords':'10',
              'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,product)}
    ) 
    # DEBUG 
    logging.info('SC.XML response request url: %s',response.request.url)
    logging.info('SC.XML response: %s',response.text)
    # DEBUG END
    root = ET.fromstring(response.content)
    ns = {'test': 'http://www.loc.gov/zing/srw/',
          'dcterms': 'http://purl.org/dc/terms/',
          'overheid': 'http://standaarden.overheid.nl/owms/terms/',
          'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
          'sru': 'http://standaarden.overheid.nl/sru',
          'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
    
    recordcount = root.find("test:numberOfRecords", ns)
    if int(recordcount.text) > 0:      
      element = root.find("test:records", ns)
      element = element.find("test:record", ns)
      element = element.find("test:recordData", ns)
      element = element.find("sru:gzd", ns)
      element = element.find("sru:originalData", ns)
      element = element.find("overheidproduct:scproduct", ns)
      element = element.find("overheidproduct:meta", ns)
      element = element.find("overheidproduct:owmskern", ns)
      element = element.find("dcterms:identifier", ns)

      if product == "paspoort":
        url = element.text
        dispatcher.utter_template("utter_product_paspoort_lost", tracker, url="{0}".format(url))
      
      elif product == "identiteitskaart":
        url = element.text
        dispatcher.utter_template("utter_product_paspoort_lost", tracker, url="{0}".format(url))

      elif product == "uittreksel":
        url = element.text
        dispatcher.utter_template("utter_product_uittreksel-verhuismelding_proofregistration", tracker, url="{0}".format(url))

      elif product == "verhuizing":
        url = element.text
        dispatcher.utter_template("utter_product_verhuismelding_dontwantdigid", tracker, url="{0}".format(url))
        
      elif element is None:
        dispatcher.utter_template("utter_sc_not_found", tracker)

      else:
        url = element.text
        dispatcher.utter_template("utter_sc_found", tracker, url="{0}".format(url))

        #if int(recordcount.text) > 1:
          #url_sc = "https://www.overheid.nl/zoekresultaat/diensten-per-organisatie/1/10/_overheid.authority={0}".format(municipality)
          #dispatcher.utter_template("utter_sc_more_found", tracker, sc_url="{0}".format(url_sc)
          #dispatcher.utter_template("utter_sc_more_found", tracker, items="{0}".format(int(recordcount.text)-1))
          
    else:
      dispatcher.utter_template("utter_sc_not_found", tracker)
      
    return []
