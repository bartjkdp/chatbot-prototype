import requests
import logging
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI
from .sc_api import SamenwerkendeCatalogiAPI

class ChannelFormAction(FormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_channels"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        content_api = ContentAPI()
        return content_api.required_slots(tracker, "channels_apply")

    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        content_api = ContentAPI()
        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")
        response = content_api.get(product, municipality)
        if not response:
            dispatcher.utter_template("utter_product_not_found", tracker)
            return []
        channel = tracker.get_latest_input_channel()
        appointmenturl = None
        onlineurl = None
        scurl = None
        chatbot = False
        sc_api = SamenwerkendeCatalogiAPI()
        sc_url = sc_api.getProductURL(product,municipality)
       
        if response["channels_apply"].get("appointment"):
            appointmenturl = response["channels_apply"]["appointment"]
        if response["channels_apply"].get("online"):
            onlineurl = response["channels_apply"]["online"]
            #override with sc url
            aanvraagurl = sc_api.getAanvraagURL(product,municipality)
            if aanvraagurl is not None:
                onlineurl = aanvraagurl
        if response["channels_apply"].get("chatbot_digid"):
            chatbot = True
        
        if (channel == "socketio"):
            message = ""
            if appointmenturl is not None and onlineurl is None:
                message += "Je kunt hier [online een afspraak]({0}) voor maken. ".format(appointmenturl)
            elif appointmenturl is not None and onlineurl is not None:
                message += "Je kunt dit [op de website]({0}) regelen of hier [online een afspraak]({1}) voor maken.  ".format(onlineurl,appointmenturl)
            elif appointmenturl is None and onlineurl is not None:
                message += "Je kunt dit [op de website]({0}) regelen.  ".format(onlineurl)
            else:
                message += ""
                
            message += "Meer informatie is te vinden op [op de website]({0})".format(sc_url)
            dispatcher.utter_message(message)
        elif (False and channel == "facebook_referral"):
            a = 2
        else:
            message = ""
            if appointmenturl is not None and onlineurl is None:
                message += "Je kunt hier online een afspraak voor maken. "
            elif appointmenturl is not None and onlineurl is not None:
                message += "Je kunt dit op de website regelen of hier online een afspraak voor maken.  "
            elif appointmenturl is None and onlineurl is not None:
                message += "Je kunt dit op de website regelen.  "
            else:
                message += ""
            message += "Meer informatie is te vinden op url"    
            dispatcher.utter_message(message)
        return []
