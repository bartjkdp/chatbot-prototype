import random
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher

import logging
import requests
import json

logger = logging.getLogger(__name__)


class ActionCBSPopulation(Action):
  def name(self):
    return "action_cbs_population"

  def run(self, dispatcher, tracker, domain):
    #https://opendata.cbs.nl/ODataApi/odata/37296eng/TypedDataSet
    # get length of value[-1].TotalPopulation_1
    
    logger.info("CBS_Population")
    url = "https://opendata.cbs.nl/ODataApi/odata/37296eng/TypedDataSet"
    logger.info("CBS_Population: {0}".format(url))
    try:
      response = requests.get(url).json() #make an api call
    except Exception as e:
      logger.warning(e)
      dispatcher.utter_message("Wat is geel en zie je niet? Een banaan om de hoek (API is weg)")
      return []
    logger.info(response)
    population = response['value'][-1]['TotalPopulation_1'] #extract population returned json response
    year = response['value'][-1]['Periods'] #extract population returned json response
    year = year[:-4]
    logger.info(population)
    dispatcher.utter_message("Nederland heeft {0} inwoners in {1}".format(str(int(population)), year)) #send the message back to the user
    return []
