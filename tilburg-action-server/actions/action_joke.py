import requests
import json
import os
import logging
from rasa_sdk import Action

logger = logging.getLogger(__name__)

class ActionJoke(Action):
  def name(self):
    return "action_joke"

  def run(self, dispatcher, tracker, domain):
    logger.info("In joke action")
    content_api = os.environ.get('CONTENT_API')
    url = content_api + 'jokes/random'
    logger.info("Content API + Joke URL: {}".format(url))
    try:
      response = requests.get(url).json() #make an api call
    except Exception as e:
      logger.warning(e)
      dispatcher.utter_message("Wat is geel en zie je niet? Een banaan om de hoek (API is weg)")
      return []
    logger.info(response)
    joke = response['value']['joke'] #extract a joke from returned json response
    logger.info(joke)
    dispatcher.utter_message(joke) #send the message back to the user
    return []
