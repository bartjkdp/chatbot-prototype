from .bag_api import BagAPI
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
import logging
logger = logging.getLogger(__name__)

# gets full address from zipcode and house number

class ActionGetaddress(Action):
    def name(self):
        return "action_getaddress"

    def run(self, dispatcher, tracker, domain):
        postcode = tracker.get_slot("new_postalcode")
        housenr = tracker.get_slot("new_housenr")
        post_api = BagAPI()

        if postcode and housenr:
            data = post_api.get(postcode, housenr)
            logger.info(data)
            if data == None :
                logger.info("Empty data")
                dispatcher.utter("Sorry, ik kon je adres niet vinden", tracker)
            else:
                logger.info(data["street"])
                logger.info(data["city"])
                return [
                    SlotSet('new_street', data["street"]),
                    SlotSet('new_city', data["city"])
                ]
        else:
            dispatcher.utter_template("utter_verhuizing_request_new", tracker)
            return []
