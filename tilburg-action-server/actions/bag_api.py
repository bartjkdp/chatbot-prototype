import logging
import os
import requests

logger = logging.getLogger(__name__)


class BagAPI(object):

    def get(self, postcode, housenr):
        logger.info("In Content API GET")
        API_KEY = os.environ.get('BAG_API_KEY', None)
        if not API_KEY:
            logger.error("BAG_API_KEY isn't configured, please set it to connect to the BAG API")
            return None
        if not postcode or not housenr:
            logger.info("Missing required fields")
            return None

        API_URL = os.environ.get('BAG_API_URL', 'http://bag.basisregistraties.overheid.nl/api/v1/nummeraanduidingen')
        postcode = postcode.replace(" ", "").upper()
        housenr = housenr.replace(" ", "")
        headers = {'X-Api-Key': API_KEY}
        params = {'postcode': postcode, 'huisnummer': housenr}
        logger.info(params)
        logger.info(headers)
        try:
            id_response = requests.get(API_URL, params=params, headers=headers).json()
        except Exception as e:
            logger.warning(e)
            return None
        logger.info(id_response)
        if not '_embedded' in id_response:
            logger.info("Invalid response")
            return None
        try:
            street_response = requests.get(id_response['_embedded']['nummeraanduidingen'][0]['_links']['bijbehorendeOpenbareRuimte']['href'],headers=headers).json()
            street = street_response['naam']
        except Exception as e:
            logger.warning(e)
            return None
        geo = None
        try:
            geo_response = requests.get(id_response['_embedded']['nummeraanduidingen'][0]['_links']['adresseerbaarObject']['href'],headers=headers).json()
            logger.info(geo_response)
            geo = geo_response["_embedded"]["geometrie"]["coordinates"]
        except Exception as e:
            logger.warning(e)
        try:
            city_response = requests.get(street_response['_links']['bijbehorendeWoonplaats']['href'], headers=headers).json()
            city = city_response['naam']
        except Exception as e:
            logger.warning(e)
            return None
        data = {'street': street, 'city': city,'postcode': postcode, 'geo': geo}
        logger.info(data)
        return data
