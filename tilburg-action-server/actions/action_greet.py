import requests
import json
import os
import logging
import random
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher
from rasa_sdk.events import UserUtteranceReverted, SlotSet

logger = logging.getLogger(__name__)

class ActionGreet(Action):
  def name(self):
    return "action_greet"

  def run(self, dispatcher, tracker, domain):
    ID = tracker.get_slot("context_chatid")
    name = tracker.get_slot("context_name")
    municipality = tracker.get_slot("context_municipality")
    events = [UserUtteranceReverted()]
    if municipality == None:
      municipality = next(tracker.get_latest_entity_values("municipality"), None)
    name = next(tracker.get_latest_entity_values("name"), name) #use name from slot as fallback
    if ID == None :
      ID = 0
      content_api = os.environ.get('CONTENT_API')
      url = content_api + 'sequence/develop'
      logger.info("Content API + Sequence URL: {}".format(url))

      try:
        response = requests.get(url).json() #make an api call
      except Exception as e:
        logger.warning(e)
        return []

      logger.info(response)
      ID = response['sequence'] #extract sequence from returned json response
      events.append(SlotSet('context_chatid', "{0}".format(ID)))
    logger.info(ID)
        
    if name == None :
      dispatcher.utter_message(template="utter_greet")
    else:
      dispatcher.utter_message(template="utter_greet_personal",name=name)
    dispatcher.utter_message(template="utter_chatid",ID="{0}".format(ID))
    dispatcher.utter_message(template="utter_howcanihelp")

    if municipality:
      events.append(SlotSet('context_municipality',municipality))
    if name:
      events.append(SlotSet('context_name',name))
    
    return events
