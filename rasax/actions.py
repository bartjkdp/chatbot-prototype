import requests
import json
import logging
import xml.etree.ElementTree as ET

from typing import Dict, Text, Any, List, Union, Optional
from rasa_core_sdk import ActionExecutionRejection
from rasa_core_sdk import Tracker
from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet
from rasa_core_sdk.executor import CollectingDispatcher
from rasa_core_sdk.forms import FormAction, REQUESTED_SLOT


class ActionJoke(Action):
  def name(self):
    return "action_joke"

  def run(self, dispatcher, tracker, domain):
    request = requests.get('http://webservices.tilburg.io:8099/jokes/random').json() #make an api call
    joke = request['value']['joke'] #extract a joke from returned json response
    dispatcher.utter_message(joke) #send the message back to the user
    return []


class ActionGetProductURL(Action):
  def name(self):
    return "action_productURL"

  def run(self, dispatcher, tracker, domain):
    # TODO: add checks for input slots => change CustomAction to a ActionForm
    municipality = tracker.get_slot("municipality")
    product = tracker.get_slot("product")
     
    logging.info("Slot: %s", municipality)
    logging.info("Slot: %s", product)
    #if municipality != None and product != None:
    response = requests.get('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
      params={'version':'1.2', 
              'operation':'searchRetrieve',
              'x-connection':'sc',
              'recordSchema':'sc4.0',
              'startRecord':'1',
              'maximumRecords':'10',
              'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,product)}
    ) #make an api call
    logging.info('SC.XML response request url: %s',response.request.url)
    logging.info('SC.XML response: %s',response.text)
    root = ET.fromstring(response.content)
    #logging.info('SC.XML response tag: %s',root.tag)
    ns = {'test': 'http://www.loc.gov/zing/srw/',
          'dcterms': 'http://purl.org/dc/terms/',
          'overheid': 'http://standaarden.overheid.nl/owms/terms/',
          'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
          'sru': 'http://standaarden.overheid.nl/sru',
          'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
    
    recordcount = root.find("test:numberOfRecords", ns)
    if int(recordcount.text) > 0:      
      #element = root.find("dcterms:identifier", ns)
      element = root.find("test:records", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("test:record", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("test:recordData", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("sru:gzd", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("sru:originalData", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("overheidproduct:scproduct", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("overheidproduct:meta", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("overheidproduct:owmskern", ns)
      #logging.info('SC.XML element tag: %s',element.tag)
      element = element.find("dcterms:identifier", ns)
      #logging.info('SC.XML element tag: %s',element.tag)

      if element is None:
        #logging.info('SC.XML no product found')
        dispatcher.utter_template("utter_product_not_found", tracker)
      else:
        url = element.text
        #logging.info('SC.XML url: %s', url)
        dispatcher.utter_message("Zie voor meer informatie {0}".format(url))
        if int(recordcount.text) > 1:
          dispatcher.utter_message("We hebben nog meer informatie gevonden. ({0})".format(int(recordcount.text)-1))
          
    else:
      dispatcher.utter_template("utter_product_not_found", tracker)
      
    return []


class PriceForm(FormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "price_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["product", "municipality"]


    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        # utter submit template
        #dispatcher.utter_template('utter_submit', tracker)
        return [SlotSet("price", 50)]
