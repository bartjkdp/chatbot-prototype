#!/usr/bin/env bash

# Required to allow training of new models via Rasa X
chmod -R 777 ../data
docker-compose -f rasa-x.yml up
