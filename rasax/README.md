Rasa X 
------

# Requirements

- Docker
- git

# Rasa X Setup

## .env

    touch .env

## Step 2.

The following steps are required before you can run Rasa X.

1. Create a connection between rasa and rasax containers, this is already part of the Rasa setup.
    
    docker network create my-rasa-network 
   
2. Start Rasa
    
    cd PROJECT_FOLDER
    
    docker-compose up | ./run.sh
    
    
3. Start Rasa X 

Open another console session to run start Rasa X.
An init.sh script is included in this project to create the required database for Rasa (X)? when first run.
  
    cd PROJECT_FOLDER/rasax/
    
    docker-compose up | ./run.sh


4. Add Firewall rule to Google Cloud environment for rasa-strack-vm

Rasa X is running on port 81, this port needs to be exposed to the outside.


5. Accessing Rasa X

Point you browser to http://localhost:81, and enter the password found on the console. Look for:

    -------
    
    
## Switch training data folder

FIXIT: rename training-sandbox.

If you want to use the a different training set edit your .env file and enable the following line by removed in the #:

    RASA_DATA=training-sandbox
    
Then restart Rasa X.


## Reset Rasa (X) database

If for some reason you want to start with a fresh database, stop both Rasa and Rasa X before you continue then execute the following commands:

    cd PROJECT_HOME
    rm -rf db
    

# Migrating tracker store events from Rasa to Postgresql database of Rasa X on a developement environment

    docker-compose run rasa-x python ./migrate_tracker_store.py 
    or  
    ./migrate_tracker.sh


# Migrating tracker store events from Rasa into Postgresql database of Rasa X on a Container Optimized server-environment
    
    docker run -i --rm -v /var/run/docker.sock:/var/run/docker.sock -v "$PWD:$PWD" -w="$PWD" docker/compose:1.24.0 run rasa-x python ./migrate_tracker_store.py
    or
    sh ./migrate_tracker_server.sh
    
    
# Migrating tracker store events from Rasa (Container Optimized server-environment) to developement environment

TODO: This needs to be explored a bit futher!

1. Stop Rasa
2. Stop Rasa X
3. Zip the "db" folder and transfer using gitlab?
4. 

# Rasa X production install & conversation import script
FIXIT: layout

https://trello.com/c/n1qyIdXu/107-rasa-x-productie-wachtwoord

 * http://chatbot.tilburg.io:81
 * Conversations zijn in Rasa X via het migrate_tracker_store/server script in te laden uit Rasa.
 * Rasa X + conversations lijkt goed te werken:

$ python3 migrate_tracker_store.py
Welcome to Rasa X 🚀

This script will migrate your old tracker store to the new SQL based Rasa X tracker store.
Let's start!

? Please provide the path to your endpoints configuration which specifies the credentials for your old tracker store: endpoints-local.yml

? Do you want to migrate to the local version of Rasa X?  No

? Please provide the path to your endpoints configuration which specifies the credentials for your new tracker store: endpoints-local.yml

Old and new endpoints file is the same. I will skip migrating the tracker store and only migrate the events to Rasa X.
