#!/bin/bash

# clear model directory

    echo "Splitting Model in progress"

# Load up .env
set -o allexport; source .env; set +o allexport 
# Forced retraining of Rasa NLU + Core
docker run \
  -v $(pwd)/training-sandbox:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom \
  rasa/rasa:$RASA_VERSION \
  data split nlu \

docker run \
  -v $(pwd)/training-sandbox:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom \
  rasa/rasa:$RASA_VERSION \
  test nlu -u train_test_split/test_data.md \

docker run \
  -v $(pwd)/training-sandbox:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom \
  rasa/rasa:$RASA_VERSION \
  test core \
  --stories test_stories.md \
  --out results \

echo "Split done"