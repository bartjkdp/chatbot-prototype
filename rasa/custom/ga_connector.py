from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import inspect

import logging

from sanic import Blueprint, response
from sanic.request import Request

from rasa.core.channels.channel import UserMessage, OutputChannel
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import CollectingOutputChannel
from typing import Any, Dict, List, Text

import json	

logger = logging.getLogger(__name__)

class GoogleAssistantOutput(OutputChannel):
    """Output channel that collects send messages in a list"""

    # TODO: Implement all of the following functions to match the Google Speech APIs JSON format. See README.

    def __init__(self):
        self.messages = []

    @classmethod
    def name(cls):
        return 'google_home'

    @staticmethod
    def _message(recipient_id, text=None, image=None, buttons=None, attachment=None, custom=None):
        obj = {
            'recipient_id': recipient_id,
            'text': text,
            'image': image,
            'buttons': buttons,
            'attachment': attachment,
            'custom': custom,
        }
        return {k: v for k, v in obj.items() if v is not None}

    def latest_output(self):
        if self.messages:
            return self.messages[-1]
        else:
            return None

    async def send_text_message(self, recipient_id: Text, text: Text, **kwargs: Any) -> None:
        for message_part in text.split('\n\n'):
            await self.messages.append(self._message(recipient_id, text=message_part))

    async def send_image_url(self, recipient_id: Text, image: Text, **kwargs: Any) -> None:
        await self.messages.append(self._message(recipient_id, image=image))

    async def send_attachment(self, recipient_id: Text, attachment: Text, **kwargs: Any) -> None:
        await self.messages.append(self._message(recipient_id, attachment=attachment))

    async def send_text_with_buttons(
            self,
            recipient_id: Text,
            text: Text,
            buttons: List[Dict[Text, Any]],
            **kwargs: Any
    ) -> None:
        await self.messages.append(self._message(recipient_id, text=text, buttons=buttons))

    async def send_custom_json(self, recipient_id: Text, json_message: Dict[Text, Any], **kwargs: Any) -> None:
        await self.messages.append(self._message(recipient_id, custom=json_message))



class GoogleConnector(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @classmethod
    def name(cls):
        return "google_home"


    def blueprint(self, on_new_message):
    
        google_webhook = Blueprint("google_webhook", __name__)


        @google_webhook.route("/", methods=['GET'])
        async def health(request: Request):
            return response.json({"status": "ok"})

        @google_webhook.route("/webhook", methods=['POST'])
        async def receive(request: Request):
            
            payload = request.json		
            sender_id = payload['conversation']['conversationId']
            intent = payload['inputs'][0]['intent'] 			
            text = payload['inputs'][0]['rawInputs'][0]['query'] 		

            response_json = {
                  "conversationToken": "{\"state\":null,\"data\":{}}",
                  "expectUserResponse": 'true',
                  "expectedInputs": [
                    {
                      "inputPrompt": {
                        "richInitialPrompt": {
                          "items": []
                        }
                      },
                      "possibleIntents": [
                        {
                          "intent": "actions.intent.TEXT"
                        }
                      ]
                    }
                  ]
               }
               
            items = response_json["expectedInputs"][0]["inputPrompt"]["richInitialPrompt"]["items"]
            if intent == 'actions.intent.MAIN':	
                firstResponse = {"simpleResponse": {"ssml":"<speak><emphasis level=\"reduced\">Hallo,<\/emphasis><break time=\"300ms\"\/><emphasis level=\"reduced\">mijn naam is Gem!<\/emphasis><break time=\"200ms\"\/><emphasis level=\"reduced\">ik wil je graag helpen<\/emphasis><break time=\"200ms\"\/><emphasis level=\"reduced\">Wat kan ik voor je doen?<\/emphasis><\/speak>"}} 
                items.append(firstResponse)
            else:
                out = CollectingOutputChannel()			
                await on_new_message(
                  UserMessage(text, out, sender_id, input_channel=GoogleConnector.name()))
                responses = [m["text"] for m in out.messages]
                message0 = {"simpleResponse": {"textToSpeech":responses[0]}}
                items.append(message0)
                if len(responses) > 1:
                    message1 = {"simpleResponse": {"textToSpeech":responses[1]}}
                    items.append(message1)
                logger.debug(out.messages)	
                logger.debug(response_json)	

            return response.json(response_json)		
          		
        return google_webhook

