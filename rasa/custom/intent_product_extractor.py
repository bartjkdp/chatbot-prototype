from rasa.nlu.components import Component
import typing
import logging
from typing import Any, Optional, Text, Dict

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

logger = logging.getLogger(__name__)


class ProductExtractor(Component):
    """A new component"""

    # Defines what attributes the pipeline component will
    # provide when called. The listed attributes
    # should be set by the component on the message object
    # during test and train, e.g.
    # ```message.set("entities", [...])```
    provides = ["entities"]

    # Which attributes on a message are required by this
    # component. e.g. if requires contains "tokens", than a
    # previous component in the pipeline needs to have "tokens"
    # within the above described `provides` property.
    requires = ["intent"]

    # Defines the default configuration parameters of a component
    # these values can be overwritten in the pipeline configuration
    # of the model. The component should choose sensible defaults
    # and should be able to create reasonable results with the defaults.
    defaults = {"precedence": False}

    # Defines what language(s) this component can handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    language_list = None

    def __init__(self, component_config=None):
        super(ProductExtractor, self).__init__(component_config)

    def train(self, training_data, cfg, **kwargs):
        """Train this component.

        This is the components chance to train itself provided
        with the training data. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.train`
        of components previous to this one."""
        pass

    def process(self, message, **kwargs):
        """Process an incoming message.

        This is the components chance to process an incoming
        message. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.process`
        of components previous to this one."""
        logger.info("process product extractor")
        intent = message.get("intent").get("name")
        entities = message.get("entities")
        logger.info("intent = {}".format(intent))
        logger.info("entities = {}".format(entities))
        if intent is None:
            return
        intentparts = intent.split("_");
           
        # Getting length of list 
        length = len(intentparts) 
        i = 0
           
        product = None
        # Iterating using while loop 
        while i < length:
            part = intentparts[i]
            if (part == "product" and i < length-1):
                product = intentparts[i+1]
                i = length+1
            else:    
                i += 1
        if i == length:
            return

        logger.info("product = {}".format(product))
        
        existingentity = next((item for item in entities if item["entity"] == "product"), None)
        logger.info("existingentity = {}".format(existingentity))
        precedence = self.component_config.get("precedence")
        if existingentity is None or precedence: #what is the correct behaviour? set precedence in pipeline config
            entity = {'entity': 'product', 'value': product, 'confidence': 1.0, 'extractor': 'ProductExtractor', 'start': 0, 'end': 0}
            entities.append(entity)
        
    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this component to disk for future loading."""

        pass

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any
    ) -> "Component":
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            return cls(meta)


