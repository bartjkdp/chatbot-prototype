#!/bin/bash

# This is a modified version of train_sandbox.sh for our Gitlab pipeline
# to ensure the model output filename remains consistent

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/*
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

# Load up .env
set -o allexport; source .env; set +o allexport 
# Forced retraining of Rasa NLU + Core
mkdir $(pwd)/models
chmod -R 777 $(pwd)/models
docker run \
  -v $(pwd)/training-sandbox:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/build/lib/python3.6/site-packages/custom \
  rasa/rasa:$RASA_VERSION \
  train \
    -vv \
    --data data \
    --debug-plots \
    --force \
    --fixed-model-name gitlab-model # Give the model a fixed location (models/gitlab-model.tar.gz) so we can pick it up as an artifact

echo "Training done"
