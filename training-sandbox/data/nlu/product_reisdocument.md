## intent:product_id-kaart_costs
#- Wat kost een idkaart?
#- Hoeveel kost een paspoort?
#- Wat betaal ik voor een idkaart?

## intent:product_reisdoc-costs-urgent
#- Wat kost het als ik het [idkaart](product:id-kaart) sneller nodig heb?
#- Wat zijn de kosten van een spoedaanvraag voor een [idkaart](product)?
#- Wat kost een spoedaanvraag [idkaart](product)?
#- Wat betaal ik als ik het [idkaart](product) met spoed aanvraag?
#- Hoeveel meer kost een spoedaanvraag [idkaart](product)?"

## intent:product_reisdoc-costs-paymentmethod
#- Kan ik met pinpas betalen?
#- Kan ik met cash betalen?
#- Kan ik met contant geld betalen?
#- Hoe kan ik betalen bij de balie?

