## intent: product_verhuismelding_general
- Verhuizen
- Ik verhuis
- Ik wil verhuizen
- Ik ga verhuizen
- Wij gaan verhuizen
- Ik heb een vraag over verhuizen
- Mijn vraag gaat over een verhuizing


#BLOK A: Vraag termijn verhuizen

## intent:product_verhuismelding_when
- Wanneer moet ik mijn [verhuizing](product:verhuismelding) doorgeven
- Wanneer kan ik mijn [verhuismelding] doorgeven
- Op welk moment moet ik mijn [verhuizing](product:verhuismelding) doorgeven
- Wanneer geef ik mijn [adreswijziging](product:verhuismelding) door
- kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- kan ik ons als nieuwe bewoners inschrijven?
- Wanneer willen jullie [mijn nieuwe adres](product:verhuismelding) weten
- Ik weet nu al wanneer ik ga [verhuizen](product:verhuismelding). Kan ik dat nu al doorgeven?
- Kan ik mijn [verhuizing](product:verhuismelding) al doorgeven?
- Ik ben [verhuisd](product:verhuismelding). Tot wanneer kan ik dit aangeven?
- ik ben vergeten mijn [adreswijziging](product:verhuismelding) door te geven. Kan dit nog?
- ik ga in oktober [verhuizen](product:verhuismelding), kan ik dat nu al doorgeven?
- Volgende maand [verhuis](product:verhuismelding) ik. Mag ik dat nu al doorgeven?
- Wanneer kan ik mn [verhiuzin](product:verhuismelding) doorgeven
- Als ik volgende maand [verhuis](product:verhuismelding), kan ik dat dan nu al doorgeven?
- Als ik volgende week ga [verhuizen](product:verhuismelding), kan ik dat nu al zeggen?
- Wanneer kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven
- Ik ga [verhuizen](product:verhuismelding) naar Utrecht. Wanneer moet ik dit doorgeven?
- Ik ben vorige weekbij mijn vriend [gaan wonen](product:verhuismelding), wanneer moet ik dat doorgeven?
- Op welk moment moet ik mijn [verhuizing](product:verhuismelding) doorgeven? 
- Wanneer geef ik mijn [adreswijziging](product:verhuismelding) door? 
- Wanneer willen jullie mijn nieuwe adres weten?
- Binnen welke termijn moet ik mij inschrijven op mijn nieuwe adres?
- Ik ben vergeten mijn [verhuizing](product:verhuismelding) door te geven.
- Ik ben waarschijnlijk te laat met het doorgeven van mijn [adreswijziging](product:verhuismelding).
- Kan ik mijn [verhuizing](product:verhuismelding) nog doorgeven?
- Ik ben te laat met het doorgeven van mijn [verhuizing](product:verhuismelding). Kan ik dat nog doorgeven?
- Mijn [verhuizing](product:verhuismelding) heb ik te laat doorgegeven. Kan ik dat nog doen?

## intent:product_verhuismelding_when_info
- Hoe ver van tevoren kan ik [verhuizing](product:verhuismelding) doorgeven? 
- Hoe lang van tevoren moet ik mijn verhuizing doorgeven?
- Kan ik mijn verhuizing al eerder doorgeven?
- Kan ik mij al voor mijn verhuizing inschrijven?
- Kan ik mijn adreswijziging al eerder doorgeven? 
- Kan ik mij al eerder inschrijven in de gemeente?
- Is inschrijven al voor de verhuizing mogelijk?
- Hoeveel eerder kun je je verhuizing doorgeven?


#BLOK B: Vraag kind verhuizen

## intent:product_verhuismelding_kindinschrijven
- Ik wil mijn kind inschrijven.
- Kan ik mijn kind bij mij inschrijven?
- Kan ik mijn kind verhuizen? 
- Ik wil het adres van mijn kind wijzigen. 
- Ik wil mijn kind inschrijven op een ander adres.
- Kan ik mijn kind mee verhuizen? 
- Kan ik mijn kind meeverhuizen 
- Kan ik dat ook voor mijn kind doorgeven? (vervolgvraag) 
- Mijn kind(eren) verhuist/verhuizen mee, kan ik dat gelijk doorgeven?
- Kan ik mijn meerderjarige kind meeverhuizen?

## intent:product_verhuismelding_kindinschrijvenanderadres
- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven?
- Hoe geef ik het nieuwe adres van mijn studerende zoon door?
- Mijn dochter gaat in de stad studeren, hoe kan ik het nieuwe adres doorgeven?
- Kan ik het nieuwe adres doorgeven van mijn studerende kind?
- Kan ik de adreswijziging doorgeven voor mijn kind dat op kamers gaat?


#BLOK C: Vraag hoe verhuizen

## intent:product_verhuismelding_adreswijziging
- Hoe geef ik mijn [verhuizing](product:verhuismelding) door?
- Hoe kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Waar kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Waar geef ik mijn [verhuizing](product:verhuismelding) door?
- Hoe verander ik mijn adres?
- Hoe geef ik mijn [nieuwe adres](product:verhuismelding) door?
- Hoe kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven?
- Hoe geef ik mijn [adreswijziging](product:verhuismelding) door?
- Yo ik heb een [nieuw adres](product:verhuismelding)
- [Verhuizing doorgeven](product:verhuismelding)
- Ik wil mijn [adres wijzigen](product:verhuismelding)
- Ik wil mijn nieuwe adres doorgeven
- Ik heb een nieuw adres
- Ik moet een nieuw adres  doorgeven
- Ik heb een adreswijziging
- Adreswijziging
- Hoe moet ik een verhuizing doorgeven?
- Kan ik mijn nieuwe adres doorgeven?

## intent:product_verhuismelding_bevestiging
- Ik heb geen bevestiging van mijn verhuizing gekregen.
- Hoe kom ik aan een bewijs dat ik verhuisd ben?
- Krijg ik een bevestiging als ik mijn adres heb gewijzigd
- Hoe weet ik zeker dat het doorgeven van mijn nieuwe adres gelukt is?
- Hoe weet ik of het doorgeven van mijn verhuizing goed is gegaan?
- Hoe weet ik of mijn verhuizing gelukt is?
- Is mijn verhuizing al verwerkt? 
- Hebben jullie mijn verhuizing al verwerkt? 
- Is mijn verhuizing al doorgevoerd/afgewerkt/behandeld? 
- Sta ik al ingeschreven? 
- Ben ik al uitgeschreven?

## intent:product_verhuismelding_mandatory
- Moet ik mijn verhuizing doorgeven?
- Is het verplicht mijn adreswijziging door te geven?
- Moet ik mij inschrijven op mijn nieuwe adres?
- Is inschrijven op je nieuwe adres verplicht?
- Ben ik verplicht om mijn verhuizing te melden?
- Moet ik doorgeven dat ik verhuisd ben?

## intent:product_uittreksel-verhuismelding_proofregistration
- Hoe kan ik aantonen dat ik ergens ingeschreven sta?
- Wat is een bewijs van inschrijving?
- Kan ik een uittreksel krijgen waarop staat waar ik woon?
- Hoe kan ik bewijzen dat ik ergens ingeschreven ben? 
- Kan ik een bewijs van inschrijving krijgen?

## intent:product_verhuismelding_applydigid
- Ik heb geen DigiD. 
- DigiD, dat heb ik niet. 
- Dat heb ik niet. (vervolgvraag)
- Heb ik niet. (vervolgvraag)
- Heb geen DigiD. 

## intent:product_verhuismelding_dontwantdigid
- Ik wil geen gebruik maken van DigiD.
- Ik vertrouw DigiD niet., Ik wil geen DigiD gebruiken.
- Ik vind DigiD stom. 
- DigiD wil ik niet. 
- Ik ben tegen digid. 

## intent:product_verhuismelding_digidinfo
- Wat is DigiD?
- Ik weet niet wat DigiD is.
- Ik heb geen idee wat DigiD is. 
- Wat is dat? (vervolgvraag)
- DigiD? Nog nooit van gehoord.

## intent:product_verhuismelding_formerror
- Online doorgeven lukt niet. Hoe kan ik nu mijn adreswijziging doorgeven?
- Het formulier werkt niet, wat nu?
- De website loopt vast, hoe geef ik nu mijn verhuizing door?
- Ik begrijp de vragen niet, kan ik mijn verhuizing ook op een andere manier doorgeven
- Ik probeerde mijn verhuizing door te geven maar mijn adres wordt niet herkend. Wat nu?
- Mijn nieuwe adres zit er niet in.
- Ik wilde mijn adres wijzigen maar hij pakt het verkeerde adres. Hoe kan ik nu mijn verhuizing doorgeven?
- Het lukt bij verhuizen niet om mijn adres in te voeren. Hoe geef ik het nu door?
- Mijn adres is geheim, hoe geef ik dan mijn verhuizing door?
- Ik verhuis naar een verzorgingshuis maar het lukt niet online. Hoe doe ik dat nu?

## intent:product_verhuismelding_applyabroad
- Hoe geef ik mijn verhuizing vanuit het buitenland door?
- Ik verhuis naar Nederland. Moet ik mij inschrijven?
- Ik heb al eerder in Nederland gewoond. Moet ik mij nu opnieuw inschrijven?
- Ik woon in het buitenland. Hoe geef ik mijn verhuizing dan door?
- Ik wil naar Nederland verhuizen. Hoe moet ik dit doorgeven?

## intent:product_verhuismelding_emigrate
- Wanneer moet ik me uitschrijven bij de gemeente?
- Ik verhuis naar het buitenland, moet ik me uitschrijven?
- Ik ga naar het buitenland, is het nodig dat ik me uitschrijf?
- Ik ga reizen, moet ik me uitschrijven?
- Moet ik me uitschrijven als ik ga emigreren?
