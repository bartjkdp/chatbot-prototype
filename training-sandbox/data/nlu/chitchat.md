## intent:chitchat
- wanneer is de borrel
- ik wil een biertje bestellen
- mag ik een biertje
- mag ik een pilsje
- een vaasje aub
- een vaasje
- een fluitje
- een La Trappe blond aub
- een Hertog Jan
- ik heb zin in bier
- ik wil een biertje

## intent:thanks
- Bedankt!
- bedankt
- Super!
- dank je
- dankje
- thanks
- dank u
- dank u vriendelijk
- enorm bedankt!
- fijn, dankje
- bedankt he
- bedankt voor de hulp

## intent:unfriendly
- klote bot
- stomme bot
- stomme chatbot
- klote chatbot
- rot op
- Rot op
- ROT OP
- FUCK YOU
- fuck you
- stomme gem
- ROT OP GEM
- ga toch weg robot

## intent:unfriendly_questions
- ben je dom ofzo
- Doe niet zo dom.
- waarom snap je niets
- je snapt ook niets
- domme bot
- Domme Gem

## intent:ask_directeurvdp
- Wie is de directeur van de VDP?
- wie is de directeur van VDP?
- wie is de directeur van de vdp?
- directeur VDP

## intent:ask_directeur
- Wie is de directeur?
- wie is de directeur?
- wie is de directeur van de hackaton?
- wie is de directeur van zaakgerichtwerken?
- wie is de directeur van ZGW?

## intent:ask_burgemeestertilburg
- Wie is de burgemeester in [Tilburg]?
- Burgemeerster [Tilburg]?
- wie is de burgermeester van [Tilburg]?
- wie is de burgermeester van [tilburg](municipality:Tilburg)?
- wie is de burgervader van [Tilburg]?
- wie is de voorzitter van de gemeenteraad in [tilburg](municipality:Tilburg)?
- Wie is de baas van de politie in [Tilburg]?

## intent:ask_burgemeesterdongen
- Wie is de burgemeester in [Dongen]?
- Wie is de burgemeester van [Dongen]?
- burgemeester [Dongen]?
- Weet jij wie de burgemeester is in [Dongen]
- wie is de burgervader van [dongen](municipality:Dongen)
- wie is de voorzitter van de gemeenteraad in [Dongen]?
- wie zit de gemeenteraad voor in [Dongen]?

## intent:ask_burgemeesterutrecht
- Wie is de burgemeester in [Utrecht]?
- Wie is de burgemeester in [utrecht](municipality:Utrecht)
- Wie is de burgemeester van [utrecht](municipality:Utrecht)?
- burgemeester van [utrecht](municipality:Utrecht)?
- Weet jij wie de burgemeester is in [Utrecht]?
- wie is de burgervader van [utrecht](municipality:Utrecht)?
- wie is de voorzitter van de gemeenteraad in [utrecht](municipality:Utrecht)?
- Wie is de baas van de politie in [Utrecht]?

## intent:ask_population
- Hoeveel inwoners heeft nederland?
- weet jij hoeveel mensen er in Nederland wonen?
- hoeveel nederlanders zijn er
- inwoners nederland
- hoeveel inwoners heeft nederland
- Hoeveel inwoners hebben we in Nederland
- hoeveel mensen wonen er in Nederland