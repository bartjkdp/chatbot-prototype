# intent:product_id-kaart_info
#- id-kaart
#- ik wil een [idkaart](product:id-kaart) aanvragen.
#- is een [idkaart](product:id-kaart) geldig als [idbewijs](product:id-kaart)?
#- is een [identiteitskaart](product:id-kaart) geldig als identiteitsbewijs?
#- is een [idkaart](product:id-kaart)  ook een identificatiedocument?
#- kan ik mijn [identiteitskaart](product) gebruiken als legitimatiebewijs?
#- is een [identiteitskaart](product) ook een reisdocument?
#- is een [identiteitskaart](product) ook geldig als legitimatie?
#- is een [identiteitskaart](product) hetzelfde als een legitimatiekaart?
#- is de [identiteitskaart](product) ook te verkrijgen als pas?
#- is een idkaart geldig als idbewijs?
#- is een identiteitskaart geldig als identiteitsbewijs?
#- is een idkaart  ook een identificatiedocument?
#- kan ik mijn identiteitskaart gebruiken als legitimatiebewijs?
#- is een identiteitskaart ook een reisdocument?
#- is een identiteitskaart ook geldig als legitimatie?
#- is een identiteitskaart hetzelfde als een legitimatiekaart?
#- is de identiteitskaart ook te verkrijgen als pas?
#- kan ik de idkaart gebruiken als persoonsbewijs?
#- is een identiteitskaart te gebruiken als een idpas?
#- is een identiteitskaart te gebruiken als een id-pas?
#- kan ik mij met een identiteitskaart identificeren?
#- naar welke landen kan ik reizen met een identiteitskaart?
#- moet ik een afspraak maken om mijn identiteitskaart aan te vragen?
#- kan ik een afspraak maken  om mijn identiteitskaart op te halen?
#- is een identiteitskaart goedkoper dan een paspoort?
#- hoe lang duurt het voordat ik mijn identiteitskaart heb?
#- hoe lang is een idkaart geldig?
#- Mijn idkaart is gestolen, wat moet ik nu doen?
#- is een idkaart overal aan te vragen of moet dit in je eigen gemeente?
#- ik woon in het buitenland, kan ik dan ook een NIK aanvragen?
#- ik wil mijn idkaart verlengen
#- wat kost een idkaart?
#- mijn identiteitskaart is verlopen, kan ik dan nog reizen?

# intent:product_id-kaart_aanvragen
#- id-kaart aanvragen
#- ik wil een [idkaart](product:id-kaart) aanvragen.
#- ik wil een [id-kaart](product) aanvragen.
#- ik wil een [identiteitskaart](product) aanvragen
#- ik moet binnen 5 dagen weg, kan ik met spoed een [id-kaart](product) aanvragen?
#- ik ben mijn [idkaart](product) kwijt, kan ik een nieuwe aanvragen?
#- ik woon in het buitenland, kan ik dan ook een [idkaart](product) aanvragen?
#- ik woon in het buitenland, kan ik dan ook een Nederlandse [identiteitskaart](product) aanvragen?
#- ik woon in het buitenland, kan ik dan ook een [idkaart](product) aanvragen?
#- dit is een test


#BLOK A: Kosten

## intent:product_id-kaart_costs
- Wat kost een [idkaart](product:id-kaart)?
- Hoeveel kost een [idkaart](product:id-kaart)?
- Wat betaal ik voor een [idkaart](product:id-kaart)?
- Hoe duur is een id-kaart?
- Hoeveel moet ik voor een id-kaart betalen?
- Wat is de prijs van een id-kaart?
- Wat betaalt men voor een id-kaart?

## intent:product_id-kaart_costsurgent
- Wat kost het als ik het id-kaart sneller nodig heb?
- Wat zijn de kosten van een spoedaanvraag voor een id-kaart?
- Wat kost een spoedaanvraag id-kaart?
- Wat betaal ik als ik het id-kaart met spoed aanvraag?
- Hoeveel meer kost een spoedaanvraag id-kaart?
- Wat is de prijs van een spoedaanvraag voor een id-kaart?
- Wat zijn de kosten als ik een id-kaart met spoed nodig heb?


# Blok B: Hoe aanvragen

## intent:product_id-kaart_applyinfo
- Hoe kom ik aan een id-kaart? 
- Kan ik een id-kaart aanvragen?
- Hoe vraag ik een id-kaart aan?
- Ik wil een id-kaart aanvragen, hoe doe ik dat?
- Hoe krijg ik een id-kaart?
- Kan ik online een id-kaart aanvragen?

## intent:product_id-kaart_urgentinfo
- Kan ik een id-kaart met spoed aanvragen ?
- Hoe kan ik een id-kaart met spoed aanvragen?
- Kan ik versneld een id-kaart aanvragen?
- Ik wil mijn id-kaart eerder hebben, kan dat? 
- Ik wil snel een id-kaart aanvragen.

## intent:product_id-kaart_eta
- Kunt u kijken of mijn id-kaart al klaar ligt?
- Hoe snel kan ik een nieuwe id-kaart hebben? 
- Wanneer is het id-kaart klaar?
- Wanneer kan ik het id-kaart ophalen?
- Hoe lang duurt het voor mijn id-kaart klaar is?
- Kan ik mijn id-kaart al ophalen? 
- Is mijn id-kaart al klaar?
- Hoe snel kan ik een nieuw id-kaart hebben? 
- Hoe lang duurt het voordat ik een nieuw id-kaart heb?
- Hoe lang duurt het aanvragen van een id-kaart? 
- Duurt het lang voordat ik een nieuw id-kaart heb?

## intent:product_id-kaart_appointment
- Kan ik een afspraak maken om aan een id-kaart te komen?
- Hoe maak ik een afspraak om id-kaart aan te vragen?
- Ik wil een id-kaart aanvragen aan de balie, hoe maak ik een afspraak?
- Kan ik online een afspraak maken voor het aanvragen van een id-kaart? 
- Ik wil een id-kaart aanvragen. 

## intent:product_id-kaart_necessities
- Wat moet ik meenemen bij de aanvraag van een id-kaart?
- Moet ik iets meenemen als ik een id-kaart aanvraag? 
- Wat ik heb ik nodig als ik een id-kaart aan wil vragen?
- Wat zijn de benodigdheden voor als ik een id-kaart aan wil vragen?
- Wat moet ik bij me hebben als ik een id-kaart aan ga vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een id-kaart?

## intent:product_id-kaart_photo
- Waar kan ik pasfoto's laten maken voor een id-kaart?
- Wat voor pasfoto moet ik meenemen voor id-kaart?
- Wat zijn de regels voor een foto voor een id-kaart?
- Aan welke eisen moet een pasfoto voor id-kaart voldoen?
- Wat zijn de eisen van een pasfoto voor mijn id-kaart?
- Waar moet ik een pasfoto laten maken voor mijn id-kaart?
- Welke pasfoto moet er op mijn id-kaart?

## intent:product_id-kaart_collect
- Hoe kan ik een id-kaart ophalen? 
- Waar kan ik mijn id-kaart afhalen?
- Moet ik een afspraak maken voor het afhalen van mijn id-kaart?
- Kan ik mijn id-kaart ophalen zonder afspraak? 
- Kan ik een id-kaart zonder afspraak op komen halen?
- Is het mogelijk om een id-kaart zonder afspraak op te halen?

## intent:product_id-kaart_noticket
- Kan ik het id-kaart afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het id-kaart ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het id-kaart kwijt, wat nu? 
- Kan ik mijn id-kaart zonder bewijs ophalen? 
- Is het afhalen van mijn id-kaart mogelijk zonder afhaalbewijs?
- Kan ik het [idkaart](product:id-kaart) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het [idkaart](product:id-kaart) ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het [identiteitskaart](product:id-kaart) kwijt, wat nu? 
- Kan ik mijn [id](product:id-kaart) zonder bewijs ophalen? 
- Is het afhalen van mijn [identiteitskaart](product:id-kaart) mogelijk zonder afhaalbewijs?

## intent:product_id-kaart_delivery
- Wordt een id-kaart thuisbezorgd? 
- Ik kan mijn id-kaart niet ophalen, kan die opgestuurd worden?
- Kan het id-kaart opgestuurd worden?
- Hoe duur is het om een id-kaart thuis te laten bezorgen?
- Versturen jullie id-kaart ook per post?

## intent:product_id-kaart_abroad
- Hoe kom ik aan een id-kaart als ik in het buitenland woon?
- Waar haal ik mijn id-kaart in het buitenland?
- Kan ik een id-kaart ook in het buitenland aanvragen?
- Ik woon in het buitenland, hoe kom ik aan mijn id-kaart?
- Hoe vraag ik een id-kaart in het buitenland aan?

## intent:product_id-kaart_notregistered
- Hoe moet ik een Nederlands id-kaart aanvragen als ik nergens sta ingeschreven?
- Ik wil een id-kaart aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Ik wil me inschrijven en een id-kaart aanvragen, kan dat? 
- Ik sta nergens ingeschreven, hoe kom ik aan een id-kaart? 
- Hoe vraag je een id-kaart aan als je niet ingeschreven staat?

## intent:product_id-kaart_requirements
- Wie kan een id-kaart aanvragen? 
- Hoe oud moet je zijn om een id-kaart aan te vragen? 
- Mag iedereen een id-kaart aanvragen?
- Wanneer mag je een id-kaart aanvragen? 
- Vanaf welke leeftijd mag je een id-kaart aanvragen?

## intent:product_id-kaart_otherperson
- Kan iemand anders mijn id-kaart aanvragen of ophalen?
- Hoe kan ik een id-kaart aanvragen als ik niet langs kan komen?
- Ik kan niet naar de balie komen. Hoe vraag ik een id-kaart aan? 
- Ik ben slecht ter been en kan mijn id-kaart daardoor niet ophalen. 
- Ik heb last van mijn gezondheid en kan daardoor mijn id-kaart niet ophalen.

## intent:product_id-kaart_fingerprint
- Is een vingerafdruk verplicht voor een id-kaart?
- Moet ik een vingerafdruk zetten voor een id-kaart?
- Is het nodig dat ik een vingerafdruk laat maken voor een id-kaart? 
- Wat doen jullie met mijn vingerafdrukken? 
- Waarvoor hebben jullie mijn vingerafdrukken nodig? 
- Waarom zijn mijn vingerafdrukken nodig?




# Blok C: Kwijt

## intent:product_id-kaart_lost
- mijn [id-kaart](product:id-kaart) is kwijt. wat moet ik doen?
- ik ben mijn [idkaart](product:id-kaart) verloren. wat moet ik doen?
- mijn [id-kaart](product:id-kaart) is weg.
- mijn [idkaart](product:id-kaart) is weg. wat moet ik doen?
- mijn [id-kaart](product:id-kaart) is gestolen. Wat moet ik doen?
- Wat moet ik doen als mijn id-kaart kwijt, vermist of gestolen is?
- Mijn id-kaart is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?
- Moet ik het doorgeven als mijn id-kaart kwijt is?
- Het id-kaart van mijn kind is kwijt, wat moet ik doen?
- Ik kan mijn id-kaart niet meer vinden, wat nu?

## intent:product_id-kaart_found 
- Ik heb mijn id-kaart teruggevonden, mag ik die weer gebruiken?
- Mijn verloren id-kaart is weer opgedoken. Wat doe ik daarmee?
- Ik vond mijn id-kaart weer, wat nu?
- Wat moet ik doen als ik mijn id-kaart weer heb teruggevonden?
- Mijn id-kaart vond ik zojuist weer terug.

## intent:product_id-kaart_lostabroad
- Ik heb mijn id-kaart in het buitenland verloren. Wat moet ik doen?
- Mijn id-kaart is gestolen in het buitenland. Wat nu?
- Ik ben mijn id-kaart kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?
- Waar vraag ik een tijdelijk id-kaart in het buitenland aan?
- Ik ben mijn id-kaart verloren op vakantie. Wat moet ik doen?


# Blok D: Geldigheid

## intent:product_id-kaart_validity
- Hoe lang is een id-kaart geldig?
- Hoeveel jaar is mijn id-kaart geldig?
- Tot wanneer is een id-kaart geldig
- Na hoeveel jaar is mijn id-kaart verlopen?
- Wanneer verloopt mijn id-kaart?

## intent:product_id-kaart_validityabroad
- Waar is een id-kaart geldig?
- In welke landen is een id-kaart geldig?
- Naar welke landen mag ik met mijn id-kaart?
- Waar ter wereld is mijn id-kaart geldig?
- Welke landen erkennen mijn id-kaart?

# Blok E: Kind

## intent:product_id-kaart_childnecessities
- Wat moet ik meenemen als ik een id-kaart aan vraag voor mijn kind?
- Wat heb ik nodig als ik voor mijn kind een id-kaart aan wil vragen?
- Wat moet ik doen als ik voor mijn dochter een id-kaart wil aanvragen?
- Wat heb ik nodig als ik voor mijn zoon een id-kaart ga aanvragen?
- Wat moet ik hebben als ik een id-kaart voor mijn kind wil halen?


