## intent:product_trouwen_info
- ik ga trouwen
- trouwen
- Onze trouwdag is over 3 weken en we hebben de trouwambtenaar nog niet gesproken
- Hoe kunnen wij een trouwdatum vastleggen?
- Mogen we ons trouwfeest in de tuin doen?
- Mogen we zelf een trouwlocatie kiezen?
- Is de trouwzaal beschikbaar?
- Hoe kunnen we een voorgenomen huwelijk melden?
- Hoeveel getuigen mogen we hebben bij het trouwen?
- Wij gaan trouwen, kan ik een datum vastleggen?

