# intent:product_paspoort_info
#- paspoort
#- ik wil een paspoort aanvragen
#- moet ik mijn oude reisdocument ook meenemen?
#- is mijn paspoort te gebruiken als legitimatiebewijs?
#- is een paspoort ook een persoonsbewijs?
#- kan ik een reispas aanvragen?
#- is een paspoort een legitimatiebewijs?
#- kan ik ook een zakenpaspoort aanvragen?
#- ik ben mijn paspoort kwijt, kan ik een noodpaspoort krijgen?
#- kan ik ook een tweede paspoort aanvragen?
#- is mijn paspoort te gebruiken als legitimatiebewijs?
#- is een paspoort ook een persoonsbewijs?
#- kan ik een reispas aanvragen?
#- is een paspoort een legitimatiebewijs?
#- kan ik ook een zakenpaspoort aanvragen?
#- ik ben mijn paspoort kwijt, kan ik een noodpaspoort krijgen?
#- moet ik mijn oude reisdocument ook meenemen?
#- kan ik een vluchtelingenpaspoort aanvragen?
#- kan ik een vreemdelingenpaspoort aanvragen?
#- wanneer kan ik mijn paspoort ophalen?
#- hoe lang is een paspoort geldig?
#- ik ben mijn paspoort kwijt, kan ik een noodpaspoort krijgen?
#- ik ben mijn paspoort verloren, wat moet ik nu doen?
#- mijn paspoort is gestolen, wat moet ik doen? 
#- moet mijn kind een eigen paspoort?
#- ik woon in het buitenland, kan ik dan ook een paspoort aanvragen?
#- wat kost een paspoort?
#- mijn paspoort is verlopen, kan ik dan nog reizen?
#- ik wil mijn paspoort verlengen

# intent:product_paspoort_aanvragen
#- paspoort aanvragen
#- ik wil een paspoort aanvragen
#- ik wil graag een paspoort aanvragen
#- ik wil graag mijn paspoort vernieuwen
#- ik wil een paspoort aanvragen
#- kan ik ook een tweede paspoort aanvragen?
#- ik moet binnen 5 dagen weg, kan ik met spoed een paspoort aanvragen?
#- moet ik een afspraak maken voor het aanvragen van een paspoort?
#- is een paspoort overal aan te vragen of moet dit in je eigen gemeente?
#- Ik wil een paspoort aanvragen
#- Kan ik nu mijn paspoort aanvragen?

## intent: product_paspoort_general
- paspoort
- ik wil meer informatie over paspoort
- mijn vraag gaat over paspoort
- ik heb een vraag over mijn paspoort
- kan je mij informatie geven over paspoort
- ik heb je hulp. het gaat over mijn paspoort

#BLOK A: Kosten

## intent:product_paspoort_costs
- Wat kost een paspoort?
- Wat kost een paspoort in [Tilburg](municipality)?
- Wat kost een paspoort in [Loon op Zand](municipality)?
- Hoeveel kost een paspoort?
- Hoeveel kost een paspoort in [Utrecht](municipality)?
- Wat betaal ik voor een paspoort?
- Wat betaal ik voor een paspoort in [Tilburg](municipality)?
- [Hoe duur is een paspoort?]
- Hoe duur is een paspoort in [Dongen](municipality)?
- Hoeveel moet ik voor een paspoort betalen?
- Hoeveel moet ik voor een paspoort betalen in [Tilburg](municipality)?
- Wat is de prijs van een paspoort?
- Wat is de prijs van een paspoort in [dongen](municipality:Dongen)
- Wat betaalt men voor een paspoort?
- Wat betaalt men voor een paspoort in [Tilburg](municipality)?


## intent:product_paspoort_costsurgent
- Wat kost het als ik het paspoort sneller nodig heb?
- Wat kost het als ik het paspoort sneller nodig heb in [Dongen](municipality)?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort in [Tilburg](municipality)?
- Wat kost een spoedaanvraag paspoort?
- Wat kost een spoedaanvraag paspoort in [Utrecht](municipality)?
- Wat betaal ik als ik het paspoort met spoed aanvraag?
- Wat betaal ik als ik het paspoort met spoed aanvraag in [Ede](municipality)?
- Hoeveel meer kost een spoedaanvraag paspoort?
- Hoeveel meer kost een spoedaanvraag paspoort in [De Bilt](municipality)?
- Wat is de prijs van een spoedaanvraag voor een paspoort?
- Wat is de prijs van een spoedaanvraag voor een paspoort in [Amsterdam](municipality)?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb in [Eindhoven](municipality)?

## intent:product_paspoort_paymentmethod
- Kan ik met pinpas betalen?
- Kan ik met cash betalen?
- Kan ik met contant geld betalen?
- Hoe kan ik betalen bij de balie?
- Kan ik pinnen?
- Welke betaalmethode bieden jullie aan?
- Hoe kan ik betalen?
- Kan ik met briefgeld betalen?
- Is het mogelijk om met creditcard te betalen?


# Blok B: Hoe aanvragen

## intent:product_paspoort_applyinfo
- Hoe kom ik aan een paspoort? 
- kan ik mijn paspoort verlengen?
- Kan ik een paspoort aanvragen in [Utrecht](municipality)?
- Hoe vraag ik een paspoort aan?
- Ik wil een paspoort aanvragen, hoe doe ik dat?
- Hoe krijg ik een paspoort in [Amsterdam](municipality)?
- Kan ik online een paspoort aanvragen?

## intent:product_paspoort_urgentinfo
- Kan ik een paspoort met spoed aanvragen?
- Hoe kan ik een paspoort met spoed aanvragen?
- Kan ik versneld een paspoort aanvragen?
- Ik wil mijn paspoort eerder hebben, kan dat? 
- Ik wil snel een paspoort aanvragen.

## intent:product_paspoort_eta
- Kunt u kijken of mijn paspoort al klaar ligt?
- Wanneer is het paspoort klaar?
- Wanneer kan ik het paspoort ophalen?
- Hoe lang duurt het voor mijn paspoort klaar is?
- Kan ik mijn paspoort al ophalen? 
- Is mijn paspoort al klaar?

## intent:product_paspoort_etanotapplied
- Hoe snel kan ik een nieuw paspoort hebben? 
- Hoe lang duurt het voordat ik een nieuw paspoort krijg?
- Hoe lang duurt het voordat ik een nieuw paspoort heb?
- Hoe lang duurt het aanvragen van een paspoort? 
- Duurt het lang voordat ik een nieuw paspoort heb?

## intent:product_paspoort_appointment
- Kan ik een afspraak maken om aan een paspoort te komen in [Dongen](municipality)?
- Hoe maak ik een afspraak om paspoort aan te vragen?
- Ik wil een paspoort aanvragen aan de balie, hoe maak ik een afspraak in [Amsterdam](municipality)?
- Kan ik online een afspraak maken voor het aanvragen van een paspoort? 
- Ik wil een paspoort aanvragen in [Tilburg](municipality).
- Ik wil graag een afspraak maken voor het aanvragen van een paspoort.

## intent:product_paspoort_necessities
- Wat moet ik meenemen bij de aanvraag van een paspoort?
- Moet ik iets meenemen als ik een paspoort aanvraag? 
- Wat ik heb ik nodig als ik een paspoort aan wil vragen?
- Wat zijn de benodigdheden voor als ik een paspoort aan wil vragen?
- Wat moet ik bij me hebben als ik een paspoort aan ga vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een paspoort?

## intent:product_paspoort_photo
- Waar kan ik pasfoto's laten maken voor een paspoort?
- Wat voor pasfoto moet ik meenemen voor paspoort?
- Wat zijn de regels voor een foto voor een paspoort?
- Aan welke eisen moet een pasfoto voor paspoort voldoen?
- Wat zijn de eisen van een pasfoto voor mijn paspoort?
- Waar moet ik een pasfoto laten maken voor mijn paspoort?
- Welke pasfoto moet er op mijn paspoort?

## intent:product_paspoort_collect
- Hoe kan ik een paspoort ophalen in [Amsterdam](municipality)? 
- Waar kan ik mijn paspoort afhalen?
- Moet ik een afspraak maken voor het afhalen van mijn paspoort in [Dongen](municipality)?
- Kan ik mijn paspoort ophalen zonder afspraak in [Ede](municipality)? 
- Kan ik een paspoort zonder afspraak op komen halen?
- Is het mogelijk om een paspoort zonder afspraak op te halen?

## intent:product_paspoort_noticket
- Kan ik het paspoort afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het paspoort ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het paspoort kwijt, wat nu? 
- Kan ik mijn paspoort zonder bewijs ophalen? 
- Is het afhalen van mijn paspoort mogelijk zonder afhaalbewijs?

## intent:product_paspoort_delivery
- Wordt een paspoort thuisbezorgd in [De Bilt](municipality)? 
- Ik kan mijn paspoort niet ophalen, kan die opgestuurd worden?
- Kan het paspoort opgestuurd worden?
- Hoe duur is het om een paspoort thuis te laten bezorgen in [Dongen](municipality)?
- Versturen jullie paspoort ook per post in [Tilburg](municipality)?

## intent:product_paspoort_abroad
- Hoe kom ik aan een paspoort als ik in het buitenland woon?
- Waar haal ik mijn paspoort in het buitenland?
- Kan ik een paspoort ook in het buitenland aanvragen?
- Ik woon in het buitenland, hoe kom ik aan mijn paspoort?
- Hoe vraag ik een paspoort in het buitenland aan?

## intent:product_paspoort_notregistered
- Hoe moet ik een Nederlands paspoort aanvragen als ik nergens sta ingeschreven?
- Ik wil een paspoort aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Ik wil me inschrijven en een paspoort aanvragen, kan dat? 
- Ik sta nergens ingeschreven, hoe kom ik aan een paspoort? 
- Hoe vraag je een paspoort aan als je niet ingeschreven staat?

## intent:product_paspoort_requirements
- Wie kan een paspoort aanvragen? 
- Hoe oud moet je zijn om een paspoort aan te vragen? 
- Mag iedereen een paspoort aanvragen?
- Wanneer mag je een paspoort aanvragen? 
- Vanaf welke leeftijd mag je een paspoort aanvragen?

## intent:product_paspoort_otherperson
- Kan iemand anders mijn paspoort aanvragen of ophalen?
- Hoe kan ik een paspoort aanvragen als ik niet langs kan komen?
- Ik kan niet naar de balie komen. Hoe vraag ik een paspoort aan? 
- Ik ben slecht ter been en kan mijn paspoort daardoor niet ophalen. 
- Ik heb last van mijn gezondheid en kan daardoor mijn paspoort niet ophalen.

## intent:product_paspoort_fingerprint
- Is een vingerafdruk verplicht voor een paspoort?
- Moet ik een vingerafdruk zetten voor een paspoort?
- Is het nodig dat ik een vingerafdruk laat maken voor een paspoort? 
- Wat doen jullie met mijn vingerafdrukken? 
- Waarvoor hebben jullie mijn vingerafdrukken nodig? 
- Waarom zijn mijn vingerafdrukken nodig?

# Blok C: Kwijt

## intent:product_paspoort_lost
- mijn paspoort is kwijt. wat moet ik doen?
- ik ben mijn paspoort verloren. wat moet ik doen?
- mijn paspoort is weg.
- mijn paspoort is weg. wat moet ik doen?
- mijn paspoort is gestolen. Wat moet ik doen?
- Wat moet ik doen als mijn paspoort kwijt, vermist of gestolen is?
- Mijn paspoort is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?
- Moet ik het doorgeven als mijn paspoort kwijt is?
- Het paspoort van mijn kind is kwijt, wat moet ik doen?
- Ik kan mijn paspoort niet meer vinden, wat nu?
- Ik ben mijn paspoort kwijt?

## intent:product_paspoort_lostreport
- Moet ik geen aangifte bij de politie doen?
- Moet ik het ook aan de politie doorgeven?
- Moet ik ook naar de politie als ik mijn paspoort ben verloren?
- Is het verplicht om naar de politie te gaan?
- Is het nodig om bij de politie aangifte te doen?


## intent:product_paspoort_found 
- Ik heb mijn paspoort teruggevonden, mag ik die weer gebruiken?
- Mijn verloren paspoort is weer opgedoken. Wat doe ik daarmee?
- Ik vond mijn paspoort weer, wat nu?
- Wat moet ik doen als ik mijn paspoort weer heb teruggevonden?
- Mijn paspoort vond ik zojuist weer terug.

## intent:product_paspoort_lostabroad
- Ik heb mijn paspoort in het buitenland verloren. Wat moet ik doen?
- Mijn paspoort is gestolen in het buitenland. Wat nu?
- Ik ben mijn paspoort kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?
- Waar vraag ik een tijdelijk paspoort in het buitenland aan?
- Ik ben mijn paspoort verloren op vakantie. Wat moet ik doen?

## intent:product_paspoort_emergency
- Wat doe ik als mijn paspoort kwijt is en ik moet vandaag nog weg?
- Ik heb een noodpaspoort nodig, hoe regel ik dat?
- Waar vraag ik een noodpaspoort aan?
- Hoe vraag ik een noodpaspoort aan?
- Ik wil een noodpaspoort aanvragen.




# Blok D: Geldigheid

## intent:product_paspoort_validity
- Hoe lang is een paspoort geldig?
- Hoeveel jaar is mijn paspoort geldig?
- Tot wanneer is een paspoort geldig
- Na hoeveel jaar is mijn paspoort verlopen?
- Wanneer verloopt mijn paspoort?

## intent:product_paspoort_validityabroad
- Waar is een paspoort geldig?
- In welke landen is een paspoort geldig?
- Naar welke landen mag ik met mijn paspoort?
- Waar ter wereld is mijn paspoort geldig?
- Welke landen erkennen mijn paspoort?




# Blok E: Kind

## intent:product_paspoort_childnecessities
- Wat moet ik meenemen als ik een paspoort aan vraag voor mijn kind?
- Wat heb ik nodig als ik voor mijn kind een paspoort aan wil vragen?
- Wat moet ik doen als ik voor mijn dochter een paspoort wil aanvragen?
- Wat heb ik nodig als ik voor mijn zoon een paspoort ga aanvragen?
- Wat moet ik hebben als ik een paspoort voor mijn kind wil halen?

## intent:product_paspoort_childadd
- Kan ik mijn kind nog laten bijschrijven in mijn paspoort?
- Mag mijn zoon/dochter in mijn paspoort bijgeschreven worden?
- Ik wil mijn kind in mijn paspoort bijschrijven, kan dat?
- Hoe kan ik mijn kind laten bijschrijven in mijn paspoort?
- Ik wil graag mijn kind laten bijschrijven in m'n paspoort.

## intent:product_paspoort_childabroad
- Wat heb ik nodig als eenoudergezin om mijn kind mee naar het buitenland te nemen?
- Heb ik iets nodig om met mijn kind naar het buitenland te reizen?
- Ik wil op vakantie met mijn kind, wat moet ik regelen?
- Ik wil met mijn kind naar het buitenland.
- Kan ik mijn kind meenemen naar het buitenland?

## intent:product_paspoort_childpermission
- Wat moet ik doen als de andere ouder geen toestemming wil geven om te reizen?
- Mijn ex-partner geeft geen toestemming om met mijn kind te reizen.
- Mijn ex saboteert mijn vakantie met mijn kind. Wat moet ik doen?
- Ik krijg geen toestemming van mijn ex om met mijn kind op vakantie te gaan.
- Ik wil met mijn kind op vakantie gaan, maar mijn vorige partner geeft hiervoor geen toestemming.



