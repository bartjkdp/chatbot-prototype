## intent:inform
- [Tilburg](municipality)
- [tilburg](municipality:Tilburg)
- [Utrecht](municipality)
- [utrecht](municipality:Utrecht)
- [Amsterdam](municipality)
- [amsterdam](municipality:Amsterdam)
- [Dongen](municipality)
- [dongen](municipality:Dongen)
- Gemeente [Tilburg](municipality)
- [Utrecht](municipality)
- Gemeente [Rotterdam](municipality)
- [Rotterdam](municipality)
- Dat is [Dongen](municipality)
- Voor [Amsterdam](municipality)
- In [Ede](municipality)
- [almere](municipality:Almere)
- [Almere](municipality)
- [udenhout](municipality:Tilburg)
- [berkel](municipality:Tilburg)
- [enschot](municipality:Tilburg)
- [Berkel-Enschot](municipality:Tilburg)
- [Biezenmortel](municipality:Tilburg)
- [biezenmortel](municipality:Tilburg)
- [Udenhout](municipality:Tilburg)
- [dongen](municipality:Dongen)
- ['s Gravenmoer](municipality:Dongen)
- [Vaart](municipality:Dongen)
- [Klein Dongen](municipality:Dongen)
- [klein dongen](municipality:Dongen)
- [vaart](municipality:Dongen)
- [sgravenmoer](municipality:Dongen)
- ['sgravenmoer](municipality:Dongen)
- [Arnhem](municipality)
- [Amsterdam](municipality)
- [amsterdam]
- [De bilt](municipality)
- [De Bilt](municipality)
- [Buren](municipality)
- [buren]
- [Best](municipality)
- [best]
- [Ede](municipality)
- [ede]
- [Eindhoven](municipality)
- [eindhoven]
- [Enschede](municipality)
- [enschede]
- [Tilburg](municipality)
- [tilburg]
- [Utrecht](municipality)
- [utrecht]

## intent:product_id-kaart_validitycountry
- Is een id-kaart geldig in [Zwitserland](idcountries)
- Mag ik met mijn idkaart naar [Noord-Korea](noidcountries) reizen?
- Kan ik met een idkaart naar [Marokko](noidcountries)?
- Is het mogelijk om met een id-kaart naar [Polen](idcountries) te reizen?
- Accepteert [Duitsland](idcountries) een id-kaart?
- Is mijn idkaart geldig in [Iran](noidcountries)?
- Kan ik met mijn id kaart naar [China](noidcountries)?
- Wordt mijn idkaart geaccepteerd in [Italië](idcountries)?
- Geldt mijn idkaart in [Irak](noidcountries)?
- Is een idkaart een geldig document in [Griekenland](idcountries)?
- Is een id-kaart voldoende als ik op vakantie ga naar [Rusland](noidcountries)?
- Kan ik op vakantie naar [Brazilie](noidcountries:Brazilië) met een idkaart?

## intent:affirmative
- ja
- ja, graag
- graag
- aub
- yes please!
- ok
- akkoord
- prima
- dat zou mooi zijn
- helemaal geweldig
- alsjeblieft

## intent:denial
- nee
- nee, bedankt
- nope
- neen
- alsjeblieft niet
- zeker niet
- echt niet

## intent:greet
- hallo [Tilburg](municipality)!
- Hallo [Utrecht](municipality)! Mijn naam is [Piet](name)
- hey gem, mijn naam is [jason](name)
- Hoi mijn naam is [Wim](name)
- Hi, ik ben [Harvey](name)
- Hallo hier [Ali](name)
- Hey, ik ben [Jeroen](name)
- Hallo, [Emma](name) hier!
- Hallo [Ede](municipality), ik ben [Wendy](name)
- Hallo
- hoi
- hi
- hey
- heey
- heuy
- goede morgen
- goede middag
- goedenavond
- goedenavond [Utrecht](municipality)
- Hoi [Dongen](municipality)
- Hoi [Uden](municipality)

## intent: ask_greet
- alles goed?
- hoe gaat het met je?
- hoe was je dag?
- hoe is je dag?
- gaat alles goed vandaag?
- how are you doing?
- how are you?
- all good?
- are you doing good?

## intent:dontknow
- dat weet ik niet
- geen idee
- ?
- zou het niet weten
- daar vraag je me wat
- Joost mag het weten

## intent:cancel
- annuleren
- stop
- ik wil stoppen
- ik wil afbreken aub
- kunnen we stoppen?

## intent:restart
- Opnieuw
- opnieuw
- restart
- Herstart
- Reboot
- Ik wil opnieuw beginnen
- Hoe start ik opnieuw?
- opnieuw invoeren

## intent:ask_for_human
- Ik wil met een medewerker spreken
- Ga weg robot
- Ga weg Gem
- ga weg gem
- ga weg
- ik wil een mens spreken
- kan ik een mensen spreken
- kan ik een echt gesprek hebben
- Ik wil een mens aan de lijn
- Verbind me even door met een echt mens
- echt mens aub
- ik praat niet met chatbots
- Ik praat niet met bots
- Ik praat niet met robots
- praat met medewerker
- ik wil iemand spreken van burgerzaken
- Ik wil met een mens chatten

## intent:ask_for_chatbot
- ik wil met de chatbot chatten
- ik wil verder met de chatbot
- Chat met de chatbot
- Ga verder met de bot
- Ga door met de chatbot
- Verder praten met de chatbot

## synonym:Almere
- almere

## synonym:Utrecht
- utrecht

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## lookup:municipality
  data/municipalities.txt

## lookup:idcountries
  data/idcountries.txt

## lookup:noidcountries
  data/noidcountries.txt

## lookup:product
- paspoort
- parkeervergunning
- id-kaart
- overlijdensakte
- verhuismelding
- bijstand
- uitkering
- hondenbelasting
- afvalcontainer
- belasting-kwijtschelding
- brp-uittreksel
- trouwen
- afspraak
- afvalkalender
- rijbewijs
- geboorteaangifte
- melding-openbare-ruimte
- reisdoc-costs-urgent
- reisdoc-costs-paymentmethod
