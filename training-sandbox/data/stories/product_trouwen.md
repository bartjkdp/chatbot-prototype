## product_trouwen_info location is unknown 
* product_trouwen_info{"product": "trouwen"}
  - utter_product_trouwen_notlearned
  - utter_ask_location_trouwen
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_trouwen_info location is known 
* product_trouwen_info{"product": "trouwen"}
  - slot{"product": "trouwen"}
  - slot{"municipality": "Tilburg"}
  - utter_product_trouwen_notlearned
  - action_productURL
  - action_listen




