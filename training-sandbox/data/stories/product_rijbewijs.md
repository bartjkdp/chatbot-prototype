## product_rijbewijs_info location is unknown 
* product_rijbewijs_info{"product": "rijbewijs"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_rijbewijs_info location is known 
* product_rijbewijs_info{"product": "rijbewijs"}
  - slot{"product": "rijbewijs"}
  - slot{"municipality": "Tilburg"}
  - utter_product_notlearned
  - action_productURL
  - action_listen


