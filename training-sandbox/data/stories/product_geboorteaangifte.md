## product_geboorteaangifte_info location is unknown 
* product_geboorteaangifte_info{"product": "geboorteaangifte"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_geboorteaangifte_info location is known 
* product_geboorteaangifte_info{"product": "geboorteaangifte"}
  - slot{"product": "geboorteaangifte"}
  - slot{"municipality": "Tilburg"}
  - utter_product_notlearned
  - action_productURL
  - action_listen
  



