## Afspraak location is unknown 
* product_afspraak_info{"product": "afspraak"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## Afspraak location is known
* product_afspraak_info{"product": "afspraak"}
  - slot{"product": "afspraak"}
  - slot{"municipality": "Tilburg"}
  - utter_product_notlearned
  - action_productURL
  - action_listen




