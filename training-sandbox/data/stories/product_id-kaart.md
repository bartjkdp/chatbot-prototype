#### Manuele Stories

### product_id-kaart_validity

## product_id-kaart_validity
* product_id-kaart_validity{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_validity
  - utter_helpmore


### product_id-kaart_validity

## product_id-kaart_validityabroad
* product_id-kaart_validityabroad{"product": "id-kaart"}
  - utter_product_id-kaart_validityabroad
  - utter_helpmore

## product_id-kaart_validityabroad
* product_id-kaart_validityabroad{"product": "id-kaart"}
  - utter_product_id-kaart_validityabroad
  - utter_helpmore


##### KIND

#### Manuele Stories

### product_id-kaart_childnecessities

## product_id-kaart_childnecessities
* product_id-kaart_childnecessities{"product": "id-kaart"}
  - utter_product_id-kaart_childnecessities
  - utter_helpmore


##### KOSTEN

#### Manuele Stories

### product_id-kaart_costs

## product_id-kaart_costs location is unknown
* product_id-kaart_costs{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - utter_product_id-kaart_costs
  - utter_helpmore

## product_id-kaart_costs location is known
* product_id-kaart_costs{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - utter_product_id-kaart_costs
  - utter_helpmore

## product_id-kaart_costs location is known from entity
* product_id-kaart_costs{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - utter_product_id-kaart_costs
  - utter_helpmore

## product_id-kaart_costs location is known from entity without context municipality
* product_id-kaart_costs{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - utter_product_id-kaart_costs
  - utter_helpmore


### product_id-kaart_costsurgent

## product_id-kaart_costsurgent location is unknown
* product_id-kaart_costsurgent{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_costsurgent
  - utter_helpmore

## product_id-kaart_costsurgent location is known
* product_id-kaart_costsurgent{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_costsurgent
  - utter_helpmore

## product_id-kaart_costsurgent location is known from entity
* product_id-kaart_costsurgent{"product": "id-kaart","municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_costsurgent
  - utter_helpmore

## product_id-kaart_costsurgent location is known from entity without context
* product_id-kaart_costsurgent{"product": "id-kaart","municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_costsurgent
  - utter_helpmore


### product_id-kaart_paymentmethod

## product_id-kaart_paymentmethod GENERAL
* product_paspoort_paymentmethod{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_paspoort_paymentmethod
  - utter_helpmore

## product_id-kaart_paymentmethod with context GENERAL
* product_paspoort_paymentmethod{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_paymentmethod
  - utter_helpmore


##### HOE AANVRAGEN

#### Manuele Stories

### product_id-kaart_applyinfo

## product_id-kaart_applyinfo location is unknown
* product_id-kaart_applyinfo{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_applyinfo
  - utter_helpmore

## product_id-kaart_applyinfo location is known 
* product_id-kaart_applyinfo{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_applyinfo
  - utter_helpmore

## product_id-kaart_applyinfo location is known from entity
* product_id-kaart_applyinfo{"product": "id-kaart", "municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_applyinfo
  - utter_helpmore

## product_id-kaart_applyinfo location is known from entity without context
* product_id-kaart_applyinfo{"product": "id-kaart", "municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_applyinfo
  - utter_helpmore


### product_id-kaart_applyinfo

## product_id-kaart_urgentinfo location is unknown
* product_id-kaart_urgentinfo{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_urgentinfo
  - utter_helpmore

## product_id-kaart_urgentinfo location is known
* product_id-kaart_urgentinfo{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_urgentinfo
  - utter_helpmore

## product_id-kaart_urgentinfo location is known from entity
* product_id-kaart_urgentinfo{"product": "id-kaart", "municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_urgentinfo
  - utter_helpmore

## product_id-kaart_urgentinfo location is known without context municipality
* product_id-kaart_urgentinfo{"product": "id-kaart", "municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_urgentinfo
  - utter_helpmore


### product_id-kaart_eta + product_id-kaart_etanotapplied

## product_id-kaart_eta GENERAL
* product_id-kaart_eta{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_eta
  - utter_helpmore

## product_id-kaart_eta GENERAL with context municipality 
* product_id-kaart_eta{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_eta
  - utter_helpmore

## product_id-kaart_etanotapplied GENERAL 
* product_id-kaart_etanotapplied{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_etanotapplied
  - utter_helpmore

## product_id-kaart_etanotapplied GENERAL with context municipality 
* product_id-kaart_etanotapplied{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_etanotapplied
  - utter_helpmore


### product_id-kaart_appointment

## product_id-kaart_appointment location is unknown
* product_id-kaart_appointment{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_appointment
  - utter_helpmore

## product_id-kaart_appointment location is known
* product_id-kaart_appointment{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_appointment
  - utter_helpmore

## product_id-kaart_appointment location is known from entity
* product_id-kaart_appointment{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_appointment
  - utter_helpmore

## product_id-kaart_appointment location is known without context municipality
* product_id-kaart_appointment{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_appointment
  - utter_helpmore


### product_id-kaart_necessities

## product_id-kaart_necessities GENERAL 
* product_id-kaart_necessities{"product": "id-kaart"}
  - utter_product_id-kaart_necessities
  - utter_helpmore

## product_id-kaart_necessities GENERAL with context municipality 
* product_id-kaart_necessities{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_necessities
  - utter_helpmore


### product_id-kaart_photo

## product_id-kaart_photo GENERAL
* product_id-kaart_photo{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_photo
  - utter_helpmore

## product_id-kaart_photo GENERAL with context municipality
* product_id-kaart_photo{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_photo
  - utter_helpmore


### product_id-kaart_collect

## product_id-kaart_collect location is unknown
* product_id-kaart_collect{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_collect
  - utter_helpmore

## product_id-kaart_collect location is known
* product_id-kaart_collect{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_collect
  - utter_helpmore

## product_id-kaart_collect location is known from entity
* product_id-kaart_collect{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_collect
  - utter_helpmore

## product_id-kaart_collect location is known without context municipality
* product_id-kaart_collect{"product": "id-kaart", "municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_collect
  - utter_helpmore


### product_id-kaart_noticket

## product_id-kaart_noticket GENERAL 
* product_id-kaart_noticket{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_noticket
  - utter_helpmore

## product_id-kaart_noticket GENERAL with context municipality
* product_id-kaart_noticket{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_noticket
  - utter_helpmore


### product_id-kaart_delivery

## product_id-kaart_delivery location is unknown
* product_id-kaart_delivery{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_id-kaart_delivery
  - utter_helpmore

## product_id-kaart_delivery location is known
* product_id-kaart_delivery{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_delivery
  - utter_helpmore

## product_id-kaart_delivery location is known from entity
* product_id-kaart_delivery{"product": "id-kaart","municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_delivery
  - utter_helpmore

## product_id-kaart_delivery location is known without context municipality
* product_id-kaart_delivery{"product": "id-kaart","municipality": "Tilburg"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_delivery
  - utter_helpmore


### product_id-kaart_abroad

## product_id-kaart_abroad GENERAL 
* product_id-kaart_abroad{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_abroad
  - utter_helpmore

## product_id-kaart_abroad GENERAL with context
* product_id-kaart_abroad{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_abroad
  - utter_helpmore


### product_id-kaart_notregistered

## product_id-kaart_notregistered GENERAL 
* product_id-kaart_notregistered{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_notregistered
  - utter_helpmore

## product_id-kaart_requirements GENERAL with context 
* product_id-kaart_requirements{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_requirements
  - utter_helpmore


### product_id-kaart_otherperson

## product_id-kaart_otherperson GENERAL 
* product_id-kaart_otherperson{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_otherperson
  - utter_helpmore

## product_id-kaart_otherperson GENERAL with context
* product_id-kaart_otherperson{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_id-kaart_otherperson
  - utter_helpmore


### product_id-kaart_fingerprint

## product_id-kaart_fingerprint GENERAL 
* product_id-kaart_fingerprint{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_fingerprint
  - utter_helpmore

## product_id-kaart_fingerprint GENERAL with context municipality
* product_id-kaart_fingerprint{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_fingerprint
  - utter_helpmore


#### KWIJT

### product_id-kaart_lost

## product_id-kaart_lost is unknown
* product_id-kaart_lost{"product": "id-kaart"}
  - utter_ask_location
* inform{"municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## product_id-kaart_lost location known
* product_id-kaart_lost{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## product_id-kaart_lost location known from entity
* product_id-kaart_lost{"product": "id-kaart","municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## product_id-kaart_lost location known without context municipality 
* product_id-kaart_lost{"product": "id-kaart","municipality":"Tilburg"}
  - slot{"product": "id-kaart"}
  - action_productURL
  - utter_helpmore


### product_id-kaart_found

## product_id-kaart_found GENERAL 
* product_id-kaart_found{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_found
  - utter_helpmore

## product_id-kaart_found GENERAL with context
* product_id-kaart_found{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_found
  - utter_helpmore


### product_id-kaart_lostabroad

## product_id-kaart_lostabroad GENERAL
* product_id-kaart_lostabroad{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_lostabroad
  - utter_helpmore

## product_id-kaart_lostabroad GENERAL with context municipality
* product_id-kaart_lostabroad{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_lostabroad
  - utter_helpmore


### product_id-kaart_emergency

## product_id-kaart_emergency GENERAL 
* product_id-kaart_emergency{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - utter_product_id-kaart_emergency
  - utter_helpmore

## product_id-kaart_emergency GENERAL with context
* product_id-kaart_emergency{"product": "id-kaart"}
  - slot{"product": "id-kaart"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_emergency
  - utter_helpmore


### product_id-kaart_validitycountryyes

## product_id-kaart_validitycountryyes GENERAL
* product_id-kaart_validitycountry{"idcountries": "Duitsland"}
  - slot{"idcountries": "Duitsland"}
  - utter_product_id-kaart_validitycountryyes
  - action_resetslotidcountries
  - utter_helpmore

## product_id-kaart_validitycountryyes GENERAL with context
* product_id-kaart_validitycountry{"idcountries": "Duitsland"}
  - slot{"idcountries": "Duitsland"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_validitycountryyes
  - action_resetslotidcountries
  - utter_helpmore


### product_id-kaart_validitycountryno

## product_id-kaart_validitycountryno GENERAL
* product_id-kaart_validitycountry{"noidcountries": "Noord-Korea"}
  - slot{"noidcountries": "Noord-Korea"}
  - utter_product_id-kaart_validitycountryno
  - action_resetslotnoidcountries
  - utter_helpmore

## product_id-kaart_validitycountryno GENERAL with context
* product_id-kaart_validitycountry{"noidcountries": "Noord-Korea"}
  - slot{"noidcountries": "Noord-Korea"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_id-kaart_validitycountryno
  - action_resetslotnoidcountries
  - utter_helpmore