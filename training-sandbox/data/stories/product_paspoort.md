###### Stories Product Paspoort

##### GENERAL + ALGEMENE INFO

#### Manuele Stories

### Product_paspoort_applyinfo

## municipality is unknown  
* product_paspoort_applyinfo{"product": "paspoort"}
    - slot{"product": "paspoort"}
    - utter_ask_location
* inform{"municipality": "Tilburg"}
    - slot{"context_municipality": "Tilburg"}
    - utter_product_paspoort_applyinfo
    - utter_helpmore

## municipality is known  
* product_paspoort_applyinfo{"product": "paspoort"}
    - slot{"context_municipality": "Tilburg"}
    - slot{"product": "paspoort"}
    - utter_product_paspoort_applyinfo
    - utter_helpmore

## municipality is known from entity
* product_paspoort_applyinfo{"product": "paspoort", "municipality": "Tilburg"}
    - slot{"context_municipality": "Tilburg"}
    - slot{"product": "paspoort"}
    - utter_product_paspoort_applyinfo
    - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_applyinfo{"product": "paspoort", "municipality": "Tilburg"}
    - slot{"product": "paspoort"}
    - utter_product_paspoort_applyinfo
    - utter_helpmore


### Product_paspoort_general GENERAL

## municipality is unknown
* product_paspoort_general{"product": "paspoort"}
    - slot{"product": "paspoort"}
    - utter_product_paspoort_general

## municipality is known
* product_paspoort_general{"product": "paspoort"}
    - slot{"product": "paspoort"}
    - slot{"context_municipality": "Tilburg"}
    - utter_product_paspoort_general



#### PASPOORT COSTS

#### Manuele Stories

### Product_paspoort_costs

## municipality is unknown
* product_paspoort_costs{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_costs
  - utter_helpmore

## municipality is known
* product_paspoort_costs{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_costs
  - utter_helpmore

## municipality is known from entity
* product_paspoort_costs{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_costs
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_costs{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - price_form
  - form{"name": "price_form"}
  - form{"name": null}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_costs
  - utter_helpmore


### product_paspoort_costsurgent

## municipality is unknown
* product_paspoort_costsurgent{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_paspoort_costsurgent
  - utter_helpmore

## municipality is known
* product_paspoort_costsurgent{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_costsurgent
  - utter_helpmore


## municipality is known from entity
* product_paspoort_costsurgent{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_costsurgent
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_costsurgent{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_costsurgent
  - utter_helpmore

### product_paspoort_paymentmethod GENERAL

## municipality is unknown
* product_paspoort_paymentmethod{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_paymentmethod
  - utter_helpmore

## municipality is known
* product_paspoort_paymentmethod{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_paymentmethod
  - utter_helpmore


#### Interactieve stories 

### Scenario naam



##### PASPOORT AANVRAGEN

#### Manuele stories

### product_paspoort_applyinfo

## municipality is unknown
* product_paspoort_applyinfo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
 * inform{"municipality": "Tilburg"} 
  - utter_product_paspoort_applyinfo


## municipality is known
* product_paspoort_applyinfo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_applyinfo
  - utter_helpmore

## municipality is known from entity
* product_paspoort_applyinfo{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_applyinfo
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_applyinfo{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_applyinfo
  - utter_helpmore

### product_paspoort_urgentinfo

## municipality is unknown
* product_paspoort_urgentinfo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_urgentinfo
  - utter_helpmore

## municipality is known
* product_paspoort_urgentinfo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_urgentinfo
  - utter_helpmore

## municipality is known from entity
* product_paspoort_urgentinfo{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_urgentinfo
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_urgentinfo{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_urgentinfo
  - utter_helpmore


### product_paspoort_eta GENERAL

## municipality is unknown
* product_paspoort_eta{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_eta
  - utter_helpmore

## municipality is known
* product_paspoort_eta{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_eta
  - utter_helpmore

### product_paspoort_etanotapplied GENERAL

## municipality is unknown
* product_paspoort_etanotapplied{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_etanotapplied
  - utter_helpmore

## municipality is known
* product_paspoort_etanotapplied{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_etanotapplied
  - utter_helpmore

### product_paspoort_appointment

## municipality is unknown
* product_paspoort_appointment{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_paspoort_appointment
  - utter_helpmore

## municipality is known
* product_paspoort_appointment{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_appointment
  - utter_helpmore

## municipality is known from entity
* product_paspoort_appointment{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_appointment
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_appointment{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_appointment
  - utter_helpmore


### product_paspoort_necessities GENERAL

## municipality is unknown
* product_paspoort_necessities{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_necessities
  - utter_helpmore

## municipality is known
* product_paspoort_necessities{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_necessities
  - utter_helpmore


### product_paspoort_photo GENERAL

## municipality is unknown
* product_paspoort_photo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_photo
  - utter_helpmore

## municipality is known
* product_paspoort_photo{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_photo
  - utter_helpmore


### product_paspoort_collect 

## municipality is unknown
* product_paspoort_collect{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_paspoort_collect
  - utter_helpmore

## municipality is known
* product_paspoort_collect{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_collect
  - utter_helpmore

## municipality is known from entity
* product_paspoort_collect{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_collect
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_collect{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_collect
  - utter_helpmore


### product_paspoort_noticket 

## municipality is unknown
* product_paspoort_noticket{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_noticket
  - utter_helpmore

## municipality is known
* product_paspoort_noticket{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_noticket
  - utter_helpmore

## municipality is known from entity
* product_paspoort_noticket{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_noticket
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_noticket{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_noticket
  - utter_helpmore

### product_paspoort_delivery

## municipality is unknown
* product_paspoort_delivery{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality": "Tilburg"} 
  - utter_product_paspoort_delivery
  - utter_helpmore

## municipality is known
* product_paspoort_delivery{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_delivery
  - utter_helpmore

## municipality is known from entity
* product_paspoort_delivery{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_delivery
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_delivery{"product": "paspoort", "municipality": "Tilburg"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_delivery
  - utter_helpmore


### product_paspoort_abroad GENERAL

## municipality is unknown
* product_paspoort_abroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_abroad
  - utter_helpmore

## municipality is known
* product_paspoort_abroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_abroad
  - utter_helpmore


### product_paspoort_abroad GENERAL

## municipality is unknown
* product_paspoort_abroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_abroad
  - utter_helpmore

## municipality is known
* product_paspoort_abroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_abroad
  - utter_helpmore


### product_paspoort_notregistered GENERAL

## municipality is unknown
* product_paspoort_notregistered{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_notregistered
  - utter_helpmore

## municipality is known
* product_paspoort_notregistered{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_notregistered
  - utter_helpmore


### product_paspoort_requirements GENERAL

## municipality is unknown
* product_paspoort_requirements{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_requirements
  - utter_helpmore

## municipality is known
* product_paspoort_requirements{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_requirements
  - utter_helpmore


### product_paspoort_otherperson GENERAL

## municipality is unknown
* product_paspoort_otherperson{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_otherperson
  - utter_helpmore

## municipality is known
* product_paspoort_otherperson{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_otherperson
  - utter_helpmore


### product_paspoort_fingerprint GENERAL

## municipality is unknown
* product_paspoort_fingerprint{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_fingerprint
  - utter_helpmore

## municipality is known
* product_paspoort_fingerprint{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_paspoort_fingerprint
  - utter_helpmore


##### PASPOORT KWIJT

#### Manuele stories

### product_paspoort_lost

## municipality is unknown
* product_paspoort_lost{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known
* product_paspoort_lost{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity
* product_paspoort_lost{"product": "paspoort", "municipality":"Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_lost{"product": "paspoort", "municipality":"Tilburg"}
  - slot{"product": "paspoort"}
  - action_productURL
  - utter_helpmore


### product_paspoort_lost

## municipality is unknown
* product_paspoort_lost{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_ask_location
* inform{"municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known
* product_paspoort_lost{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity
* product_paspoort_lost{"product": "paspoort", "municipality":"Tilburg"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity without context municipality
* product_paspoort_lost{"product": "paspoort", "municipality":"Tilburg"}
  - slot{"product": "paspoort"}
  - action_productURL
  - utter_helpmore

### product_paspoort_lostreport GENERAL

## municipality is unknown
* product_paspoort_lostreport{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_lostreport
  - utter_helpmore

## municipality is known
* product_paspoort_lostreport{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_lostreport
  - utter_helpmore

### product_paspoort_found GENERAL

## municipality is unknown
* product_paspoort_found{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_found
  - utter_helpmore

## municipality is known
* product_paspoort_found{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_found
  - utter_helpmore


### product_paspoort_lostabroad GENERAL

## municipality is unknown
* product_paspoort_lostabroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_lostabroad
  - utter_helpmore

## municipality is known
* product_paspoort_lostabroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_lostabroad
  - utter_helpmore


### product_paspoort_emergency GENERAL

## municipality is unknown
* product_paspoort_emergency{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_emergency
  - utter_helpmore

## municipality is known
* product_paspoort_emergency{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_emergency
  - utter_helpmore


##### GELDIGHEID
#### Manuele stories

### product_paspoort_emergency

## municipality is unknown
* product_paspoort_emergency{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_emergency
  - utter_helpmore

## municipality is known
* product_paspoort_emergency{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_emergency
  - utter_helpmore


### product_paspoort_validity

## municipality is unknown
* product_paspoort_validity{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_emergency
  - utter_helpmore

## municipality is known
* product_paspoort_validity{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_emergency
  - utter_helpmore





##### CHILD
#### Manuele stories


### product_paspoort_childnecessities GENERAL

## municipality is unknown
* product_paspoort_childnecessities{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_childnecessities
  - utter_helpmore

## municipality is known
* product_paspoort_childnecessities{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_childnecessities
  - utter_helpmore


### product_paspoort_childadd GENERAL

## municipality is unknown
* product_paspoort_childadd{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_childadd
  - utter_helpmore

## municipality is known
* product_paspoort_childadd{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_childadd
  - utter_helpmore


### product_paspoort_childabroad GENERAL

## municipality is unknown
* product_paspoort_childabroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_childabroad
  - utter_helpmore

## municipality is known
* product_paspoort_childabroad{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_childabroad
  - utter_helpmore


### product_paspoort_childpermission GENERAL

## municipality is unknown
* product_paspoort_childpermission{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - utter_product_paspoort_childpermission
  - utter_helpmore

## municipality is known
* product_paspoort_childpermission{"product": "paspoort"}
  - slot{"product": "paspoort"}
  - slot{"context_municipality":"Tilburg"}
  - utter_product_paspoort_childpermission
  - utter_helpmore