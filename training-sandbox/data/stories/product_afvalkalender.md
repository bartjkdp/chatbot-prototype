## product_afvalkalender_info location is unknown 
* product_afvalkalender_info{"product": "afvalkalender"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_afvalkalender_info location is known
* product_afvalkalender_info{"product": "afvalkalender"}
  - slot{"municipality": "Tilburg"}
  - slot{"product":"afvalkalender"}
  - utter_product_notlearned
  - action_productURL
  - action_listen

