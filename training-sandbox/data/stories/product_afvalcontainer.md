## product_afvalcontainer_info location is unknown 
* product_afvalcontainer_info{"product": "afvalcontainer"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_afvalcontainer_info location is known 
* product_afvalcontainer_info{"product": "afvalcontainer"}
  - slot{"product": "afvalcontainer"}
  - slot{"municipality": "Tilburg"}
  - utter_product_notlearned
  - action_productURL
  - action_listen
