## product_bijstand_info location is unknown 
* product_bijstand_info{"product": "bijstand"}
  - utter_product_notlearned
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - action_listen

## product_bijstand_info location is known 
* product_bijstand_info{"product": "bijstand"}
  - slot{"product": "bijstand"}
  - slot{"municipality": "Tilburg"}
  - utter_product_notlearned
  - action_productURL
  - action_listen


