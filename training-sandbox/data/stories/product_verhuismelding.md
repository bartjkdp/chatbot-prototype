##### INFO

#### Manuele Stories

### product_verhuismelding_info

## municipality is unknown 
* product_verhuismelding_info{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_info
  - utter_helpmore

## municipality is known
* product_verhuismelding_info{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_info
  - utter_helpmore


### product_verhuismelding_when

## municipality is unknown 
* product_verhuismelding_info{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_info
  - utter_helpmore


## municipality is known
* product_verhuismelding_when{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - form_when_verhuismelding
  - form{"name": "form_when_verhuismelding"}
  - form{"name": null}  
  - utter_helpmore


### product_verhuismelding_apply

## municipality is unknown 
* product_verhuismelding_apply{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_apply
  - utter_helpmore

## municipality is known
* product_verhuismelding_apply{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_verhuismelding_apply
  - utter_helpmore

#### HOE VERHUIZEN

### product_verhuismelding_adreswijziging

## municipality is unknown 
* product_verhuismelding_adreswijziging{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_ask_location_new
* inform {"municipality": "Tilburg"}
  - utter_product_verhuismelding_nieuwadres
  - utter_helpmore

## municipality is known
* product_verhuismelding_adreswijziging{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_nieuwadres
  - utter_helpmore

## municipality is known from entity
* product_verhuismelding_adreswijziging{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_nieuwadres
  - utter_helpmore

## municipality is known from entity without municipality context
* product_verhuismelding_adreswijziging{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_nieuwadres
  - utter_helpmore

### product_verhuismelding_bevestiging

## municipality is unknown 
* product_verhuismelding_bevestiging{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_bevestiging
  - utter_helpmore

## municipality is known
* product_verhuismelding_bevestiging{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_verhuismelding_bevestiging
  - utter_helpmore

### product_verhuismelding_mandatory

## municipality is unknown 
* product_verhuismelding_mandatory{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_mandatory
  - utter_helpmore

## municipality is known
* product_verhuismelding_mandatory{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_verhuismelding_mandatory
  - utter_helpmore


### product_uittreksel-verhuismelding_proofregistration

## municipality is unknown 
* product_uittreksel-verhuismelding_proofregistration{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_ask_location_new
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - utter_helpmore

## municipality is known
* product_uittreksel-verhuismelding_proofregistration{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity
* product_uittreksel-verhuismelding_proofregistration{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity without municipality context
* product_uittreksel-verhuismelding_proofregistration{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore


### product_verhuismelding_applydigid

## municipality is unknown 
* product_verhuismelding_applydigid{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_applydigid
  - utter_helpmore

## municipality is known
* product_uittreksel-verhuismelding_proofregistration{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_applydigid
  - utter_helpmore


### product_verhuismelding_dontwantdigid

## municipality is unknown 
* product_verhuismelding_dontwantdigid{"product": "verhuismelding"}
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - utter_helpmore

## municipality is known
* product_verhuismelding_dontwantdigid{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity
* product_verhuismelding_dontwantdigid{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity without municipality context
* product_verhuismelding_dontwantdigid{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

### product_verhuismelding_digidinfo

## municipality is unknown 
* product_verhuismelding_digidinfo{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_digidinfo
  - utter_helpmore

## municipality is known
* product_verhuismelding_digidinfo{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_digidinfo
  - utter_helpmore


### product_verhuismelding_formerror

## municipality is unknown 
* product_verhuismelding_formerror{"product": "verhuismelding"}
  - utter_ask_location
* inform {"municipality": "Tilburg"} 
  - action_productURL
  - utter_helpmore

## municipality is known
* product_verhuismelding_formerror{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity
* product_verhuismelding_formerror{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore

## municipality is known from entity without municipality context
* product_verhuismelding_formerror{"product": "verhuismelding", "municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - action_productURL
  - utter_helpmore
  

### product_verhuismelding_when_info

## municipality is unknown 
* product_verhuismelding_when_info{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_when_info
  - utter_helpmore

## municipality is known
* product_verhuismelding_when_info{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_when_info
  - utter_helpmore


### product_verhuismelding_kindinschrijven

## municipality is unknown minderjarig uitwonend
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadres
* affirmative
  - utter_product_verhuismelding_kindinschrijvenanderadresminderjarig
  - utter_helpmore

## municipality is unknown minderjarig thuiswonendwonend
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadres
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadresmeerderjarig
  - utter_helpmore

## municipality is unknown meerderjarig
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* affirmative
  - utter_product_verhuismelding_kindinschrijvenaffirmative
  - utter_helpmore


## municipality is known minderjarig uitwonend
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadres
* affirmative
  - utter_product_verhuismelding_kindinschrijvenanderadresminderjarig
  - utter_helpmore

## municipality is known minderjarig thuiswonendwonend
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadres
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadresmeerderjarig
  - utter_helpmore

## municipality is known meerderjarig
* product_verhuismelding_kindinschrijven{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijven
* affirmative
  - utter_product_verhuismelding_kindinschrijvenaffirmative
  - utter_helpmore


### product_verhuismelding_kindinschrijvenanderadres


## municipality is unknown product_verhuismelding_kindinschrijvenanderadres minderjarig
* product_verhuismelding_kindinschrijvenanderadres{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijvenanderadres
* affirmative
  - utter_product_verhuismelding_kindinschrijvenanderadresminderjarig
  - utter_helpmore

## municipality is unknown product_verhuismelding_kindinschrijvenanderadres meerderjarig
* product_verhuismelding_kindinschrijvenanderadres{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijvenanderadres
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadresmeerderjarig
  - utter_helpmore

## municipality is known product_verhuismelding_kindinschrijvenanderadres minderjarig
* product_verhuismelding_kindinschrijvenanderadres{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijvenanderadres
* affirmative
  - utter_product_verhuismelding_kindinschrijvenanderadresminderjarig
  - utter_helpmore

## municipality is known product_verhuismelding_kindinschrijvenanderadres meerderjarig
* product_verhuismelding_kindinschrijvenanderadres{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_kindinschrijvenanderadres
* denial
  - utter_product_verhuismelding_kindinschrijvenanderadresmeerderjarig
  - utter_helpmore
  

### product_verhuismelding_applyabroad

## municipality is unknown 
* product_verhuismelding_applyabroad{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_applyabroad
  - utter_helpmore

## municipality is known
* product_verhuismelding_applyabroad{"product": "verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_applyabroad
  - utter_helpmore




### product_verhuismelding_emigrate_longer8years

## municipality is unknown
* product_verhuismelding_emigrate{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_emigrate
* affirmative
  - utter_product_verhuismelding_emigrateyes
  - utter_helpmore

## municipality is known
* product_verhuismelding_emigrate{"product": "verhuismelding"}
  - slot{"product":"verhuismelding"}
  - slot{"context_municipality": "Tilburg"}
  - utter_product_verhuismelding_emigrate
* affirmative
  - utter_product_verhuismelding_emigrateyes
  - utter_helpmore











