## intent:affirmative
- yes please!
- dat zou mooi zijn
- graag

## intent:apply_product
- Kan ik dat nu aanvragen?

## intent:ask_burgemeesterdongen
- wie zit de gemeenteraad voor in Dongen?
- wie is de voorzitter van de gemeenteraad in Dongen?

## intent:ask_burgemeestertilburg
- Burgemeerster Tilburg?
- wie is de burgermeester van tilburg?

## intent:ask_burgemeesterutrecht
- Wie is de burgemeester van utrecht?
- Weet jij wie de burgemeester is in Utrecht?

## intent:ask_directeur
- Wie is de directeur?

## intent:ask_directeurvdp
- wie is de directeur van de vdp?

## intent:ask_for_chatbot
- Ga verder met de bot
- Verder praten met de chatbot

## intent:ask_for_human
- praat met medewerker
- ik wil iemand spreken van burgerzaken
- Ik praat niet met robots
- Ik praat niet met bots

## intent:ask_population
- Hoeveel inwoners heeft nederland?
- weet jij hoeveel mensen er in Nederland wonen?

## intent:ask_weather
- Mooi weer buiten
- lekker weer he?
- weet je wat voor weer het wordt?

## intent:cancel
- stop

## intent:chitchat
- een vaasje
- ik wil een biertje bestellen
- mag ik een pilsje

## intent:denial
- nope
- zeker niet

## intent:dontknow
- daar vraag je me wat
- zou het niet weten

## intent:greet
- hey
- Hallo, [Emma](name) hier!
- goedenavond [Utrecht](municipality)
- Hey, ik ben [Jeroen](name)
- hey gem, mijn naam is [jason](name)

## intent:inform
- Gemeente [Rotterdam](municipality)
- [best]
- [vaart](municipality:Dongen)
- [Berkel-Enschot](municipality:Tilburg)
- [Ede](municipality)
- Dat is [Dongen](municipality)
- [Amsterdam](municipality)
- [enschede]
- [tilburg]
- [Rotterdam](municipality)
- [De Bilt](municipality)

## intent:product_afspraak_info
- Hoe kan ik een afspraak maken
- ik wil een gesprek met de gemeente
- Ik wil persoonlijk contact met de gemeente
- Kan ik langskomen of gaat dit op afspraak?

## intent:product_afvalcontainer_info
- Moet ik thuis zijn als jullie de grijze kliko met oranje deksel komen brengen?
- Ik heb een grote tuin, kan ik meerdere containers krijgen?
- Is de bruine kliko ook voor GFT?
- Ze hebben mijn container gestolen. Wanneer krijg ik een nieuwe?
- Ze hebben mijn kliko gejat, kan ik een nieuwe krijgen
- Mijn grijze kliko is te klein, kan ik er twee krijgen?
- Mijn afvalbak is gejat, kost het geld om een nieuwe te krijgen?
- Mijn container staat in de weg
- Ze hebben mijn kliko gestolen. Krijg ik gratis een nieuwe?
- Ik woon in een flat. Kan ik een citybin krijgen?

## intent:product_afvalkalender_info
- wanneer wordt de minicontainer opgehaald
- Wanneer komen ze het huisvuil ophalen
- waar vind ik de inzamelkalender voor afval
- Wanneer wordt mijn afval opgehaald
- Ik snap het ophaalschema voor vuilnis niet

## intent:product_belasting-kwijtschelding_info
- Ik wil kwijtschelding aanvragen
- Ik kan geen belasting betalen
- belasting kwijtschelding
- Ik wil kwjitschelding aanvragem
- Ik wil kwijdschelding van belasting

## intent:product_bijstand_info
- Ik wil graag een sociale uitkering
- ik had werk gevonden, maar dit stopt.  kan ik opnieuw een uitkering aanvragen?
- Kan ik een werkeloosheidsuitkering aanvragen?
- kom ik in aanmerking voor bijzondere bijstand?
- Kan ik extra inkomen krijgen?

## intent:product_brp-uittreksel_info
- Hoe kom ik aan een scheidingsakte?
- Hoe kom ik aan een bewijs van uitschrijving?
- Kan ik op de website een uitreksel bestellen?
- Ik wil een echtscheidingsakte
- Hoe vraag ik een uittreksel aan.

## intent:product_geboorteaangifte_info
- Ik wil een melding maken van ons geboren kind.
- geboorteaangifte
- Mijn kind is overleden. Kan ik dan ook een geboorte aangeven?
- hoe kan ik mijn pasgeboren zoon melden
- hoe kan ik mijn pasgeboren dochter melden

## intent:product_id-kaart_abroad
- Ik woon in het buitenland, hoe kom ik aan mijn id-kaart?

## intent:product_id-kaart_applyinfo
- Hoe vraag ik een id-kaart aan?
- Kan ik online een id-kaart aanvragen?

## intent:product_id-kaart_appointment
- Ik wil een id-kaart aanvragen aan de balie, hoe maak ik een afspraak?

## intent:product_id-kaart_childnecessities
- Wat moet ik hebben als ik een id-kaart voor mijn kind wil halen?

## intent:product_id-kaart_collect
- Kan ik mijn id-kaart ophalen zonder afspraak?
- Waar kan ik mijn id-kaart afhalen?

## intent:product_id-kaart_costs
- Wat is de prijs van een id-kaart?
- Hoeveel kost een [idkaart](product:id-kaart)?

## intent:product_id-kaart_costsurgent
- Wat is de prijs van een spoedaanvraag voor een id-kaart?
- Wat zijn de kosten van een spoedaanvraag voor een id-kaart?

## intent:product_id-kaart_delivery
- Ik kan mijn id-kaart niet ophalen, kan die opgestuurd worden?

## intent:product_id-kaart_eta
- Wanneer kan ik het id-kaart ophalen?
- Kan ik mijn id-kaart al ophalen?
- Wanneer is het id-kaart klaar?

## intent:product_id-kaart_fingerprint
- Is het nodig dat ik een vingerafdruk laat maken voor een id-kaart?
- Moet ik een vingerafdruk zetten voor een id-kaart?

## intent:product_id-kaart_found
- Mijn verloren id-kaart is weer opgedoken. Wat doe ik daarmee?

## intent:product_id-kaart_lost
- Mijn id-kaart is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?

## intent:product_id-kaart_lostabroad
- Ik ben mijn id-kaart kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?

## intent:product_id-kaart_necessities
- Wat moet ik meenemen bij de aanvraag van een id-kaart?
- Wat ik heb ik nodig als ik een id-kaart aan wil vragen?

## intent:product_id-kaart_noticket
- Kan ik mijn id-kaart zonder bewijs ophalen?
- Is het afhalen van mijn [identiteitskaart](product:id-kaart) mogelijk zonder afhaalbewijs?

## intent:product_id-kaart_notregistered
- Hoe vraag je een id-kaart aan als je niet ingeschreven staat?

## intent:product_id-kaart_otherperson
- Ik ben slecht ter been en kan mijn id-kaart daardoor niet ophalen.

## intent:product_id-kaart_photo
- Welke pasfoto moet er op mijn id-kaart?
- Waar kan ik pasfoto's laten maken voor een id-kaart?

## intent:product_id-kaart_requirements
- Wanneer mag je een id-kaart aanvragen?

## intent:product_id-kaart_urgentinfo
- Hoe kan ik een id-kaart met spoed aanvragen?

## intent:product_id-kaart_validity
- Wanneer verloopt mijn id-kaart?

## intent:product_id-kaart_validityabroad
- Waar ter wereld is mijn id-kaart geldig?

## intent:product_id-kaart_validitycountry
- Is een idkaart een geldig document in [Griekenland](idcountries)?
- Geldt mijn idkaart in [Irak](noidcountries)?
- Accepteert [Duitsland](idcountries) een id-kaart?

## intent:product_melding-openbare-ruimte_info
- De openbare verlichting is defect
- In de straat hier zorgen jongeren voor overlast
- De straatlantaarn blijft branden
- Komen jullie de eikenprocessierups weghalen?
- Er is een tak van de boom gewaaid
- Het plastic is niet opgehaald
- Er ligt hier chemisch afval
- we hebben erg veel geluidsoverlast
- De straatverlichting is uitgevallen
- Er ligt hier zo veel hondenpoep, komen jullie dat opruimen?
- De kade is beschadigd
- Er ligt afval in het groenperk
- De afvalbak is kapot
- We hebben veel overlast van bepaalde buren in de straat
- ik heb last van een bedrijf
- Mijn grofvuil staat nog steeds op straat.
- bij ons in de buurt worden al enige tijd meerdere ratten gesignaleerd
- Waar kan ik vuurwerkoverlast melden?

## intent:product_paspoort_abroad
- Ik woon in het buitenland, hoe kom ik aan mijn paspoort?

## intent:product_paspoort_applyinfo
- Kan ik een paspoort aanvragen in [Utrecht](municipality)?
- Hoe krijg ik een paspoort in [Amsterdam](municipality)?

## intent:product_paspoort_appointment
- Hoe maak ik een afspraak om paspoort aan te vragen?
- Kan ik online een afspraak maken voor het aanvragen van een paspoort?

## intent:product_paspoort_childabroad
- Ik wil op vakantie met mijn kind, wat moet ik regelen?

## intent:product_paspoort_childadd
- Kan ik mijn kind nog laten bijschrijven in mijn paspoort?

## intent:product_paspoort_childnecessities
- Wat heb ik nodig als ik voor mijn kind een paspoort aan wil vragen?

## intent:product_paspoort_childpermission
- Wat moet ik doen als de andere ouder geen toestemming wil geven om te reizen?

## intent:product_paspoort_collect
- Kan ik mijn paspoort ophalen zonder afspraak in [Ede](municipality)?
- Moet ik een afspraak maken voor het afhalen van mijn paspoort in [Dongen](municipality)?

## intent:product_paspoort_costs
- Hoeveel moet ik voor een paspoort betalen?
- Hoeveel kost een paspoort?
- Hoe duur is een paspoort in [Dongen](municipality)?

## intent:product_paspoort_costsurgent
- Hoeveel meer kost een spoedaanvraag paspoort?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb in [Eindhoven](municipality)?
- Wat kost een spoedaanvraag paspoort in [Utrecht](municipality)?

## intent:product_paspoort_delivery
- Kan het paspoort opgestuurd worden?

## intent:product_paspoort_emergency
- Waar vraag ik een noodpaspoort aan?

## intent:product_paspoort_eta
- Is mijn paspoort al klaar?
- Kunt u kijken of mijn paspoort al klaar ligt?

## intent:product_paspoort_etanotapplied
- Hoe lang duurt het voordat ik een nieuw paspoort krijg?

## intent:product_paspoort_fingerprint
- Waarvoor hebben jullie mijn vingerafdrukken nodig?
- Wat doen jullie met mijn vingerafdrukken?

## intent:product_paspoort_found
- Mijn paspoort vond ik zojuist weer terug.

## intent:product_paspoort_lost
- Wat moet ik doen als mijn paspoort kwijt, vermist of gestolen is?
- Moet ik het doorgeven als mijn paspoort kwijt is?

## intent:product_paspoort_lostabroad
- Ik ben mijn paspoort kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?

## intent:product_paspoort_lostreport
- Moet ik het ook aan de politie doorgeven?

## intent:product_paspoort_necessities
- Wat moet ik bij me hebben als ik een paspoort aan ga vragen?
- Moet ik iets meenemen als ik een paspoort aanvraag?

## intent:product_paspoort_noticket
- Ik ben het afhaalbewijs voor het paspoort kwijt, wat nu?

## intent:product_paspoort_notregistered
- Ik sta nergens ingeschreven, hoe kom ik aan een paspoort?

## intent:product_paspoort_otherperson
- Hoe kan ik een paspoort aanvragen als ik niet langs kan komen?

## intent:product_paspoort_paymentmethod
- Kan ik pinnen?
- Kan ik met cash betalen?

## intent:product_paspoort_photo
- Waar moet ik een pasfoto laten maken voor mijn paspoort?
- Aan welke eisen moet een pasfoto voor paspoort voldoen?

## intent:product_paspoort_requirements
- Mag iedereen een paspoort aanvragen?

## intent:product_paspoort_urgentinfo
- Kan ik versneld een paspoort aanvragen?

## intent:product_paspoort_validity
- Hoe lang is een paspoort geldig?

## intent:product_paspoort_validityabroad
- Naar welke landen mag ik met mijn paspoort?

## intent:product_rijbewijs_info
- Wanneer kan ik mijn rijbewijs verlengen?
- Ik ben mijn rijbewijs kwijt. Wat moet ik doen?
- Welke rijbewijscategorieën zijn er?
- Ik wil een afspraak om mijn rijbewijs aan te vragen
- Hoeveel kost een rijbewijs?
- Na hoeveel dagen kan ik mijn rijbewijs ophalen?

## intent:product_trouwen_info
- Is de trouwzaal beschikbaar?
- ik ga trouwen

## intent:product_uittreksel-verhuismelding_proofregistration
- Kan ik een uittreksel krijgen waarop staat waar ik woon?

## intent:product_verhuismelding_adreswijziging
- Waar kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Ik heb een adreswijziging
- Hoe geef ik mijn [verhuizing](product:verhuismelding) door?
- Hoe kan ik mijn [verhuizing](product:verhuismelding) doorgeven?

## intent:product_verhuismelding_applyabroad
- Ik heb al eerder in Nederland gewoond. Moet ik mij nu opnieuw inschrijven?

## intent:product_verhuismelding_applydigid
- Heb ik niet. (vervolgvraag)

## intent:product_verhuismelding_bevestiging
- Hoe weet ik of het doorgeven van mijn verhuizing goed is gegaan?
- Hoe kom ik aan een bewijs dat ik verhuisd ben?
- Ben ik al uitgeschreven?

## intent:product_verhuismelding_digidinfo
- Ik weet niet wat DigiD is.

## intent:product_verhuismelding_dontwantdigid
- Ik vind DigiD stom.

## intent:product_verhuismelding_emigrate
- Moet ik me uitschrijven als ik ga emigreren?

## intent:product_verhuismelding_formerror
- Mijn nieuwe adres zit er niet in.
- Het formulier werkt niet, wat nu?

## intent:product_verhuismelding_kindinschrijven
- Kan ik mijn kind bij mij inschrijven?
- Kan ik mijn kind meeverhuizen

## intent:product_verhuismelding_kindinschrijvenanderadres
- Kan ik de adreswijziging doorgeven voor mijn kind dat op kamers gaat?

## intent:product_verhuismelding_mandatory
- Moet ik mij inschrijven op mijn nieuwe adres?
- Ben ik verplicht om mijn verhuizing te melden?

## intent:product_verhuismelding_when
- Ik ben vorige weekbij mijn vriend [gaan wonen](product:verhuismelding), wanneer moet ik dat doorgeven?
- Ik ben te laat met het doorgeven van mijn verhuizing. Kan ik dat nog doorgeven?
- Als ik volgende week ga [verhuizen](product:verhuismelding), kan ik dat nu al zeggen?
- ik ben vergeten mijn [adreswijziging](product:verhuismelding) door te geven. Kan dit nog?
- Mijn verhuizing heb ik te laat doorgegeven. Kan ik dat nog doen?
- Ik weet nu al wanneer ik ga [verhuizen](product:verhuismelding). Kan ik dat nu al doorgeven?

## intent:product_verhuismelding_when_info
- Kan ik mij al voor mijn verhuizing inschrijven?
- Hoeveel eerder kun je je verhuizing doorgeven?

## intent:restart
- Hoe start ik opnieuw?
- Opnieuw

## intent:thanks
- dank u vriendelijk
- bedankt
- fijn, dankje

## intent:unfriendly
- ROT OP
- stomme gem
- rot op

## intent:unfriendly_questions
- domme bot
- Domme Gem

## synonym:Almere
- almere

## synonym:Amsterdam
- amsterdam

## synonym:Brazilië
- Brazilie

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## synonym:Utrecht
- utrecht

## synonym:id-kaart
- idkaart
- identiteitskaart
- id

## synonym:verhuismelding
- verhuizing
- adreswijziging
- mijn nieuwe adres
- verhuizen
- verhuisd
- verhuis
- verhiuzin
- nieuwe adres
- gaan wonen

## lookup:municipality
  data/municipalities.txt

## lookup:idcountries
  data/idcountries.txt

## lookup:noidcountries
  data/noidcountries.txt

## lookup:product
- paspoort
- parkeervergunning
- id-kaart
- overlijdensakte
- verhuismelding
- bijstand
- uitkering
- hondenbelasting
- afvalcontainer
- belasting-kwijtschelding
- brp-uittreksel
- trouwen
- afspraak
- afvalkalender
- rijbewijs
- geboorteaangifte
- melding-openbare-ruimte
- reisdoc-costs-urgent
- reisdoc-costs-paymentmethod
