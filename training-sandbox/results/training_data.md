## intent:affirmative
- aub
- ja
- akkoord
- prima
- helemaal geweldig
- alsjeblieft
- ok
- ja, graag

## intent:apply_product
- Kan ik dat nu doen?
- Kan ik dat nu doorgeven?

## intent:ask_burgemeesterdongen
- Weet jij wie de burgemeester is in Dongen
- Wie is de burgemeester in Dongen?
- burgemeester Dongen?
- wie is de burgervader van dongen
- Wie is de burgemeester van Dongen?

## intent:ask_burgemeestertilburg
- wie is de voorzitter van de gemeenteraad in tilburg?
- Wie is de baas van de politie in Dongen?
- wie is de burgermeester van Tilburg?
- Wie is de burgemeester in Tilburg?
- wie is de burgervader van Tilburg?
- Wie is de baas van de politie in Tilburg?

## intent:ask_burgemeesterutrecht
- wie is de voorzitter van de gemeenteraad in utrecht?
- wie is de burgervader van utrecht?
- Wie is de burgemeester in utrecht
- Wie is de burgemeester in Utrecht?
- Wie is de baas van de politie in Utrecht?
- burgemeester van utrecht?

## intent:ask_directeur
- wie is de directeur van zaakgerichtwerken?
- wie is de directeur van de hackaton?
- wie is de directeur?
- wie is de directeur van ZGW?

## intent:ask_directeurvdp
- wie is de directeur van VDP?
- directeur VDP
- Wie is de directeur van de VDP?

## intent:ask_for_chatbot
- Chat met de chatbot
- ik wil met de chatbot chatten
- Ga door met de chatbot
- ik wil verder met de chatbot

## intent:ask_for_human
- Ik wil een mens aan de lijn
- Ga weg robot
- Ga weg Gem
- kan ik een mensen spreken
- Verbind me even door met een echt mens
- ik wil een mens spreken
- Ik wil met een medewerker spreken
- echt mens aub
- ga weg
- Ik wil met een mens chatten
- ga weg gem
- ik praat niet met chatbots
- kan ik een echt gesprek hebben

## intent:ask_population
- hoeveel mensen wonen er in Nederland
- inwoners nederland
- hoeveel nederlanders zijn er
- Hoeveel inwoners hebben we in Nederland
- hoeveel inwoners heeft nederland

## intent:ask_weather
- wat voor weer is het daar?
- is het droog buiten?
- regent het?
- is het droog?
- wat is de weersvoorspelling?
- moet ik mijn paraplu pakken?
- wat voor weer is het?
- valt er regen?
- is het droog buiten?
- gaat het regenen?
- Lekker weertje hè?

## intent:cancel
- ik wil afbreken aub
- kunnen we stoppen?
- annuleren
- ik wil stoppen

## intent:chitchat
- een Hertog Jan
- een La Trappe blond aub
- wanneer is de borrel
- ik wil een biertje
- een vaasje aub
- een fluitje
- ik heb zin in bier
- mag ik een biertje

## intent:denial
- neen
- nee, bedankt
- nee
- alsjeblieft niet
- echt niet

## intent:dontknow
- ?
- dat weet ik niet
- geen idee
- Joost mag het weten

## intent:greet
- heuy
- Hoi mijn naam is [Wim](name)
- Hoi [Uden](municipality)
- Hallo [Ede](municipality), ik ben [Wendy](name)
- Hi, ik ben [Harvey](name)
- Hoi [Dongen](municipality)
- Hallo hier [Ali](name)
- hoi
- goedenavond
- hallo [Tilburg](municipality)!
- Hallo
- goede middag
- heey
- Hallo [Utrecht](municipality)! Mijn naam is [Piet](name)
- hi
- goede morgen

## intent:inform
- ['s Gravenmoer](municipality:Dongen)
- [biezenmortel](municipality:Tilburg)
- [Biezenmortel](municipality:Tilburg)
- [tilburg](municipality:Tilburg)
- [amsterdam](municipality:Amsterdam)
- [berkel](municipality:Tilburg)
- [klein dongen](municipality:Dongen)
- [Eindhoven](municipality)
- [Amsterdam](municipality)
- Gemeente [Tilburg](municipality)
- [utrecht](municipality:Utrecht)
- [Klein Dongen](municipality:Dongen)
- [dongen](municipality:Dongen)
- [ede]
- [Arnhem](municipality)
- [Udenhout](municipality:Tilburg)
- [Dongen](municipality)
- [Utrecht](municipality)
- [Buren](municipality)
- [utrecht]
- [Vaart](municipality:Dongen)
- ['sgravenmoer](municipality:Dongen)
- [buren]
- [almere](municipality:Almere)
- [eindhoven]
- [enschot](municipality:Tilburg)
- [Tilburg](municipality)
- [amsterdam]
- [Almere](municipality)
- [De bilt](municipality)
- Voor [Amsterdam](municipality)
- [Enschede](municipality)
- [sgravenmoer](municipality:Dongen)
- [Tilburg](municipality)
- In [Ede](municipality)
- [udenhout](municipality:Tilburg)
- [Best](municipality)
- [Utrecht](municipality)
- [dongen](municipality:Dongen)
- [Utrecht](municipality)

## intent:product_afspraak_info
- Hoe maak ik een afspraak?
- Hoe kan ik een afspraak wijzigen?
- In welke gevallen kan ik een spoedafspraak maken?
- Na hoeveel dagen kan ik de afhaalafspraak plannen?
- afspraak
- Is een afspraak nodig als ik langs wil komen?
- Is een afspraak aan de balie mogelijk?
- Hoe kan ik een afspraak plannen?
- Online afspraak maken lukt niet
- ik wil mijn afspraak afzeggen
- Kan ik ook langskomen bij de gemeente?
- is de wachttijd langer zonder afspraak
- Kan ik hier een afspraak maken?
- Moet ik voor het ophalen van mijn paspoort een afsrpaak maken?

## intent:product_afvalcontainer_info
- Mijn kliko zit steeds overvol. Kan ik een tweede krijgen?
- ik gebruik mijn container niet
- zijn er meerdere maten voor een vuilniscontainer?
- Mag ik een andere container?
- de afvalbak is kapot
- Ik heb mijn afvalbak niet nodig
- mijn afvalbak is kapot, ik wil een nieuwe
- ik wil een nieuwe rolcontainer
- Kunnen jullie de grijze container met de blauwe deksel komen ophalen?
- Kan ik een kleinere kliko krijgen?
- Kan ik een kleinere kliko krijgen?
- Kan ik de kliko omruilen?
- Kan iedereen een afvalcontainer krijgen?
- Mijn afvalbak is gejat
- Ik heb geen ruimte in mijn tuin, kan ik een draagbare bak krijgen?
- afvalcontainer
- Kan ik een andere minicontainer krijgen?
- Is de groene kliko hetzelfde als een bruine kliko?
- Kan ik een andere minicontainer krijgen?
- ik wil een nieuwe rolcontainer aanvragen
- Ik heb een kliko gevonden, kunnen jullie deze ophalen?
- mijn minicontainer is te groot, kan ik een kleinere krijgen?
- Kan ik de grijze kliko met blauw deksel teruggeven?
- Ik woon in een nieuwbouwhuis, maar heb nog geen kliko.  Krijg ik er nog één?
- MIjn kliko is gestolen, krijg ik een nieuwe?
- Mijn container is gejat. Hoe kan ik een nieuwe krijgen?
- kan ik een nieuwe papiercontainer krijgen?
- Kom ik in aanmerking voor een afvalcontainer?
- Mijn afvalbak is overbodig
- Ik heb te weinig aan 1 kliko. Krijg ik gratis een tweede?
- Mag ik een andere container?
- Ik heb te weinig aan een kliko. Krijg ik gratis een tweede?
- De chip van mijn container is kapot.
- ik wil een nieuwe papiercontainer, kan dat?
- Ik ben mijn container kwijt, kan ik een nieuwe krijgen?
- mijn vuilcontainer is gestolen, kan ik een nieuwe bestellen?

## intent:product_afvalkalender_info
- is er een huisvuilkalender
- Waneer wordt het vuilinis opgehaald
- Wat is het schema voor huisvuil
- Wanneer wordt de grijze bak opgehaald
- Mijn afval is niet opgehaald
- Op welke dag wordt de kliko opgehaald
- Wat is het schema voor afval
- Ik zoek de afvalwijzer
- wanneer wordt de groene bak geleegd
- Wanneer wordt de afvalbak geleegd
- Wat is het schema voor vuilnis
- Kan ik een afvalkalender krijgen?
- afvalkalender
- Wanneer kan ik mijn kliko buiten zetten
- Hoe vaak wordt de afvalcontainer opgehaald
- Wanneer kan ik het afval buiten zetten
- wat is de ophaaldag voor afval
- Welke dag is de afvalinzameling
- wanneer wordt de blauwe bak opgehaald

## intent:product_belasting-kwijtschelding_info
- Ik kan de belasting niet betalen
- Ik heb te weinig geld om de belasting te betalen
- Kom ik in aanmerling voor ontheffing belasting?
- Kom ik in aanmerking voor vrijstelling belasting?
- Kan ik kwijtscheling aanvragen
- Ik heb geen geld om de belasting te betalen
- Kan ik kweitschelding krijgen?
- Ik hoor niks van Canock Chase
- Kunt u de belasting kwijt schelden
- Kan ik belasting terugkrijgen?
- Ik wil teruggave belasting
- Ik kan de belasting niet betalen
- Mijn aanvraag kwijtscheling ligt bij Cannock Chase
- Ik wil een kwijtscheldingsformulier
- Kan ik nu mijn bezwaar maken?
- Ik wil kwijtschelding aanvragen

## intent:product_bijstand_info
- Ik ben ondernemer, maar verdien niet genoeg. Kan ik bijstand zelfstandigen krijgen?
- Ons leefgeld is nog niet gestort, wanneer komt dat?
- ik ben dakloos, kom ik in aanmerking voor bijstand?
- De WAO is zo laag, is er extra bijstand mogelijk?
- wanneer kan ik bijstand krijgen?
- De AOW is zo laag, is er extra bijstand mogelijk?
- We zitten zo krap, is er financiele hulp mogelijk?
- Kan ik een uitkering aanvragen?
- Hoe kan ik een uitkering krijgen?
- Ik heb een wajong uitkering
- Kom ik in aanmerking voor inkomensondersteuning?
- We hebben te weinig geld, is daar een uitkering voor?
- Kan ik een bijstandsuitkering aanvragen?
- Ik heb nu een ww uitkering, hoeveel krijg ik als dat afgelopen is?
- bijstand
- kan ik gebruik maken van extra toelagen?
- Wanneer wordt de uitkering gestort?

## intent:product_brp-uittreksel_info
- Ik heb een trouwakte nodig
- Hoe kan ik aan een bewijs van nederlanderschap komen?
- Ik heb een uitreksel uit het geboorteregister nodig
- Heeft u een geboortebewijs voor me?
- Ik heb een uittreksel uit het bevolkingsadministratie nodig
- Ze hebben een bewijs van ongehuwdheid nodig.
- Ik heb een geboorteakte nodig
- Hoe kom ik aan een bewijs van in leven zijn?
- Ik wil een huwelijksakte aanvragen
- Hoe vraag ik een acte aan?
- Kan ik een uittrekel van mijn persoonlijst krijgen?
- Kan ik een uittreksel uit de BRP krijgen?
- Hoe vraag ik een echtscheidingsakte aan?
- uittreksel
- een uittreksel uit het bevolkingsregister bestellen
- Hoe kom ik aan een geboortecertificaat?
- brp uittreksel
- Ik heb een geboorteakte nodig
- Kan ik een uittreksel uit het GBA krijgen?
- Kan ik aan een attestaie de vita komen?

## intent:product_geboorteaangifte_info
- Ik wil de geboorte melden van ons kind
- hoe kan ik mijn pasgeboren dochter melden
- Ik wil een geboorte aangeven.
- Hoe kan ik onze pasgeboren baby aangeven.
- Waar doe ik aangifte van mijn kind?
- Kan ik mijn levenloos geboren kind ook laten registreren?
- Kan ik mijn kind erkennen bij de geboorteaangifte?
- Ik wil aangifte doen van de geboorte van ons kind.
- Wie kan mijn kind aangeven?
- geboorte aangeven
- Ik wil de komst van ons kind doorgeven.
- Mijn kind is in het buitenland geboren. Waar moet ik het aangeven?
- Wie mogen de geboorteaangifte doen?
- Mijn kind is geboren. Ik wil dit aangeven.
- Mijn kind is doodgeboren. Wordt er dan wel een geboorteakte opgemaakt?
- Is het ook mogelijk de geboorteaangifte digitaal te doen?
- Kan ik bij de geboorteaangifte zelf kiezen welke achternaam mijn kind krijgt?

## intent:product_id-kaart_abroad
- Kan ik een id-kaart ook in het buitenland aanvragen?
- Hoe kom ik aan een id-kaart als ik in het buitenland woon?
- Hoe vraag ik een id-kaart in het buitenland aan?
- Waar haal ik mijn id-kaart in het buitenland?

## intent:product_id-kaart_applyinfo
- Hoe kom ik aan een id-kaart?
- Ik wil een id-kaart aanvragen, hoe doe ik dat?
- Hoe krijg ik een id-kaart?
- Kan ik een id-kaart aanvragen?

## intent:product_id-kaart_appointment
- Ik wil een id-kaart aanvragen.
- Kan ik een afspraak maken om aan een id-kaart te komen?
- Hoe maak ik een afspraak om id-kaart aan te vragen?
- Kan ik online een afspraak maken voor het aanvragen van een id-kaart?

## intent:product_id-kaart_childnecessities
- Wat heb ik nodig als ik voor mijn zoon een id-kaart ga aanvragen?
- Wat heb ik nodig als ik voor mijn kind een id-kaart aan wil vragen?
- Wat moet ik meenemen als ik een id-kaart aan vraag voor mijn kind?
- Wat moet ik doen als ik voor mijn dochter een id-kaart wil aanvragen?

## intent:product_id-kaart_collect
- Hoe kan ik een id-kaart ophalen?
- Is het mogelijk om een id-kaart zonder afspraak op te halen?
- Kan ik een id-kaart zonder afspraak op komen halen?
- Moet ik een afspraak maken voor het afhalen van mijn id-kaart?

## intent:product_id-kaart_costs
- Hoeveel moet ik voor een id-kaart betalen?
- Hoe duur is een id-kaart?
- Wat betaalt men voor een id-kaart?
- Wat kost een [idkaart](product:id-kaart)?
- Wat betaal ik voor een [idkaart](product:id-kaart)?

## intent:product_id-kaart_costsurgent
- Wat kost het als ik het id-kaart sneller nodig heb?
- Wat kost een spoedaanvraag id-kaart?
- Wat betaal ik als ik het id-kaart met spoed aanvraag?
- Hoeveel meer kost een spoedaanvraag id-kaart?
- Wat zijn de kosten als ik een id-kaart met spoed nodig heb?

## intent:product_id-kaart_delivery
- Kan het id-kaart opgestuurd worden?
- Wordt een id-kaart thuisbezorgd?
- Versturen jullie id-kaart ook per post?
- Hoe duur is het om een id-kaart thuis te laten bezorgen?

## intent:product_id-kaart_eta
- Duurt het lang voordat ik een nieuw id-kaart heb?
- Hoe lang duurt het voor mijn id-kaart klaar is?
- Hoe lang duurt het aanvragen van een id-kaart?
- Kunt u kijken of mijn id-kaart al klaar ligt?
- Hoe lang duurt het voordat ik een nieuw id-kaart heb?
- Is mijn id-kaart al klaar?
- Hoe snel kan ik een nieuw id-kaart hebben?
- Hoe snel kan ik een nieuwe id-kaart hebben?

## intent:product_id-kaart_fingerprint
- Wat doen jullie met mijn vingerafdrukken?
- Is een vingerafdruk verplicht voor een id-kaart?
- Waarom zijn mijn vingerafdrukken nodig?
- Waarvoor hebben jullie mijn vingerafdrukken nodig?

## intent:product_id-kaart_found
- Ik vond mijn id-kaart weer, wat nu?
- Mijn id-kaart vond ik zojuist weer terug.
- Ik heb mijn id-kaart teruggevonden, mag ik die weer gebruiken?
- Wat moet ik doen als ik mijn id-kaart weer heb teruggevonden?

## intent:product_id-kaart_lost
- Moet ik het doorgeven als mijn id-kaart kwijt is?
- Het id-kaart van mijn kind is kwijt, wat moet ik doen?
- Ik kan mijn id-kaart niet meer vinden, wat nu?
- Wat moet ik doen als mijn id-kaart kwijt, vermist of gestolen is?

## intent:product_id-kaart_lostabroad
- Ik ben mijn id-kaart verloren op vakantie. Wat moet ik doen?
- Ik heb mijn id-kaart in het buitenland verloren. Wat moet ik doen?
- Waar vraag ik een tijdelijk id-kaart in het buitenland aan?
- Mijn id-kaart is gestolen in het buitenland. Wat nu?

## intent:product_id-kaart_necessities
- Wat zijn de benodigdheden voor als ik een id-kaart aan wil vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een id-kaart?
- Moet ik iets meenemen als ik een id-kaart aanvraag?
- Wat moet ik bij me hebben als ik een id-kaart aan ga vragen?

## intent:product_id-kaart_noticket
- Kan ik het id-kaart afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik mijn [id](product:id-kaart) zonder bewijs ophalen?
- Is het afhalen van mijn id-kaart mogelijk zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het [identiteitskaart](product:id-kaart) kwijt, wat nu?
- Kan ik het [idkaart](product:id-kaart) ophalen zonder afhaalbewijs?
- Kan ik het [idkaart](product:id-kaart) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het id-kaart ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het id-kaart kwijt, wat nu?

## intent:product_id-kaart_notregistered
- Hoe moet ik een Nederlands id-kaart aanvragen als ik nergens sta ingeschreven?
- Ik wil een id-kaart aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Ik sta nergens ingeschreven, hoe kom ik aan een id-kaart?
- Ik wil me inschrijven en een id-kaart aanvragen, kan dat?

## intent:product_id-kaart_otherperson
- Ik heb last van mijn gezondheid en kan daardoor mijn id-kaart niet ophalen.
- Ik kan niet naar de balie komen. Hoe vraag ik een id-kaart aan?
- Hoe kan ik een id-kaart aanvragen als ik niet langs kan komen?
- Kan iemand anders mijn id-kaart aanvragen of ophalen?

## intent:product_id-kaart_photo
- Wat zijn de regels voor een foto voor een id-kaart?
- Waar moet ik een pasfoto laten maken voor mijn id-kaart?
- Wat zijn de eisen van een pasfoto voor mijn id-kaart?
- Aan welke eisen moet een pasfoto voor id-kaart voldoen?
- Wat voor pasfoto moet ik meenemen voor id-kaart?

## intent:product_id-kaart_requirements
- Mag iedereen een id-kaart aanvragen?
- Wie kan een id-kaart aanvragen?
- Vanaf welke leeftijd mag je een id-kaart aanvragen?
- Hoe oud moet je zijn om een id-kaart aan te vragen?

## intent:product_id-kaart_urgentinfo
- Ik wil snel een id-kaart aanvragen.
- Kan ik versneld een id-kaart aanvragen?
- Ik wil mijn id-kaart eerder hebben, kan dat?
- Kan ik een id-kaart met spoed aanvragen ?

## intent:product_id-kaart_validity
- Tot wanneer is een id-kaart geldig
- Na hoeveel jaar is mijn id-kaart verlopen?
- Hoeveel jaar is mijn id-kaart geldig?
- Hoe lang is een id-kaart geldig?

## intent:product_id-kaart_validityabroad
- Waar is een id-kaart geldig?
- Naar welke landen mag ik met mijn id-kaart?
- In welke landen is een id-kaart geldig?
- Welke landen erkennen mijn id-kaart?

## intent:product_id-kaart_validitycountry
- Kan ik met mijn id kaart naar [China](noidcountries)?
- Is een id-kaart geldig in [Zwitserland](idcountries)
- Kan ik op vakantie naar [Brazilie](noidcountries:Brazilië) met een idkaart?
- Is een id-kaart voldoende als ik op vakantie ga naar [Rusland](noidcountries)?
- Is het mogelijk om met een id-kaart naar [Polen](idcountries) te reizen?
- Mag ik met mijn idkaart naar [Noord-Korea](noidcountries) reizen?
- Kan ik met een idkaart naar [Marokko](noidcountries)?
- Wordt mijn idkaart geaccepteerd in [Italië](idcountries)?
- Is mijn idkaart geldig in [Iran](noidcountries)?

## intent:product_melding-openbare-ruimte_info
- Kan ik overlast door daklozen melden?
- ik heb overlast van een bedrijf
- Het verkeerslicht is niet goed afgesteld.
- ik heb last van ongedierte
- De straatverlichting is kapot
- Mijn huisvuil is niet opgehaald
- kunnen jullie strooien
- Het is hier donker, er is te weinig straatverlichting
- De plastic container puilt uit.
- Er staat al heel lang een fietswrak in de straat.
- De ondergrondsce container is kapot
- ik heb last van bouwlawaai
- De reclamezuil is kapot
- In de straat is een heel gevaarlijke hond, die heeft al eens gebeten
- Er is een verkeersbord omgereden
- Het regent en we hebben op straat wateroverlast
- Het bankje is kapot
- Er liggen hier veel bladeren op de weg
- Kunnen jullie graffiti verwijderen?
- In het tunneltje ligt rotzooi
- Er ligt veel vuil in de struiken hier
- De verkeerssituatie is hier echt onveilig
- Bij de brug ligt afval
- Het papier is niet opgehaald
- De bushalte is vernield door vandalen
- Het bushokje is kapot
- De werkzaamheden zorgen voor overlast
- Het GFT staat er nog steeds
- Kan het gras gemaaid worden?
- In de vijver zit blauwalg
- De put in de straat stroomt over
- Ik wil gladheid melden
- Er is hier veel overlast van geparkeerde auto's
- Kan er hier een fietsenrek komen?
- Er zwerft hier een losse winkelwagen
- Er liggen kerstbomen op de weg
- Het speeltoestel is beschadigd
- Kan ik stankoverlast melden?
- De afvalbak is vol
- Het troittoir is kapot
- De kledingbak is vol
- ik heb overlast
- Er is een straatnaambord verdwenen
- Het stoplicht heeft storing
- Er is een paaltje omver gereden
- De plantenbak is kapot gemaakt
- Ik wil drugsoverlast melden.
- De stoeptegel ligt los
- dat bedrijf zorgt voor veel overlast
- De lantaarnpaal is kapot
- De papiercontainer zit vol.
- ik ervaar overlast
- De textielbak is vol
- De prullenbak is kapot
- Bij het park zijn vaak loslopende honden
- Hier ligt een dood/dode dier langs de weg
- De ondergrondse container zit vol.
- Onze riolering is kapot
- Kan ik overlast door verslaafden melden?
- Er is hier ontzettend veel onkruid in de straat. Ziet er niet uit.
- Ik wil een vervuilde woning melden
- De prullenbak is vol
- We hebben last van de bouw
- Graag wil ik ook alvast via deze weg een melding maken met betrekking tot afval
- De glasbak zit vol.
- Er staat afval naast de ondergrondse container
- Ze doen hier aan illegale kamerverhuur
- Het fietspad is niet geveegd sneeuw
- We hebben overlast van geparkeerde fietsen
- Op de speelplaats ligt afval
- De abri is vernield.

## intent:product_paspoort_abroad
- Kan ik een paspoort ook in het buitenland aanvragen?
- Hoe kom ik aan een paspoort als ik in het buitenland woon?
- Waar haal ik mijn paspoort in het buitenland?
- Hoe vraag ik een paspoort in het buitenland aan?

## intent:product_paspoort_applyinfo
- Hoe kom ik aan een paspoort?
- Ik wil een paspoort aanvragen, hoe doe ik dat?
- Hoe vraag ik een paspoort aan?
- Kan ik online een paspoort aanvragen?

## intent:product_paspoort_appointment
- Ik wil een paspoort aanvragen aan de balie, hoe maak ik een afspraak in [Amsterdam](municipality)?
- Ik wil een paspoort aanvragen in [Tilburg](municipality).
- Ik wil graag een afspraak maken voor het aanvragen van een paspoort.
- Kan ik een afspraak maken om aan een paspoort te komen in [Dongen](municipality)?

## intent:product_paspoort_childabroad
- Heb ik iets nodig om met mijn kind naar het buitenland te reizen?
- Kan ik mijn kind meenemen naar het buitenland?
- Wat heb ik nodig als eenoudergezin om mijn kind mee naar het buitenland te nemen?
- Ik wil met mijn kind naar het buitenland.

## intent:product_paspoort_childadd
- Ik wil mijn kind in mijn paspoort bijschrijven, kan dat?
- Hoe kan ik mijn kind laten bijschrijven in mijn paspoort?
- Ik wil graag mijn kind laten bijschrijven in m'n paspoort.
- Mag mijn zoon/dochter in mijn paspoort bijgeschreven worden?

## intent:product_paspoort_childnecessities
- Wat moet ik meenemen als ik een paspoort aan vraag voor mijn kind?
- Wat moet ik hebben als ik een paspoort voor mijn kind wil halen?
- Wat heb ik nodig als ik voor mijn zoon een paspoort ga aanvragen?
- Wat moet ik doen als ik voor mijn dochter een paspoort wil aanvragen?

## intent:product_paspoort_childpermission
- Mijn ex saboteert mijn vakantie met mijn kind. Wat moet ik doen?
- Ik krijg geen toestemming van mijn ex om met mijn kind op vakantie te gaan.
- Ik wil met mijn kind op vakantie gaan, maar mijn vorige partner geeft hiervoor geen toestemming.
- Mijn ex-partner geeft geen toestemming om met mijn kind te reizen.

## intent:product_paspoort_collect
- Is het mogelijk om een paspoort zonder afspraak op te halen?
- Waar kan ik mijn paspoort afhalen?
- Hoe kan ik een paspoort ophalen in [Amsterdam](municipality)?
- Kan ik een paspoort zonder afspraak op komen halen?

## intent:product_paspoort_costs
- Wat betaal ik voor een paspoort?
- Wat kost een paspoort in [Tilburg](municipality)?
- Hoeveel kost een paspoort in [Utrecht](municipality)?
- Hoeveel moet ik voor een paspoort betalen in [Tilburg](municipality)?
- Wat is de prijs van een paspoort in [dongen](municipality:Dongen)
- Wat kost een paspoort?
- Hoe duur is een paspoort?
- Wat kost een paspoort in [Loon op Zand](municipality)?
- Wat betaalt men voor een paspoort in [Tilburg](municipality)?
- Wat is de prijs van een paspoort?
- Wat betaal ik voor een paspoort in [Tilburg](municipality)?
- Wat betaalt men voor een paspoort?

## intent:product_paspoort_costsurgent
- Wat is de prijs van een spoedaanvraag voor een paspoort?
- Wat is de prijs van een spoedaanvraag voor een paspoort in [Amsterdam](municipality)?
- Wat betaal ik als ik het paspoort met spoed aanvraag in [Ede](municipality)?
- Wat kost het als ik het paspoort sneller nodig heb?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort?
- Wat kost een spoedaanvraag paspoort?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort in [Tilburg](municipality)?
- Wat betaal ik als ik het paspoort met spoed aanvraag?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb?
- Hoeveel meer kost een spoedaanvraag paspoort in [De Bilt](municipality)?
- Wat kost het als ik het paspoort sneller nodig heb in [Dongen](municipality)?

## intent:product_paspoort_delivery
- Hoe duur is het om een paspoort thuis te laten bezorgen in [Dongen](municipality)?
- Versturen jullie paspoort ook per post in [Tilburg](municipality)?
- Ik kan mijn paspoort niet ophalen, kan die opgestuurd worden?
- Wordt een paspoort thuisbezorgd in [De Bilt](municipality)?

## intent:product_paspoort_emergency
- Ik wil een noodpaspoort aanvragen.
- Hoe vraag ik een noodpaspoort aan?
- Wat doe ik als mijn paspoort kwijt is en ik moet vandaag nog weg?
- Ik heb een noodpaspoort nodig, hoe regel ik dat?

## intent:product_paspoort_eta
- Kan ik mijn paspoort al ophalen?
- Hoe lang duurt het voor mijn paspoort klaar is?
- Wanneer is het paspoort klaar?
- Wanneer kan ik het paspoort ophalen?

## intent:product_paspoort_etanotapplied
- Hoe lang duurt het aanvragen van een paspoort?
- Hoe lang duurt het voordat ik een nieuw paspoort heb?
- Hoe snel kan ik een nieuw paspoort hebben?
- Duurt het lang voordat ik een nieuw paspoort heb?

## intent:product_paspoort_fingerprint
- Waarom zijn mijn vingerafdrukken nodig?
- Is een vingerafdruk verplicht voor een paspoort?
- Moet ik een vingerafdruk zetten voor een paspoort?
- Is het nodig dat ik een vingerafdruk laat maken voor een paspoort?

## intent:product_paspoort_found
- Wat moet ik doen als ik mijn paspoort weer heb teruggevonden?
- Ik heb mijn paspoort teruggevonden, mag ik die weer gebruiken?
- Mijn verloren paspoort is weer opgedoken. Wat doe ik daarmee?
- Ik vond mijn paspoort weer, wat nu?

## intent:product_paspoort_lost
- Ik kan mijn paspoort niet meer vinden, wat nu?
- Mijn paspoort is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?
- Ik ben mijn paspoort kwijt?
- Het paspoort van mijn kind is kwijt, wat moet ik doen?

## intent:product_paspoort_lostabroad
- Ik ben mijn paspoort verloren op vakantie. Wat moet ik doen?
- Waar vraag ik een tijdelijk paspoort in het buitenland aan?
- Mijn paspoort is gestolen in het buitenland. Wat nu?
- Ik heb mijn paspoort in het buitenland verloren. Wat moet ik doen?

## intent:product_paspoort_lostreport
- Moet ik ook naar de politie als ik mijn paspoort ben verloren?
- Is het nodig om bij de politie aangifte te doen?
- Is het verplicht om naar de politie te gaan?
- Moet ik geen aangifte bij de politie doen?

## intent:product_paspoort_necessities
- Wat zijn de benodigdheden voor als ik een paspoort aan wil vragen?
- Wat moet ik meenemen bij de aanvraag van een paspoort?
- Welke dingen moet ik meenemen bij de aanvraag van een paspoort?
- Wat ik heb ik nodig als ik een paspoort aan wil vragen?

## intent:product_paspoort_noticket
- Kan ik mijn paspoort zonder bewijs ophalen?
- Kan ik het paspoort afhalen als ik het afhaalbewijs kwijt ben?
- Is het afhalen van mijn paspoort mogelijk zonder afhaalbewijs?
- Kan ik het paspoort ophalen zonder afhaalbewijs?

## intent:product_paspoort_notregistered
- Ik wil een paspoort aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Hoe moet ik een Nederlands paspoort aanvragen als ik nergens sta ingeschreven?
- Ik wil me inschrijven en een paspoort aanvragen, kan dat?
- Hoe vraag je een paspoort aan als je niet ingeschreven staat?

## intent:product_paspoort_otherperson
- Ik kan niet naar de balie komen. Hoe vraag ik een paspoort aan?
- Ik heb last van mijn gezondheid en kan daardoor mijn paspoort niet ophalen.
- Kan iemand anders mijn paspoort aanvragen of ophalen?
- Ik ben slecht ter been en kan mijn paspoort daardoor niet ophalen.

## intent:product_paspoort_paymentmethod
- Kan ik met briefgeld betalen?
- Welke betaalmethode bieden jullie aan?
- Is het mogelijk om met creditcard te betalen?
- Hoe kan ik betalen bij de balie?
- Hoe kan ik betalen?
- Kan ik met pinpas betalen?
- Kan ik met contant geld betalen?

## intent:product_paspoort_photo
- Wat voor pasfoto moet ik meenemen voor paspoort?
- Welke pasfoto moet er op mijn paspoort?
- Wat zijn de regels voor een foto voor een paspoort?
- Wat zijn de eisen van een pasfoto voor mijn paspoort?
- Waar kan ik pasfoto's laten maken voor een paspoort?

## intent:product_paspoort_requirements
- Hoe oud moet je zijn om een paspoort aan te vragen?
- Vanaf welke leeftijd mag je een paspoort aanvragen?
- Wie kan een paspoort aanvragen?
- Wanneer mag je een paspoort aanvragen?

## intent:product_paspoort_urgentinfo
- Kan ik een paspoort met spoed aanvragen?
- Hoe kan ik een paspoort met spoed aanvragen?
- Ik wil snel een paspoort aanvragen.
- Ik wil mijn paspoort eerder hebben, kan dat?

## intent:product_paspoort_validity
- Hoeveel jaar is mijn paspoort geldig?
- Wanneer verloopt mijn paspoort?
- Tot wanneer is een paspoort geldig
- Na hoeveel jaar is mijn paspoort verlopen?

## intent:product_paspoort_validityabroad
- Welke landen erkennen mijn paspoort?
- Waar is een paspoort geldig?
- In welke landen is een paspoort geldig?
- Waar ter wereld is mijn paspoort geldig?

## intent:product_rijbewijs_info
- Hoe lang heb ik een puntenrijbewijs?
- Hoe lang is een beginnersrijbewijs geldig?
- Kan ik mijn rijbewijs ook online verlengen?
- Wat zijn de rijbewijsregels
- Kan ik een afspraak maken voor een nieuw rijbewijs?
- Geldt een autorijbewijs ook voor een scooter?
- Kan ik een buitenlands rijbewijs omwisselen voor een Nederladse?
- Kan ik een rijbewijs digitaal aanvragen?
- Hoe kom ik aan een nieuw rijbewijs?
- Ik moet mijn motorrijbewijs verlengen
- Ik moet mijn groot rijbewijs verlengen
- Hoe lang is een rijbewijs geldig?
- In welke landen is het Europees rijbewijs geldig?
- Ik wil mijn vrachtwagenrijbewijs verlengen
- Hoe kan ik een rijbewijs aanvragen
- Vanaf welke leeftijd kan ik het jongerenrijbewijs halen?
- rijbewijs
- Mag ik al scooterrijbewijs halen?
- Hoeveel dagen duurt de spoedaanvraag voor een rijbewijs
- Heb je voor een speed-pedelec een brommerrijbewijs nodig?

## intent:product_trouwen_info
- Mogen we zelf een trouwlocatie kiezen?
- trouwen
- Hoeveel getuigen mogen we hebben bij het trouwen?
- Hoe kunnen we een voorgenomen huwelijk melden?
- Onze trouwdag is over 3 weken en we hebben de trouwambtenaar nog niet gesproken
- Hoe kunnen wij een trouwdatum vastleggen?
- Mogen we ons trouwfeest in de tuin doen?
- Wij gaan trouwen, kan ik een datum vastleggen?

## intent:product_uittreksel-verhuismelding_proofregistration
- Hoe kan ik aantonen dat ik ergens ingeschreven sta?
- Hoe kan ik bewijzen dat ik ergens ingeschreven ben?
- Wat is een bewijs van inschrijving?
- Kan ik een bewijs van inschrijving krijgen?

## intent:product_verhuismelding_adreswijziging
- Hoe verander ik mijn adres?
- Adreswijziging
- Ik wil mijn nieuwe adres doorgeven
- Kan ik mijn nieuwe adres doorgeven?
- Yo ik heb een nieuw adres
- Verhuizing doorgeven
- Ik heb een nieuw adres
- Hoe geef ik mijn [nieuwe adres](product:verhuismelding) door?
- Hoe kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven?
- Ik wil mijn adres wijzigen
- Hoe geef ik mijn adreswijziging door?
- Waar geef ik mijn [verhuizing](product:verhuismelding) door?
- Ik moet een nieuw adres  doorgeven
- Hoe moet ik een verhuizing doorgeven?

## intent:product_verhuismelding_applyabroad
- Ik wil naar Nederland verhuizen. Hoe moet ik dit doorgeven?
- Ik woon in het buitenland. Hoe geef ik mijn verhuizing dan door?
- Ik verhuis naar Nederland. Moet ik mij inschrijven?
- Hoe geef ik mijn verhuizing vanuit het buitenland door?

## intent:product_verhuismelding_applydigid
- Heb geen DigiD.
- Dat heb ik niet. (vervolgvraag)
- DigiD, dat heb ik niet.
- Ik heb geen DigiD.

## intent:product_verhuismelding_bevestiging
- Krijg ik een bevestiging als ik mijn adres heb gewijzigd
- Sta ik al ingeschreven?
- Hebben jullie mijn verhuizing al verwerkt?
- Is mijn verhuizing al verwerkt?
- Hoe weet ik zeker dat het doorgeven van mijn nieuwe adres gelukt is?
- Hoe weet ik of mijn verhuizing gelukt is?
- Ik heb geen bevestiging van mijn verhuizing gekregen.
- Is mijn verhuizing al doorgevoerd/afgewerkt/behandeld?

## intent:product_verhuismelding_digidinfo
- DigiD, Wat is dat? 
- digid, Wat is dat? 
- DigiD? Nog nooit van gehoord.
- Ik heb geen idee wat DigiD is.
- Wat is DigiD?
- Wat is digid?
- DigiD?
- vertel mij meer over DigiD

## intent:product_verhuismelding_dontwantdigid
- Ik wil geen gebruik maken van DigiD.
- DigiD wil ik niet.
- Ik ben tegen digid.
- Ik vertrouw DigiD niet., Ik wil geen DigiD gebruiken.

## intent:product_verhuismelding_emigrate
- Ik ga reizen, moet ik me uitschrijven?
- Ik verhuis naar het buitenland, moet ik me uitschrijven?
- Ik ga naar het buitenland, is het nodig dat ik me uitschrijf?
- Wanneer moet ik me uitschrijven bij de gemeente?

## intent:product_verhuismelding_formerror
- Online doorgeven lukt niet. Hoe kan ik nu mijn adreswijziging doorgeven?
- De website loopt vast, hoe geef ik nu mijn verhuizing door?
- Ik wilde mijn adres wijzigen maar hij pakt het verkeerde adres. Hoe kan ik nu mijn verhuizing doorgeven?
- Ik begrijp de vragen niet, kan ik mijn verhuizing ook op een andere manier doorgeven
- Mijn adres is geheim, hoe geef ik dan mijn verhuizing door?
- Het lukt bij verhuizen niet om mijn adres in te voeren. Hoe geef ik het nu door?
- Ik verhuis naar een verzorgingshuis maar het lukt niet online. Hoe doe ik dat nu?
- Ik probeerde mijn verhuizing door te geven maar mijn adres wordt niet herkend. Wat nu?

## intent:product_verhuismelding_kindinschrijven
- Ik wil mijn kind inschrijven op een ander adres.
- Kan ik mijn meerderjarige kind meeverhuizen?
- Kan ik mijn kind mee verhuizen?
- Kan ik dat ook voor mijn kind doorgeven? (vervolgvraag)
- Ik wil het adres van mijn kind wijzigen.
- Mijn kind(eren) verhuist/verhuizen mee, kan ik dat gelijk doorgeven?
- Ik wil mijn kind inschrijven.
- Kan ik mijn kind verhuizen?

## intent:product_verhuismelding_kindinschrijvenanderadres
- Kan ik het nieuwe adres doorgeven van mijn studerende kind?
- Hoe geef ik het nieuwe adres van mijn studerende zoon door?
- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven?
- Mijn dochter gaat in de stad studeren, hoe kan ik het nieuwe adres doorgeven?

## intent:product_verhuismelding_mandatory
- Moet ik mijn verhuizing doorgeven?
- Moet ik doorgeven dat ik verhuisd ben?
- Is inschrijven op je nieuwe adres verplicht?
- Is het verplicht mijn adreswijziging door te geven?

## intent:product_verhuismelding_when
- Kan ik mijn verhuizing nog doorgeven?
- Kan ik mijn [verhuizing](product:verhuismelding) al doorgeven?
- Als ik volgende maand [verhuis](product:verhuismelding), kan ik dat dan nu al doorgeven?
- Op welk moment moet ik mijn verhuizing doorgeven?
- ik ga in oktober [verhuizen](product:verhuismelding), kan ik dat nu al doorgeven?
- Wanneer willen jullie mijn nieuwe adres weten?
- kan ik mijn verhuizing doorgeven?
- Wanneer kan ik mn [verhiuzin](product:verhuismelding) doorgeven
- kan ik ons als nieuwe bewoners inschrijven?
- Wanneer kan ik mijn verhuismelding doorgeven
- Op welk moment moet ik mijn [verhuizing](product:verhuismelding) doorgeven
- Binnen welke termijn moet ik mij inschrijven op mijn nieuwe adres?
- Ik ga [verhuizen](product:verhuismelding) naar Utrecht. Wanneer moet ik dit doorgeven?
- Ik ben waarschijnlijk te laat met het doorgeven van mijn adreswijziging.
- Volgende maand [verhuis](product:verhuismelding) ik. Mag ik dat nu al doorgeven?
- Wanneer kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven
- Wanneer willen jullie [mijn nieuwe adres](product:verhuismelding) weten
- Ik ben [verhuisd](product:verhuismelding). Tot wanneer kan ik dit aangeven?
- Ik ben vergeten mijn verhuizing door te geven.
- Wanneer moet ik mijn verhuizing doorgeven
- Wanneer geef ik mijn adreswijziging door?
- Wanneer geef ik mijn [adreswijziging](product:verhuismelding) door

## intent:product_verhuismelding_when_info
- Kan ik mijn verhuizing al eerder doorgeven?
- Kan ik mij al eerder inschrijven in de gemeente?
- Is inschrijven al voor de verhuizing mogelijk?
- Kan ik mijn adreswijziging al eerder doorgeven?
- Hoe lang van tevoren moet ik mijn verhuizing doorgeven?
- Hoe ver van tevoren kan ik [verhuizing](product:verhuismelding) doorgeven?

## intent:restart
- opnieuw
- Herstart
- opnieuw invoeren
- Ik wil opnieuw beginnen
- restart
- Reboot

## intent:thanks
- Bedankt!
- thanks
- enorm bedankt!
- dankje
- bedankt he
- dank je
- bedankt voor de hulp
- Super!
- dank u

## intent:unfriendly
- klote bot
- stomme chatbot
- fuck you
- stomme bot
- klote chatbot
- Rot op
- ROT OP GEM
- ga toch weg robot
- FUCK YOU

## intent:unfriendly_questions
- Doe niet zo dom.
- ben je dom ofzo
- waarom snap je niets
- je snapt ook niets

## synonym:Almere
- almere

## synonym:Amsterdam
- amsterdam

## synonym:Brazilië
- Brazilie

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## synonym:Utrecht
- utrecht

## synonym:id-kaart
- idkaart
- identiteitskaart
- id

## synonym:verhuismelding
- verhuizing
- adreswijziging
- mijn nieuwe adres
- verhuizen
- verhuisd
- verhuis
- verhiuzin
- nieuwe adres
- gaan wonen

## lookup:municipality
  data/municipalities.txt

## lookup:idcountries
  data/idcountries.txt

## lookup:noidcountries
  data/noidcountries.txt

## lookup:product
- paspoort
- parkeervergunning
- id-kaart
- overlijdensakte
- verhuismelding
- bijstand
- uitkering
- hondenbelasting
- afvalcontainer
- belasting-kwijtschelding
- brp-uittreksel
- trouwen
- afspraak
- afvalkalender
- rijbewijs
- geboorteaangifte
- melding-openbare-ruimte
- reisdoc-costs-urgent
- reisdoc-costs-paymentmethod
