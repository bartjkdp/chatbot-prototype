## intent:affirmative
- ja
- helemaal geweldig
- alsjeblieft

## intent:apply_product
- Kan ik dat nu doorgeven?

## intent:ask_burgemeesterdongen
- burgemeester Dongen?
- wie zit de gemeenteraad voor in Dongen?

## intent:ask_burgemeestertilburg
- Burgemeerster Tilburg?
- Wie is de burgemeester in Tilburg?

## intent:ask_burgemeesterutrecht
- Wie is de burgemeester van utrecht?
- Wie is de burgemeester in utrecht

## intent:ask_directeur
- wie is de directeur?

## intent:ask_directeurvdp
- directeur VDP

## intent:ask_for_chatbot
- ik wil verder met de chatbot
- Ga verder met de bot

## intent:ask_for_human
- ik wil een mens spreken
- ga weg gem
- kan ik een mensen spreken
- Verbind me even door met een echt mens

## intent:ask_population
- weet jij hoeveel mensen er in Nederland wonen?
- Hoeveel inwoners hebben we in Nederland

## intent:ask_weather
- is het droog buiten?
- moet ik mijn paraplu pakken?
- wat voor weer is het?

## intent:cancel
- annuleren

## intent:chitchat
- een vaasje
- ik wil een biertje
- wanneer is de borrel

## intent:denial
- nope
- echt niet

## intent:dontknow
- daar vraag je me wat
- dat weet ik niet

## intent:greet
- hoi
- hi
- Hi, ik ben [Harvey](name)
- heey
- Hey, ik ben [Jeroen](name)

## intent:inform
- In [Ede](municipality)
- Dat is [Dongen](municipality)
- [Rotterdam](municipality)
- [utrecht](municipality:Utrecht)
- [enschot](municipality:Tilburg)
- Gemeente [Rotterdam](municipality)
- [dongen](municipality:Dongen)
- [tilburg]
- [Tilburg](municipality)
- ['sgravenmoer](municipality:Dongen)
- [enschede]

## intent:product_afspraak_info
- Kan ik hier een afspraak maken?
- ik wil mijn afspraak afzeggen
- Kan ik langskomen of gaat dit op afspraak?
- Kan ik ook langskomen bij de gemeente?

## intent:product_afvalcontainer_info
- De chip van mijn container is kapot.
- Kan ik een kleinere kliko krijgen?
- Kan ik de kliko omruilen?
- Kan ik de grijze kliko met blauw deksel teruggeven?
- ik wil een nieuwe rolcontainer
- Moet ik thuis zijn als jullie de grijze kliko met oranje deksel komen brengen?
- Mijn grijze kliko is te klein, kan ik er twee krijgen?
- Is de bruine kliko ook voor GFT?
- ik wil een nieuwe rolcontainer aanvragen
- Ik heb te weinig aan 1 kliko. Krijg ik gratis een tweede?

## intent:product_afvalkalender_info
- Wanneer kan ik het afval buiten zetten
- Ik zoek de afvalwijzer
- Wat is het schema voor huisvuil
- Wanneer wordt de afvalbak geleegd
- Ik snap het ophaalschema voor vuilnis niet

## intent:product_belasting-kwijtschelding_info
- Kom ik in aanmerling voor ontheffing belasting?
- Ik heb geen geld om de belasting te betalen
- Ik kan de belasting niet betalen
- Kan ik kwijtscheling aanvragen
- Kan ik kweitschelding krijgen?

## intent:product_bijstand_info
- Kom ik in aanmerking voor inkomensondersteuning?
- Kan ik een werkeloosheidsuitkering aanvragen?
- Kan ik een bijstandsuitkering aanvragen?
- Ik wil graag een sociale uitkering
- ik had werk gevonden, maar dit stopt.  kan ik opnieuw een uitkering aanvragen?

## intent:product_brp-uittreksel_info
- Hoe kom ik aan een scheidingsakte?
- Hoe vraag ik een acte aan?
- een uittreksel uit het bevolkingsregister bestellen
- Ik wil een huwelijksakte aanvragen
- Kan ik een uittrekel van mijn persoonlijst krijgen?

## intent:product_geboorteaangifte_info
- hoe kan ik mijn pasgeboren dochter melden
- hoe kan ik mijn pasgeboren dochter melden
- Mijn kind is geboren. Ik wil dit aangeven.
- Ik wil een melding maken van ons geboren kind.
- Mijn kind is in het buitenland geboren. Waar moet ik het aangeven?

## intent:product_id-kaart_abroad
- Ik woon in het buitenland, hoe kom ik aan mijn id-kaart?

## intent:product_id-kaart_applyinfo
- Hoe kom ik aan een id-kaart?
- Kan ik een id-kaart aanvragen?

## intent:product_id-kaart_appointment
- Kan ik online een afspraak maken voor het aanvragen van een id-kaart?

## intent:product_id-kaart_childnecessities
- Wat heb ik nodig als ik voor mijn kind een id-kaart aan wil vragen?

## intent:product_id-kaart_collect
- Is het mogelijk om een id-kaart zonder afspraak op te halen?
- Waar kan ik mijn id-kaart afhalen?

## intent:product_id-kaart_costs
- Hoe duur is een id-kaart?
- Wat betaal ik voor een [idkaart](product:id-kaart)?

## intent:product_id-kaart_costsurgent
- Wat betaal ik als ik het id-kaart met spoed aanvraag?
- Hoeveel meer kost een spoedaanvraag id-kaart?

## intent:product_id-kaart_delivery
- Kan het id-kaart opgestuurd worden?

## intent:product_id-kaart_eta
- Hoe snel kan ik een nieuwe id-kaart hebben?
- Hoe lang duurt het voordat ik een nieuw id-kaart heb?
- Hoe snel kan ik een nieuw id-kaart hebben?

## intent:product_id-kaart_fingerprint
- Moet ik een vingerafdruk zetten voor een id-kaart?
- Waarom zijn mijn vingerafdrukken nodig?

## intent:product_id-kaart_found
- Ik heb mijn id-kaart teruggevonden, mag ik die weer gebruiken?

## intent:product_id-kaart_lost
- Wat moet ik doen als mijn id-kaart kwijt, vermist of gestolen is?

## intent:product_id-kaart_lostabroad
- Ik heb mijn id-kaart in het buitenland verloren. Wat moet ik doen?

## intent:product_id-kaart_necessities
- Wat zijn de benodigdheden voor als ik een id-kaart aan wil vragen?
- Wat moet ik meenemen bij de aanvraag van een id-kaart?

## intent:product_id-kaart_noticket
- Is het afhalen van mijn [identiteitskaart](product:id-kaart) mogelijk zonder afhaalbewijs?
- Is het afhalen van mijn id-kaart mogelijk zonder afhaalbewijs?

## intent:product_id-kaart_notregistered
- Ik wil een id-kaart aanvragen, maar ik sta niet ingeschreven. Wat nu?

## intent:product_id-kaart_otherperson
- Ik ben slecht ter been en kan mijn id-kaart daardoor niet ophalen.

## intent:product_id-kaart_photo
- Welke pasfoto moet er op mijn id-kaart?
- Wat voor pasfoto moet ik meenemen voor id-kaart?

## intent:product_id-kaart_requirements
- Hoe oud moet je zijn om een id-kaart aan te vragen?

## intent:product_id-kaart_urgentinfo
- Ik wil mijn id-kaart eerder hebben, kan dat?

## intent:product_id-kaart_validity
- Hoeveel jaar is mijn id-kaart geldig?

## intent:product_id-kaart_validityabroad
- Waar ter wereld is mijn id-kaart geldig?

## intent:product_id-kaart_validitycountry
- Accepteert [Duitsland](idcountries) een id-kaart?
- Is een id-kaart voldoende als ik op vakantie ga naar [Rusland](noidcountries)?
- Is het mogelijk om met een id-kaart naar [Polen](idcountries) te reizen?

## intent:product_melding-openbare-ruimte_info
- Het papier is niet opgehaald
- dat bedrijf zorgt voor veel overlast
- Het troittoir is kapot
- ik heb overlast
- Bij de brug ligt afval
- Er staat afval naast de ondergrondse container
- De afvalbak is vol
- Ik wil een vervuilde woning melden
- Ik wil gladheid melden
- Mijn huisvuil is niet opgehaald
- De kade is beschadigd
- Ze doen hier aan illegale kamerverhuur
- Het bankje is kapot
- Kunnen jullie graffiti verwijderen?
- ik ervaar overlast
- In het tunneltje ligt rotzooi
- Er is een straatnaambord verdwenen
- We hebben last van de bouw

## intent:product_paspoort_abroad
- Hoe vraag ik een paspoort in het buitenland aan?

## intent:product_paspoort_applyinfo
- Hoe kom ik aan een paspoort?
- Hoe krijg ik een paspoort in [Amsterdam](municipality)?

## intent:product_paspoort_appointment
- Ik wil graag een afspraak maken voor het aanvragen van een paspoort.
- Ik wil een paspoort aanvragen aan de balie, hoe maak ik een afspraak in [Amsterdam](municipality)?

## intent:product_paspoort_childabroad
- Wat heb ik nodig als eenoudergezin om mijn kind mee naar het buitenland te nemen?

## intent:product_paspoort_childadd
- Ik wil mijn kind in mijn paspoort bijschrijven, kan dat?

## intent:product_paspoort_childnecessities
- Wat moet ik doen als ik voor mijn dochter een paspoort wil aanvragen?

## intent:product_paspoort_childpermission
- Ik wil met mijn kind op vakantie gaan, maar mijn vorige partner geeft hiervoor geen toestemming.

## intent:product_paspoort_collect
- Hoe kan ik een paspoort ophalen in [Amsterdam](municipality)?
- Is het mogelijk om een paspoort zonder afspraak op te halen?

## intent:product_paspoort_costs
- Hoeveel moet ik voor een paspoort betalen in [Tilburg](municipality)?
- Wat kost een paspoort?
- Wat betaalt men voor een paspoort?

## intent:product_paspoort_costsurgent
- Hoeveel meer kost een spoedaanvraag paspoort in [De Bilt](municipality)?
- Wat is de prijs van een spoedaanvraag voor een paspoort?
- Wat is de prijs van een spoedaanvraag voor een paspoort in [Amsterdam](municipality)?

## intent:product_paspoort_delivery
- Wordt een paspoort thuisbezorgd in [De Bilt](municipality)?

## intent:product_paspoort_emergency
- Hoe vraag ik een noodpaspoort aan?

## intent:product_paspoort_eta
- Wanneer kan ik het paspoort ophalen?
- Wanneer is het paspoort klaar?

## intent:product_paspoort_etanotapplied
- Hoe lang duurt het voordat ik een nieuw paspoort krijg?

## intent:product_paspoort_fingerprint
- Waarvoor hebben jullie mijn vingerafdrukken nodig?
- Wat doen jullie met mijn vingerafdrukken?

## intent:product_paspoort_found
- Ik vond mijn paspoort weer, wat nu?

## intent:product_paspoort_lost
- Ik kan mijn paspoort niet meer vinden, wat nu?
- Mijn paspoort is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?

## intent:product_paspoort_lostabroad
- Ik ben mijn paspoort verloren op vakantie. Wat moet ik doen?

## intent:product_paspoort_lostreport
- Is het verplicht om naar de politie te gaan?

## intent:product_paspoort_necessities
- Wat moet ik bij me hebben als ik een paspoort aan ga vragen?
- Wat ik heb ik nodig als ik een paspoort aan wil vragen?

## intent:product_paspoort_noticket
- Kan ik het paspoort afhalen als ik het afhaalbewijs kwijt ben?

## intent:product_paspoort_notregistered
- Ik wil me inschrijven en een paspoort aanvragen, kan dat?

## intent:product_paspoort_otherperson
- Kan iemand anders mijn paspoort aanvragen of ophalen?

## intent:product_paspoort_paymentmethod
- Hoe kan ik betalen bij de balie?
- Kan ik met contant geld betalen?

## intent:product_paspoort_photo
- Welke pasfoto moet er op mijn paspoort?
- Aan welke eisen moet een pasfoto voor paspoort voldoen?

## intent:product_paspoort_requirements
- Hoe oud moet je zijn om een paspoort aan te vragen?

## intent:product_paspoort_urgentinfo
- Ik wil mijn paspoort eerder hebben, kan dat?

## intent:product_paspoort_validity
- Hoe lang is een paspoort geldig?

## intent:product_paspoort_validityabroad
- Naar welke landen mag ik met mijn paspoort?

## intent:product_rijbewijs_info
- rijbewijs
- Na hoeveel dagen kan ik mijn rijbewijs ophalen?
- Ik ben mijn rijbewijs kwijt. Wat moet ik doen?
- Kan ik een buitenlands rijbewijs omwisselen voor een Nederladse?
- Kan ik een rijbewijs digitaal aanvragen?
- Kan ik mijn rijbewijs ook online verlengen?

## intent:product_trouwen_info
- Onze trouwdag is over 3 weken en we hebben de trouwambtenaar nog niet gesproken
- Is de trouwzaal beschikbaar?

## intent:product_uittreksel-verhuismelding_proofregistration
- Kan ik een uittreksel krijgen waarop staat waar ik woon?

## intent:product_verhuismelding_adreswijziging
- Ik heb een adreswijziging
- Kan ik mijn nieuwe adres doorgeven?
- Hoe kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven?
- Waar geef ik mijn [verhuizing](product:verhuismelding) door?

## intent:product_verhuismelding_applyabroad
- Hoe geef ik mijn verhuizing vanuit het buitenland door?

## intent:product_verhuismelding_applydigid
- Ik heb geen DigiD.

## intent:product_verhuismelding_bevestiging
- Hebben jullie mijn verhuizing al verwerkt?
- Hoe weet ik of mijn verhuizing gelukt is?
- Sta ik al ingeschreven?

## intent:product_verhuismelding_digidinfo
- Ik weet niet wat DigiD is.

## intent:product_verhuismelding_dontwantdigid
- DigiD wil ik niet.

## intent:product_verhuismelding_emigrate
- Wanneer moet ik me uitschrijven bij de gemeente?

## intent:product_verhuismelding_formerror
- Ik wilde mijn adres wijzigen maar hij pakt het verkeerde adres. Hoe kan ik nu mijn verhuizing doorgeven?
- Online doorgeven lukt niet. Hoe kan ik nu mijn adreswijziging doorgeven?

## intent:product_verhuismelding_kindinschrijven
- Kan ik dat ook voor mijn kind doorgeven? (vervolgvraag)
- Kan ik mijn kind verhuizen?

## intent:product_verhuismelding_kindinschrijvenanderadres
- Hoe geef ik het nieuwe adres van mijn studerende zoon door?

## intent:product_verhuismelding_mandatory
- Moet ik mijn verhuizing doorgeven?
- Moet ik mij inschrijven op mijn nieuwe adres?

## intent:product_verhuismelding_when
- Wanneer kan ik mn [verhiuzin](product:verhuismelding) doorgeven
- Wanneer willen jullie mijn nieuwe adres weten?
- Wanneer geef ik mijn adreswijziging door?
- Kan ik mijn verhuizing nog doorgeven?
- Ik ben vorige weekbij mijn vriend [gaan wonen](product:verhuismelding), wanneer moet ik dat doorgeven?
- Op welk moment moet ik mijn verhuizing doorgeven?

## intent:product_verhuismelding_when_info
- Kan ik mij al voor mijn verhuizing inschrijven?
- Hoeveel eerder kun je je verhuizing doorgeven?

## intent:restart
- Herstart
- opnieuw

## intent:thanks
- Bedankt!
- bedankt
- dank je

## intent:unfriendly
- ga toch weg robot
- fuck you
- FUCK YOU

## intent:unfriendly_questions
- domme bot
- Domme Gem

## synonym:Almere
- almere

## synonym:Amsterdam
- amsterdam

## synonym:Brazilië
- Brazilie

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## synonym:Utrecht
- utrecht

## synonym:id-kaart
- idkaart
- identiteitskaart
- id

## synonym:verhuismelding
- verhuizing
- adreswijziging
- mijn nieuwe adres
- verhuizen
- verhuisd
- verhuis
- verhiuzin
- nieuwe adres
- gaan wonen

## lookup:municipality
  data/municipalities.txt

## lookup:idcountries
  data/idcountries.txt

## lookup:noidcountries
  data/noidcountries.txt

## lookup:product
- paspoort
- parkeervergunning
- id-kaart
- overlijdensakte
- verhuismelding
- bijstand
- uitkering
- hondenbelasting
- afvalcontainer
- belasting-kwijtschelding
- brp-uittreksel
- trouwen
- afspraak
- afvalkalender
- rijbewijs
- geboorteaangifte
- melding-openbare-ruimte
- reisdoc-costs-urgent
- reisdoc-costs-paymentmethod
