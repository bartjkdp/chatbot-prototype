## intent:affirmative
- prima
- yes please!
- graag
- aub
- akkoord
- ja, graag
- dat zou mooi zijn
- ok

## intent:apply_product
- Kan ik dat nu doen?
- Kan ik dat nu aanvragen?

## intent:ask_burgemeesterdongen
- wie is de voorzitter van de gemeenteraad in Dongen?
- Wie is de burgemeester in Dongen?
- Wie is de burgemeester van Dongen?
- Weet jij wie de burgemeester is in Dongen
- wie is de burgervader van dongen

## intent:ask_burgemeestertilburg
- wie is de burgermeester van tilburg?
- Wie is de baas van de politie in Tilburg?
- wie is de voorzitter van de gemeenteraad in tilburg?
- wie is de burgermeester van Tilburg?
- wie is de burgervader van Tilburg?
- Wie is de baas van de politie in Dongen?

## intent:ask_burgemeesterutrecht
- wie is de burgervader van utrecht?
- Wie is de burgemeester in Utrecht?
- Wie is de baas van de politie in Utrecht?
- burgemeester van utrecht?
- wie is de voorzitter van de gemeenteraad in utrecht?
- Weet jij wie de burgemeester is in Utrecht?

## intent:ask_directeur
- wie is de directeur van zaakgerichtwerken?
- wie is de directeur van de hackaton?
- wie is de directeur van ZGW?
- Wie is de directeur?

## intent:ask_directeurvdp
- wie is de directeur van VDP?
- Wie is de directeur van de VDP?
- wie is de directeur van de vdp?

## intent:ask_for_chatbot
- Chat met de chatbot
- Verder praten met de chatbot
- ik wil met de chatbot chatten
- Ga door met de chatbot

## intent:ask_for_human
- kan ik een echt gesprek hebben
- Ga weg Gem
- praat met medewerker
- Ik wil met een mens chatten
- Ik wil met een medewerker spreken
- Ik praat niet met robots
- ga weg
- Ga weg robot
- Ik wil een mens aan de lijn
- ik praat niet met chatbots
- ik wil iemand spreken van burgerzaken
- echt mens aub
- Ik praat niet met bots

## intent:ask_population
- hoeveel nederlanders zijn er
- Hoeveel inwoners heeft nederland?
- inwoners nederland
- hoeveel mensen wonen er in Nederland
- hoeveel inwoners heeft nederland

## intent:ask_weather
- regent het?
- is het droog?
- Mooi weer buiten
- Lekker weertje hè?
- valt er regen?
- wat is de weersvoorspelling?
- gaat het regenen?
- weet je wat voor weer het wordt?
- wat voor weer is het daar?
- lekker weer he?
- is het droog buiten?

## intent:cancel
- ik wil afbreken aub
- stop
- ik wil stoppen
- kunnen we stoppen?

## intent:chitchat
- ik heb zin in bier
- een vaasje aub
- een Hertog Jan
- een La Trappe blond aub
- mag ik een pilsje
- een fluitje
- ik wil een biertje bestellen
- mag ik een biertje

## intent:denial
- nee
- alsjeblieft niet
- nee, bedankt
- zeker niet
- neen

## intent:dontknow
- Joost mag het weten
- zou het niet weten
- ?
- geen idee

## intent:greet
- Hallo [Utrecht](municipality)! Mijn naam is [Piet](name)
- hallo [Tilburg](municipality)!
- Hallo hier [Ali](name)
- Hallo, [Emma](name) hier!
- Hoi mijn naam is [Wim](name)
- goede morgen
- goedenavond [Utrecht](municipality)
- Hallo [Ede](municipality), ik ben [Wendy](name)
- Hallo
- hey gem, mijn naam is [jason](name)
- hey
- goedenavond
- heuy
- goede middag
- Hoi [Dongen](municipality)
- Hoi [Uden](municipality)

## intent:inform
- [Almere](municipality)
- [amsterdam](municipality:Amsterdam)
- [Biezenmortel](municipality:Tilburg)
- [De Bilt](municipality)
- [Utrecht](municipality)
- [Vaart](municipality:Dongen)
- [dongen](municipality:Dongen)
- [Tilburg](municipality)
- [Enschede](municipality)
- [Amsterdam](municipality)
- [Klein Dongen](municipality:Dongen)
- [udenhout](municipality:Tilburg)
- [eindhoven]
- [klein dongen](municipality:Dongen)
- [Utrecht](municipality)
- [best]
- [vaart](municipality:Dongen)
- [Ede](municipality)
- [De bilt](municipality)
- [ede]
- [almere](municipality:Almere)
- [amsterdam]
- [utrecht]
- [tilburg](municipality:Tilburg)
- ['s Gravenmoer](municipality:Dongen)
- [Utrecht](municipality)
- [Buren](municipality)
- [sgravenmoer](municipality:Dongen)
- [biezenmortel](municipality:Tilburg)
- [Udenhout](municipality:Tilburg)
- Gemeente [Tilburg](municipality)
- [Amsterdam](municipality)
- [Berkel-Enschot](municipality:Tilburg)
- [Eindhoven](municipality)
- [buren]
- [berkel](municipality:Tilburg)
- Voor [Amsterdam](municipality)
- [Arnhem](municipality)
- [Best](municipality)
- [Dongen](municipality)

## intent:product_afspraak_info
- Hoe kan ik een afspraak maken
- afspraak
- ik wil een gesprek met de gemeente
- Moet ik voor het ophalen van mijn paspoort een afsrpaak maken?
- Ik wil persoonlijk contact met de gemeente
- is de wachttijd langer zonder afspraak
- Online afspraak maken lukt niet
- Na hoeveel dagen kan ik de afhaalafspraak plannen?
- Is een afspraak aan de balie mogelijk?
- Is een afspraak nodig als ik langs wil komen?
- Hoe kan ik een afspraak wijzigen?
- Hoe maak ik een afspraak?
- In welke gevallen kan ik een spoedafspraak maken?
- Hoe kan ik een afspraak plannen?

## intent:product_afvalcontainer_info
- Kan ik een andere minicontainer krijgen?
- Ik heb geen ruimte in mijn tuin, kan ik een draagbare bak krijgen?
- Mijn afvalbak is gejat, kost het geld om een nieuwe te krijgen?
- Ze hebben mijn container gestolen. Wanneer krijg ik een nieuwe?
- Mijn afvalbak is gejat
- ik wil een nieuwe papiercontainer, kan dat?
- Mijn kliko zit steeds overvol. Kan ik een tweede krijgen?
- Mag ik een andere container?
- Is de groene kliko hetzelfde als een bruine kliko?
- Ik heb te weinig aan een kliko. Krijg ik gratis een tweede?
- Mijn container is gejat. Hoe kan ik een nieuwe krijgen?
- kan ik een nieuwe papiercontainer krijgen?
- Kunnen jullie de grijze container met de blauwe deksel komen ophalen?
- Ze hebben mijn kliko gejat, kan ik een nieuwe krijgen
- Mag ik een andere container?
- Ze hebben mijn kliko gestolen. Krijg ik gratis een nieuwe?
- de afvalbak is kapot
- afvalcontainer
- Ik woon in een nieuwbouwhuis, maar heb nog geen kliko.  Krijg ik er nog één?
- mijn minicontainer is te groot, kan ik een kleinere krijgen?
- Kan ik een kleinere kliko krijgen?
- mijn vuilcontainer is gestolen, kan ik een nieuwe bestellen?
- Ik heb een grote tuin, kan ik meerdere containers krijgen?
- Kom ik in aanmerking voor een afvalcontainer?
- zijn er meerdere maten voor een vuilniscontainer?
- ik gebruik mijn container niet
- Kan iedereen een afvalcontainer krijgen?
- Ik ben mijn container kwijt, kan ik een nieuwe krijgen?
- Ik heb mijn afvalbak niet nodig
- Ik heb een kliko gevonden, kunnen jullie deze ophalen?
- Mijn container staat in de weg
- Mijn afvalbak is overbodig
- mijn afvalbak is kapot, ik wil een nieuwe
- MIjn kliko is gestolen, krijg ik een nieuwe?
- Kan ik een andere minicontainer krijgen?
- Ik woon in een flat. Kan ik een citybin krijgen?

## intent:product_afvalkalender_info
- Wanneer kan ik mijn kliko buiten zetten
- Wat is het schema voor vuilnis
- Mijn afval is niet opgehaald
- is er een huisvuilkalender
- Op welke dag wordt de kliko opgehaald
- Hoe vaak wordt de afvalcontainer opgehaald
- Wanneer wordt de grijze bak opgehaald
- wanneer wordt de minicontainer opgehaald
- Kan ik een afvalkalender krijgen?
- wanneer wordt de blauwe bak opgehaald
- Waneer wordt het vuilinis opgehaald
- wat is de ophaaldag voor afval
- wanneer wordt de groene bak geleegd
- Wat is het schema voor afval
- Welke dag is de afvalinzameling
- waar vind ik de inzamelkalender voor afval
- afvalkalender
- Wanneer wordt mijn afval opgehaald
- Wanneer komen ze het huisvuil ophalen

## intent:product_belasting-kwijtschelding_info
- Kan ik belasting terugkrijgen?
- Ik wil kwjitschelding aanvragem
- Ik kan geen belasting betalen
- Kunt u de belasting kwijt schelden
- Ik wil kwijtschelding aanvragen
- Ik wil teruggave belasting
- Ik kan de belasting niet betalen
- Ik wil kwijtschelding aanvragen
- Ik heb te weinig geld om de belasting te betalen
- Ik wil kwijdschelding van belasting
- Kom ik in aanmerking voor vrijstelling belasting?
- Ik hoor niks van Canock Chase
- belasting kwijtschelding
- Mijn aanvraag kwijtscheling ligt bij Cannock Chase
- Kan ik nu mijn bezwaar maken?
- Ik wil een kwijtscheldingsformulier

## intent:product_bijstand_info
- Kan ik een uitkering aanvragen?
- We hebben te weinig geld, is daar een uitkering voor?
- wanneer kan ik bijstand krijgen?
- Kan ik extra inkomen krijgen?
- bijstand
- Ik heb nu een ww uitkering, hoeveel krijg ik als dat afgelopen is?
- ik ben dakloos, kom ik in aanmerking voor bijstand?
- Ons leefgeld is nog niet gestort, wanneer komt dat?
- kom ik in aanmerking voor bijzondere bijstand?
- Hoe kan ik een uitkering krijgen?
- Ik heb een wajong uitkering
- Ik ben ondernemer, maar verdien niet genoeg. Kan ik bijstand zelfstandigen krijgen?
- De WAO is zo laag, is er extra bijstand mogelijk?
- De AOW is zo laag, is er extra bijstand mogelijk?
- kan ik gebruik maken van extra toelagen?
- We zitten zo krap, is er financiele hulp mogelijk?
- Wanneer wordt de uitkering gestort?

## intent:product_brp-uittreksel_info
- Ik heb een geboorteakte nodig
- Ze hebben een bewijs van ongehuwdheid nodig.
- Kan ik op de website een uitreksel bestellen?
- Hoe kan ik aan een bewijs van nederlanderschap komen?
- Kan ik een uittreksel uit het GBA krijgen?
- Hoe kom ik aan een bewijs van in leven zijn?
- Ik heb een uittreksel uit het bevolkingsadministratie nodig
- Ik heb een geboorteakte nodig
- Ik wil een echtscheidingsakte
- Kan ik een uittreksel uit de BRP krijgen?
- brp uittreksel
- Hoe kom ik aan een bewijs van uitschrijving?
- Heeft u een geboortebewijs voor me?
- Hoe kom ik aan een geboortecertificaat?
- Hoe vraag ik een uittreksel aan.
- Hoe vraag ik een echtscheidingsakte aan?
- Ik heb een trouwakte nodig
- Ik heb een uitreksel uit het geboorteregister nodig
- uittreksel
- Kan ik aan een attestaie de vita komen?

## intent:product_geboorteaangifte_info
- Kan ik bij de geboorteaangifte zelf kiezen welke achternaam mijn kind krijgt?
- Kan ik mijn levenloos geboren kind ook laten registreren?
- geboorteaangifte
- Hoe kan ik onze pasgeboren baby aangeven.
- geboorte aangeven
- Wie mogen de geboorteaangifte doen?
- Ik wil de komst van ons kind doorgeven.
- Is het ook mogelijk de geboorteaangifte digitaal te doen?
- Mijn kind is doodgeboren. Wordt er dan wel een geboorteakte opgemaakt?
- hoe kan ik mijn pasgeboren zoon melden
- Ik wil een geboorte aangeven.
- Wie kan mijn kind aangeven?
- Ik wil de geboorte melden van ons kind
- Ik wil aangifte doen van de geboorte van ons kind.
- Mijn kind is overleden. Kan ik dan ook een geboorte aangeven?
- Kan ik mijn kind erkennen bij de geboorteaangifte?
- Waar doe ik aangifte van mijn kind?

## intent:product_id-kaart_abroad
- Hoe kom ik aan een id-kaart als ik in het buitenland woon?
- Hoe vraag ik een id-kaart in het buitenland aan?
- Waar haal ik mijn id-kaart in het buitenland?
- Kan ik een id-kaart ook in het buitenland aanvragen?

## intent:product_id-kaart_applyinfo
- Hoe krijg ik een id-kaart?
- Hoe vraag ik een id-kaart aan?
- Kan ik online een id-kaart aanvragen?
- Ik wil een id-kaart aanvragen, hoe doe ik dat?

## intent:product_id-kaart_appointment
- Ik wil een id-kaart aanvragen.
- Ik wil een id-kaart aanvragen aan de balie, hoe maak ik een afspraak?
- Hoe maak ik een afspraak om id-kaart aan te vragen?
- Kan ik een afspraak maken om aan een id-kaart te komen?

## intent:product_id-kaart_childnecessities
- Wat heb ik nodig als ik voor mijn zoon een id-kaart ga aanvragen?
- Wat moet ik meenemen als ik een id-kaart aan vraag voor mijn kind?
- Wat moet ik doen als ik voor mijn dochter een id-kaart wil aanvragen?
- Wat moet ik hebben als ik een id-kaart voor mijn kind wil halen?

## intent:product_id-kaart_collect
- Kan ik een id-kaart zonder afspraak op komen halen?
- Moet ik een afspraak maken voor het afhalen van mijn id-kaart?
- Hoe kan ik een id-kaart ophalen?
- Kan ik mijn id-kaart ophalen zonder afspraak?

## intent:product_id-kaart_costs
- Wat betaalt men voor een id-kaart?
- Hoeveel kost een [idkaart](product:id-kaart)?
- Wat kost een [idkaart](product:id-kaart)?
- Wat is de prijs van een id-kaart?
- Hoeveel moet ik voor een id-kaart betalen?

## intent:product_id-kaart_costsurgent
- Wat zijn de kosten van een spoedaanvraag voor een id-kaart?
- Wat zijn de kosten als ik een id-kaart met spoed nodig heb?
- Wat kost het als ik het id-kaart sneller nodig heb?
- Wat kost een spoedaanvraag id-kaart?
- Wat is de prijs van een spoedaanvraag voor een id-kaart?

## intent:product_id-kaart_delivery
- Hoe duur is het om een id-kaart thuis te laten bezorgen?
- Versturen jullie id-kaart ook per post?
- Ik kan mijn id-kaart niet ophalen, kan die opgestuurd worden?
- Wordt een id-kaart thuisbezorgd?

## intent:product_id-kaart_eta
- Hoe lang duurt het aanvragen van een id-kaart?
- Wanneer kan ik het id-kaart ophalen?
- Is mijn id-kaart al klaar?
- Kunt u kijken of mijn id-kaart al klaar ligt?
- Kan ik mijn id-kaart al ophalen?
- Duurt het lang voordat ik een nieuw id-kaart heb?
- Hoe lang duurt het voor mijn id-kaart klaar is?
- Wanneer is het id-kaart klaar?

## intent:product_id-kaart_fingerprint
- Wat doen jullie met mijn vingerafdrukken?
- Waarvoor hebben jullie mijn vingerafdrukken nodig?
- Is het nodig dat ik een vingerafdruk laat maken voor een id-kaart?
- Is een vingerafdruk verplicht voor een id-kaart?

## intent:product_id-kaart_found
- Ik vond mijn id-kaart weer, wat nu?
- Wat moet ik doen als ik mijn id-kaart weer heb teruggevonden?
- Mijn verloren id-kaart is weer opgedoken. Wat doe ik daarmee?
- Mijn id-kaart vond ik zojuist weer terug.

## intent:product_id-kaart_lost
- Het id-kaart van mijn kind is kwijt, wat moet ik doen?
- Moet ik het doorgeven als mijn id-kaart kwijt is?
- Mijn id-kaart is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?
- Ik kan mijn id-kaart niet meer vinden, wat nu?

## intent:product_id-kaart_lostabroad
- Mijn id-kaart is gestolen in het buitenland. Wat nu?
- Waar vraag ik een tijdelijk id-kaart in het buitenland aan?
- Ik ben mijn id-kaart verloren op vakantie. Wat moet ik doen?
- Ik ben mijn id-kaart kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?

## intent:product_id-kaart_necessities
- Moet ik iets meenemen als ik een id-kaart aanvraag?
- Wat moet ik bij me hebben als ik een id-kaart aan ga vragen?
- Wat ik heb ik nodig als ik een id-kaart aan wil vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een id-kaart?

## intent:product_id-kaart_noticket
- Kan ik mijn id-kaart zonder bewijs ophalen?
- Ik ben het afhaalbewijs voor het [identiteitskaart](product:id-kaart) kwijt, wat nu?
- Kan ik het [idkaart](product:id-kaart) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het [idkaart](product:id-kaart) ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het id-kaart kwijt, wat nu?
- Kan ik het id-kaart afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik mijn [id](product:id-kaart) zonder bewijs ophalen?
- Kan ik het id-kaart ophalen zonder afhaalbewijs?

## intent:product_id-kaart_notregistered
- Ik wil me inschrijven en een id-kaart aanvragen, kan dat?
- Ik sta nergens ingeschreven, hoe kom ik aan een id-kaart?
- Hoe moet ik een Nederlands id-kaart aanvragen als ik nergens sta ingeschreven?
- Hoe vraag je een id-kaart aan als je niet ingeschreven staat?

## intent:product_id-kaart_otherperson
- Ik heb last van mijn gezondheid en kan daardoor mijn id-kaart niet ophalen.
- Ik kan niet naar de balie komen. Hoe vraag ik een id-kaart aan?
- Hoe kan ik een id-kaart aanvragen als ik niet langs kan komen?
- Kan iemand anders mijn id-kaart aanvragen of ophalen?

## intent:product_id-kaart_photo
- Waar kan ik pasfoto's laten maken voor een id-kaart?
- Wat zijn de eisen van een pasfoto voor mijn id-kaart?
- Aan welke eisen moet een pasfoto voor id-kaart voldoen?
- Wat zijn de regels voor een foto voor een id-kaart?
- Waar moet ik een pasfoto laten maken voor mijn id-kaart?

## intent:product_id-kaart_requirements
- Wie kan een id-kaart aanvragen?
- Wanneer mag je een id-kaart aanvragen?
- Vanaf welke leeftijd mag je een id-kaart aanvragen?
- Mag iedereen een id-kaart aanvragen?

## intent:product_id-kaart_urgentinfo
- Kan ik versneld een id-kaart aanvragen?
- Ik wil snel een id-kaart aanvragen.
- Kan ik een id-kaart met spoed aanvragen ?
- Hoe kan ik een id-kaart met spoed aanvragen?

## intent:product_id-kaart_validity
- Hoe lang is een id-kaart geldig?
- Tot wanneer is een id-kaart geldig
- Na hoeveel jaar is mijn id-kaart verlopen?
- Wanneer verloopt mijn id-kaart?

## intent:product_id-kaart_validityabroad
- Naar welke landen mag ik met mijn id-kaart?
- Welke landen erkennen mijn id-kaart?
- In welke landen is een id-kaart geldig?
- Waar is een id-kaart geldig?

## intent:product_id-kaart_validitycountry
- Kan ik op vakantie naar [Brazilie](noidcountries:Brazilië) met een idkaart?
- Kan ik met mijn id kaart naar [China](noidcountries)?
- Kan ik met een idkaart naar [Marokko](noidcountries)?
- Is een id-kaart geldig in [Zwitserland](idcountries)
- Geldt mijn idkaart in [Irak](noidcountries)?
- Is mijn idkaart geldig in [Iran](noidcountries)?
- Is een idkaart een geldig document in [Griekenland](idcountries)?
- Wordt mijn idkaart geaccepteerd in [Italië](idcountries)?
- Mag ik met mijn idkaart naar [Noord-Korea](noidcountries) reizen?

## intent:product_melding-openbare-ruimte_info
- Kan ik overlast door verslaafden melden?
- De straatverlichting is uitgevallen
- Het GFT staat er nog steeds
- De straatverlichting is kapot
- De straatlantaarn blijft branden
- Er staat al heel lang een fietswrak in de straat.
- kunnen jullie strooien
- De ondergrondse container zit vol.
- Er is een verkeersbord omgereden
- Er is een paaltje omver gereden
- De put in de straat stroomt over
- De ondergrondsce container is kapot
- De papiercontainer zit vol.
- De werkzaamheden zorgen voor overlast
- bij ons in de buurt worden al enige tijd meerdere ratten gesignaleerd
- De verkeerssituatie is hier echt onveilig
- Het is hier donker, er is te weinig straatverlichting
- Er ligt afval in het groenperk
- ik heb overlast van een bedrijf
- De prullenbak is kapot
- Er zwerft hier een losse winkelwagen
- Onze riolering is kapot
- Mijn grofvuil staat nog steeds op straat.
- We hebben overlast van geparkeerde fietsen
- Er ligt hier zo veel hondenpoep, komen jullie dat opruimen?
- Er liggen kerstbomen op de weg
- De prullenbak is vol
- Het regent en we hebben op straat wateroverlast
- Er is hier veel overlast van geparkeerde auto's
- We hebben veel overlast van bepaalde buren in de straat
- Het plastic is niet opgehaald
- In de straat hier zorgen jongeren voor overlast
- Op de speelplaats ligt afval
- De textielbak is vol
- Bij het park zijn vaak loslopende honden
- Het stoplicht heeft storing
- Er liggen hier veel bladeren op de weg
- De afvalbak is kapot
- Kan ik overlast door daklozen melden?
- Het bushokje is kapot
- ik heb last van een bedrijf
- In de straat is een heel gevaarlijke hond, die heeft al eens gebeten
- Er is een tak van de boom gewaaid
- Ik wil drugsoverlast melden.
- In de vijver zit blauwalg
- Kan het gras gemaaid worden?
- De openbare verlichting is defect
- Er ligt hier chemisch afval
- Er ligt veel vuil in de struiken hier
- Hier ligt een dood/dode dier langs de weg
- De plantenbak is kapot gemaakt
- De reclamezuil is kapot
- De stoeptegel ligt los
- Het fietspad is niet geveegd sneeuw
- De glasbak zit vol.
- De abri is vernield.
- De plastic container puilt uit.
- De lantaarnpaal is kapot
- Waar kan ik vuurwerkoverlast melden?
- Het speeltoestel is beschadigd
- ik heb last van bouwlawaai
- Komen jullie de eikenprocessierups weghalen?
- Het verkeerslicht is niet goed afgesteld.
- Graag wil ik ook alvast via deze weg een melding maken met betrekking tot afval
- we hebben erg veel geluidsoverlast
- Er is hier ontzettend veel onkruid in de straat. Ziet er niet uit.
- Kan er hier een fietsenrek komen?
- ik heb last van ongedierte
- De bushalte is vernield door vandalen
- De kledingbak is vol
- Kan ik stankoverlast melden?

## intent:product_paspoort_abroad
- Waar haal ik mijn paspoort in het buitenland?
- Kan ik een paspoort ook in het buitenland aanvragen?
- Ik woon in het buitenland, hoe kom ik aan mijn paspoort?
- Hoe kom ik aan een paspoort als ik in het buitenland woon?

## intent:product_paspoort_applyinfo
- Hoe vraag ik een paspoort aan?
- Ik wil een paspoort aanvragen, hoe doe ik dat?
- Kan ik online een paspoort aanvragen?
- Kan ik een paspoort aanvragen in [Utrecht](municipality)?

## intent:product_paspoort_appointment
- Kan ik een afspraak maken om aan een paspoort te komen in [Dongen](municipality)?
- Ik wil een paspoort aanvragen in [Tilburg](municipality).
- Kan ik online een afspraak maken voor het aanvragen van een paspoort?
- Hoe maak ik een afspraak om paspoort aan te vragen?

## intent:product_paspoort_childabroad
- Heb ik iets nodig om met mijn kind naar het buitenland te reizen?
- Kan ik mijn kind meenemen naar het buitenland?
- Ik wil op vakantie met mijn kind, wat moet ik regelen?
- Ik wil met mijn kind naar het buitenland.

## intent:product_paspoort_childadd
- Ik wil graag mijn kind laten bijschrijven in m'n paspoort.
- Hoe kan ik mijn kind laten bijschrijven in mijn paspoort?
- Kan ik mijn kind nog laten bijschrijven in mijn paspoort?
- Mag mijn zoon/dochter in mijn paspoort bijgeschreven worden?

## intent:product_paspoort_childnecessities
- Wat moet ik hebben als ik een paspoort voor mijn kind wil halen?
- Wat moet ik meenemen als ik een paspoort aan vraag voor mijn kind?
- Wat heb ik nodig als ik voor mijn zoon een paspoort ga aanvragen?
- Wat heb ik nodig als ik voor mijn kind een paspoort aan wil vragen?

## intent:product_paspoort_childpermission
- Ik krijg geen toestemming van mijn ex om met mijn kind op vakantie te gaan.
- Mijn ex saboteert mijn vakantie met mijn kind. Wat moet ik doen?
- Mijn ex-partner geeft geen toestemming om met mijn kind te reizen.
- Wat moet ik doen als de andere ouder geen toestemming wil geven om te reizen?

## intent:product_paspoort_collect
- Kan ik een paspoort zonder afspraak op komen halen?
- Waar kan ik mijn paspoort afhalen?
- Kan ik mijn paspoort ophalen zonder afspraak in [Ede](municipality)?
- Moet ik een afspraak maken voor het afhalen van mijn paspoort in [Dongen](municipality)?

## intent:product_paspoort_costs
- Hoeveel kost een paspoort?
- Wat is de prijs van een paspoort in [dongen](municipality:Dongen)
- Hoeveel moet ik voor een paspoort betalen?
- Hoe duur is een paspoort?
- Hoeveel kost een paspoort in [Utrecht](municipality)?
- Wat betaal ik voor een paspoort?
- Wat is de prijs van een paspoort?
- Wat kost een paspoort in [Tilburg](municipality)?
- Hoe duur is een paspoort in [Dongen](municipality)?
- Wat betaalt men voor een paspoort in [Tilburg](municipality)?
- Wat kost een paspoort in [Loon op Zand](municipality)?
- Wat betaal ik voor een paspoort in [Tilburg](municipality)?

## intent:product_paspoort_costsurgent
- Wat betaal ik als ik het paspoort met spoed aanvraag in [Ede](municipality)?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort in [Tilburg](municipality)?
- Wat kost een spoedaanvraag paspoort in [Utrecht](municipality)?
- Wat betaal ik als ik het paspoort met spoed aanvraag?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb in [Eindhoven](municipality)?
- Wat kost het als ik het paspoort sneller nodig heb in [Dongen](municipality)?
- Hoeveel meer kost een spoedaanvraag paspoort?
- Wat kost het als ik het paspoort sneller nodig heb?
- Wat kost een spoedaanvraag paspoort?
- Wat zijn de kosten van een spoedaanvraag voor een paspoort?
- Wat zijn de kosten als ik een paspoort met spoed nodig heb?

## intent:product_paspoort_delivery
- Ik kan mijn paspoort niet ophalen, kan die opgestuurd worden?
- Versturen jullie paspoort ook per post in [Tilburg](municipality)?
- Kan het paspoort opgestuurd worden?
- Hoe duur is het om een paspoort thuis te laten bezorgen in [Dongen](municipality)?

## intent:product_paspoort_emergency
- Waar vraag ik een noodpaspoort aan?
- Wat doe ik als mijn paspoort kwijt is en ik moet vandaag nog weg?
- Ik wil een noodpaspoort aanvragen.
- Ik heb een noodpaspoort nodig, hoe regel ik dat?

## intent:product_paspoort_eta
- Hoe lang duurt het voor mijn paspoort klaar is?
- Kunt u kijken of mijn paspoort al klaar ligt?
- Kan ik mijn paspoort al ophalen?
- Is mijn paspoort al klaar?

## intent:product_paspoort_etanotapplied
- Hoe lang duurt het voordat ik een nieuw paspoort heb?
- Hoe snel kan ik een nieuw paspoort hebben?
- Hoe lang duurt het aanvragen van een paspoort?
- Duurt het lang voordat ik een nieuw paspoort heb?

## intent:product_paspoort_fingerprint
- Is het nodig dat ik een vingerafdruk laat maken voor een paspoort?
- Moet ik een vingerafdruk zetten voor een paspoort?
- Is een vingerafdruk verplicht voor een paspoort?
- Waarom zijn mijn vingerafdrukken nodig?

## intent:product_paspoort_found
- Wat moet ik doen als ik mijn paspoort weer heb teruggevonden?
- Ik heb mijn paspoort teruggevonden, mag ik die weer gebruiken?
- Mijn verloren paspoort is weer opgedoken. Wat doe ik daarmee?
- Mijn paspoort vond ik zojuist weer terug.

## intent:product_paspoort_lost
- Ik ben mijn paspoort kwijt?
- Moet ik het doorgeven als mijn paspoort kwijt is?
- Het paspoort van mijn kind is kwijt, wat moet ik doen?
- Wat moet ik doen als mijn paspoort kwijt, vermist of gestolen is?

## intent:product_paspoort_lostabroad
- Waar vraag ik een tijdelijk paspoort in het buitenland aan?
- Mijn paspoort is gestolen in het buitenland. Wat nu?
- Ik heb mijn paspoort in het buitenland verloren. Wat moet ik doen?
- Ik ben mijn paspoort kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?

## intent:product_paspoort_lostreport
- Moet ik geen aangifte bij de politie doen?
- Moet ik ook naar de politie als ik mijn paspoort ben verloren?
- Is het nodig om bij de politie aangifte te doen?
- Moet ik het ook aan de politie doorgeven?

## intent:product_paspoort_necessities
- Wat zijn de benodigdheden voor als ik een paspoort aan wil vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een paspoort?
- Wat moet ik meenemen bij de aanvraag van een paspoort?
- Moet ik iets meenemen als ik een paspoort aanvraag?

## intent:product_paspoort_noticket
- Kan ik mijn paspoort zonder bewijs ophalen?
- Ik ben het afhaalbewijs voor het paspoort kwijt, wat nu?
- Kan ik het paspoort ophalen zonder afhaalbewijs?
- Is het afhalen van mijn paspoort mogelijk zonder afhaalbewijs?

## intent:product_paspoort_notregistered
- Hoe moet ik een Nederlands paspoort aanvragen als ik nergens sta ingeschreven?
- Ik sta nergens ingeschreven, hoe kom ik aan een paspoort?
- Hoe vraag je een paspoort aan als je niet ingeschreven staat?
- Ik wil een paspoort aanvragen, maar ik sta niet ingeschreven. Wat nu?

## intent:product_paspoort_otherperson
- Ik kan niet naar de balie komen. Hoe vraag ik een paspoort aan?
- Ik heb last van mijn gezondheid en kan daardoor mijn paspoort niet ophalen.
- Ik ben slecht ter been en kan mijn paspoort daardoor niet ophalen.
- Hoe kan ik een paspoort aanvragen als ik niet langs kan komen?

## intent:product_paspoort_paymentmethod
- Kan ik met cash betalen?
- Kan ik pinnen?
- Welke betaalmethode bieden jullie aan?
- Is het mogelijk om met creditcard te betalen?
- Kan ik met pinpas betalen?
- Kan ik met briefgeld betalen?
- Hoe kan ik betalen?

## intent:product_paspoort_photo
- Waar moet ik een pasfoto laten maken voor mijn paspoort?
- Wat zijn de eisen van een pasfoto voor mijn paspoort?
- Wat voor pasfoto moet ik meenemen voor paspoort?
- Wat zijn de regels voor een foto voor een paspoort?
- Waar kan ik pasfoto's laten maken voor een paspoort?

## intent:product_paspoort_requirements
- Wanneer mag je een paspoort aanvragen?
- Vanaf welke leeftijd mag je een paspoort aanvragen?
- Wie kan een paspoort aanvragen?
- Mag iedereen een paspoort aanvragen?

## intent:product_paspoort_urgentinfo
- Ik wil snel een paspoort aanvragen.
- Kan ik een paspoort met spoed aanvragen?
- Hoe kan ik een paspoort met spoed aanvragen?
- Kan ik versneld een paspoort aanvragen?

## intent:product_paspoort_validity
- Wanneer verloopt mijn paspoort?
- Na hoeveel jaar is mijn paspoort verlopen?
- Tot wanneer is een paspoort geldig
- Hoeveel jaar is mijn paspoort geldig?

## intent:product_paspoort_validityabroad
- Waar ter wereld is mijn paspoort geldig?
- Waar is een paspoort geldig?
- Welke landen erkennen mijn paspoort?
- In welke landen is een paspoort geldig?

## intent:product_rijbewijs_info
- Wanneer kan ik mijn rijbewijs verlengen?
- Ik wil mijn vrachtwagenrijbewijs verlengen
- Kan ik een afspraak maken voor een nieuw rijbewijs?
- Hoe kom ik aan een nieuw rijbewijs?
- Hoeveel dagen duurt de spoedaanvraag voor een rijbewijs
- Geldt een autorijbewijs ook voor een scooter?
- Hoe lang heb ik een puntenrijbewijs?
- In welke landen is het Europees rijbewijs geldig?
- Hoeveel kost een rijbewijs?
- Heb je voor een speed-pedelec een brommerrijbewijs nodig?
- Ik moet mijn motorrijbewijs verlengen
- Wat zijn de rijbewijsregels
- Hoe kan ik een rijbewijs aanvragen
- Ik wil een afspraak om mijn rijbewijs aan te vragen
- Welke rijbewijscategorieën zijn er?
- Mag ik al scooterrijbewijs halen?
- Hoe lang is een beginnersrijbewijs geldig?
- Vanaf welke leeftijd kan ik het jongerenrijbewijs halen?
- Hoe lang is een rijbewijs geldig?
- Ik moet mijn groot rijbewijs verlengen

## intent:product_trouwen_info
- ik ga trouwen
- Wij gaan trouwen, kan ik een datum vastleggen?
- Mogen we ons trouwfeest in de tuin doen?
- Mogen we zelf een trouwlocatie kiezen?
- Hoe kunnen wij een trouwdatum vastleggen?
- trouwen
- Hoe kunnen we een voorgenomen huwelijk melden?
- Hoeveel getuigen mogen we hebben bij het trouwen?

## intent:product_uittreksel-verhuismelding_proofregistration
- Hoe kan ik bewijzen dat ik ergens ingeschreven ben?
- Hoe kan ik aantonen dat ik ergens ingeschreven sta?
- Wat is een bewijs van inschrijving?
- Kan ik een bewijs van inschrijving krijgen?

## intent:product_verhuismelding_adreswijziging
- Hoe moet ik een verhuizing doorgeven?
- Hoe geef ik mijn [nieuwe adres](product:verhuismelding) door?
- Verhuizing doorgeven
- Ik heb een nieuw adres
- Ik moet een nieuw adres  doorgeven
- Hoe kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Adreswijziging
- Hoe verander ik mijn adres?
- Waar kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Ik wil mijn nieuwe adres doorgeven
- Yo ik heb een nieuw adres
- Hoe geef ik mijn [verhuizing](product:verhuismelding) door?
- Hoe geef ik mijn adreswijziging door?
- Ik wil mijn adres wijzigen

## intent:product_verhuismelding_applyabroad
- Ik wil naar Nederland verhuizen. Hoe moet ik dit doorgeven?
- Ik verhuis naar Nederland. Moet ik mij inschrijven?
- Ik woon in het buitenland. Hoe geef ik mijn verhuizing dan door?
- Ik heb al eerder in Nederland gewoond. Moet ik mij nu opnieuw inschrijven?

## intent:product_verhuismelding_applydigid
- Dat heb ik niet. (vervolgvraag)
- DigiD, dat heb ik niet.
- Heb geen DigiD.
- Heb ik niet. (vervolgvraag)

## intent:product_verhuismelding_bevestiging
- Krijg ik een bevestiging als ik mijn adres heb gewijzigd
- Hoe weet ik zeker dat het doorgeven van mijn nieuwe adres gelukt is?
- Is mijn verhuizing al doorgevoerd/afgewerkt/behandeld?
- Ik heb geen bevestiging van mijn verhuizing gekregen.
- Is mijn verhuizing al verwerkt?
- Ben ik al uitgeschreven?
- Hoe weet ik of het doorgeven van mijn verhuizing goed is gegaan?
- Hoe kom ik aan een bewijs dat ik verhuisd ben?

## intent:product_verhuismelding_digidinfo
- Ik heb geen idee wat DigiD is.
- Wat is dat? (vervolgvraag)
- Wat is DigiD?
- DigiD? Nog nooit van gehoord.

## intent:product_verhuismelding_dontwantdigid
- Ik vertrouw DigiD niet., Ik wil geen DigiD gebruiken.
- Ik ben tegen digid.
- Ik wil geen gebruik maken van DigiD.
- Ik vind DigiD stom.

## intent:product_verhuismelding_emigrate
- Ik ga reizen, moet ik me uitschrijven?
- Moet ik me uitschrijven als ik ga emigreren?
- Ik verhuis naar het buitenland, moet ik me uitschrijven?
- Ik ga naar het buitenland, is het nodig dat ik me uitschrijf?

## intent:product_verhuismelding_formerror
- Mijn nieuwe adres zit er niet in.
- Mijn adres is geheim, hoe geef ik dan mijn verhuizing door?
- Ik probeerde mijn verhuizing door te geven maar mijn adres wordt niet herkend. Wat nu?
- Het lukt bij verhuizen niet om mijn adres in te voeren. Hoe geef ik het nu door?
- Ik begrijp de vragen niet, kan ik mijn verhuizing ook op een andere manier doorgeven
- Ik verhuis naar een verzorgingshuis maar het lukt niet online. Hoe doe ik dat nu?
- De website loopt vast, hoe geef ik nu mijn verhuizing door?
- Het formulier werkt niet, wat nu?

## intent:product_verhuismelding_kindinschrijven
- Kan ik mijn meerderjarige kind meeverhuizen?
- Kan ik mijn kind bij mij inschrijven?
- Ik wil mijn kind inschrijven.
- Ik wil mijn kind inschrijven op een ander adres.
- Mijn kind(eren) verhuist/verhuizen mee, kan ik dat gelijk doorgeven?
- Ik wil het adres van mijn kind wijzigen.
- Kan ik mijn kind mee verhuizen?
- Kan ik mijn kind meeverhuizen

## intent:product_verhuismelding_kindinschrijvenanderadres
- Kan ik de adreswijziging doorgeven voor mijn kind dat op kamers gaat?
- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven?
- Kan ik het nieuwe adres doorgeven van mijn studerende kind?
- Mijn dochter gaat in de stad studeren, hoe kan ik het nieuwe adres doorgeven?

## intent:product_verhuismelding_mandatory
- Is het verplicht mijn adreswijziging door te geven?
- Is inschrijven op je nieuwe adres verplicht?
- Moet ik doorgeven dat ik verhuisd ben?
- Ben ik verplicht om mijn verhuizing te melden?

## intent:product_verhuismelding_when
- Ik ben te laat met het doorgeven van mijn verhuizing. Kan ik dat nog doorgeven?
- Als ik volgende week ga [verhuizen](product:verhuismelding), kan ik dat nu al zeggen?
- Ik ga [verhuizen](product:verhuismelding) naar Utrecht. Wanneer moet ik dit doorgeven?
- Wanneer geef ik mijn [adreswijziging](product:verhuismelding) door
- Wanneer moet ik mijn verhuizing doorgeven
- Wanneer willen jullie [mijn nieuwe adres](product:verhuismelding) weten
- Op welk moment moet ik mijn [verhuizing](product:verhuismelding) doorgeven
- Binnen welke termijn moet ik mij inschrijven op mijn nieuwe adres?
- Volgende maand [verhuis](product:verhuismelding) ik. Mag ik dat nu al doorgeven?
- Als ik volgende maand [verhuis](product:verhuismelding), kan ik dat dan nu al doorgeven?
- Kan ik mijn [verhuizing](product:verhuismelding) al doorgeven?
- ik ben vergeten mijn [adreswijziging](product:verhuismelding) door te geven. Kan dit nog?
- Wanneer kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven
- Ik weet nu al wanneer ik ga [verhuizen](product:verhuismelding). Kan ik dat nu al doorgeven?
- kan ik ons als nieuwe bewoners inschrijven?
- kan ik mijn verhuizing doorgeven?
- ik ga in oktober [verhuizen](product:verhuismelding), kan ik dat nu al doorgeven?
- Ik ben vergeten mijn verhuizing door te geven.
- Ik ben [verhuisd](product:verhuismelding). Tot wanneer kan ik dit aangeven?
- Wanneer kan ik mijn verhuismelding doorgeven
- Mijn verhuizing heb ik te laat doorgegeven. Kan ik dat nog doen?
- Ik ben waarschijnlijk te laat met het doorgeven van mijn adreswijziging.

## intent:product_verhuismelding_when_info
- Kan ik mijn verhuizing al eerder doorgeven?
- Is inschrijven al voor de verhuizing mogelijk?
- Kan ik mij al eerder inschrijven in de gemeente?
- Hoe ver van tevoren kan ik [verhuizing](product:verhuismelding) doorgeven?
- Kan ik mijn adreswijziging al eerder doorgeven?
- Hoe lang van tevoren moet ik mijn verhuizing doorgeven?

## intent:restart
- Hoe start ik opnieuw?
- opnieuw invoeren
- Opnieuw
- restart
- Ik wil opnieuw beginnen
- Reboot

## intent:thanks
- bedankt voor de hulp
- dank u
- bedankt he
- thanks
- dank u vriendelijk
- Super!
- dankje
- fijn, dankje
- enorm bedankt!

## intent:unfriendly
- ROT OP GEM
- stomme chatbot
- stomme gem
- klote chatbot
- ROT OP
- klote bot
- Rot op
- stomme bot
- rot op

## intent:unfriendly_questions
- Doe niet zo dom.
- ben je dom ofzo
- waarom snap je niets
- je snapt ook niets

## synonym:Almere
- almere

## synonym:Amsterdam
- amsterdam

## synonym:Brazilië
- Brazilie

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## synonym:Utrecht
- utrecht

## synonym:id-kaart
- idkaart
- identiteitskaart
- id

## synonym:verhuismelding
- verhuizing
- adreswijziging
- mijn nieuwe adres
- verhuizen
- verhuisd
- verhuis
- verhiuzin
- nieuwe adres
- gaan wonen

## lookup:municipality
  data/municipalities.txt

## lookup:idcountries
  data/idcountries.txt

## lookup:noidcountries
  data/noidcountries.txt

## lookup:product
- paspoort
- parkeervergunning
- id-kaart
- overlijdensakte
- verhuismelding
- bijstand
- uitkering
- hondenbelasting
- afvalcontainer
- belasting-kwijtschelding
- brp-uittreksel
- trouwen
- afspraak
- afvalkalender
- rijbewijs
- geboorteaangifte
- melding-openbare-ruimte
- reisdoc-costs-urgent
- reisdoc-costs-paymentmethod
