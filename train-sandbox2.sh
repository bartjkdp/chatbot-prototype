# Forced retraining of Rasa NLU + Core
docker run \
  -v $(pwd)/training-sandbox2:/app \
  -v $(pwd)/models:/app/models \
  -v $(pwd)/rasa/custom:/usr/local/lib/python3.6/site-packages/custom \
  rasa/rasa:1.1.6 \
  train \
    -vv \
    --data data \
    --debug-plots \
    --force \
