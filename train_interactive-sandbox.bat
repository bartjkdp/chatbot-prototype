rem Start Rasa in Interactive training mode
echo To start interactive training make sure you "run_action_server.bat"
setlocal
FOR /F "tokens=*" %%i in ('type .env') do SET %%i
docker run -it -p 5006:5006 -v %cd%/training-sandbox:/app -v %cd%/models:/app/models -v %cd%/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:%RASA_VERSION% interactive --debug --endpoints endpoints-interactive.yml --model models --data data/stories
endlocal
