@echo off 
rem clear model directory
IF "_%1" == "_clean" (
  echo "Cleaning Model directory"
  del /Q models\*.gz
) ELSE (
  echo "To clean the model directory: %0 clean"
)

echo "Forced retraining of Rasa Core and NLU"
setlocal
FOR /F "tokens=*" %%i in ('type .env') do SET %%i
docker run -v %cd%/training:/app -v %cd%/models:/app/models -v %cd%/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:%RASA_VERSION% train  -vv --debug-plots --force --data data %1
endlocal

echo "Training done"
