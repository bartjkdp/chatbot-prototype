# Start Action server for use in Interactive training mode
#docker run -p 5055:5055 -v $(pwd)/tilburg-action-server/actions:/app/actions -e CONTENT_API=http://webservices.tilburg.io:8099/ registry.gitlab.com/gemeente-tilburg/chatbot-prototype/tilburg-action-server:latest
docker run -it -p 5055:5055 -v $(pwd)/tilburg-action-server/actions:/app/actions -e CONTENT_API=http://webservices.tilburg.io:8099/ -e BRP_API=http://webservices.tilburg.io:8089/ registry.gitlab.com/gemeente-tilburg/chatbot-prototype/tilburg-action-server:latest start -vv --actions actions.actions
