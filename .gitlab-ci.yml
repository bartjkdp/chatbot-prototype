image: debian:stable-20191014

variables:
# Required to access the Gitlab CI docker service
  DOCKER_HOST: tcp://docker:2375
# Required to ignore TLS
  DOCKER_TLS_CERTDIR: ""
  GEM_CONTENT_REGISTRY_IMAGE: registry.gitlab.com/gemeente-tilburg/chatbot-prototype/gem-content
  ACTION_SERVER_REGISTRY_IMAGE: registry.gitlab.com/gemeente-tilburg/chatbot-prototype/tilburg-action-server
  GEM_CONTENT_TAG_NAME: $GEM_CONTENT_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA
  ACTION_SERVER_TAG_NAME: $ACTION_SERVER_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA

services:
 - docker:19.03.1-dind

before_script:
 - apt-get update -qy
# gnupg2 and pass are required to allow docker login to work for pushing the images to the registry
# wget is required for checking the action server docker container
 - apt-get install -y docker.io gnupg2 pass wget
 - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
# docker-compose is required for the build and rasa stages
# However only do this AFTER the docker login due to https://github.com/docker/compose/issues/6023
 - apt-get install -y docker-compose 

stages:
 - test
 - build
 - train
 - rasa

train:
  stage: train
  script:
  - echo "Setting up & training Rasa model (train_sandbox)"
  - cp .gitlab-env .env
  - ./train_ci.sh
  artifacts:
    paths:
    - models/gitlab-model.tar.gz

# Using the built model from the train stage, perform Newman callscripts
# against the trained Rasa instance
rasa:
  stage: rasa
  script:
  - echo "Setting up & starting Rasa"
  - cp .gitlab-env .env
  - docker network create my-rasa-network
  - cd models
  - ls -l
  - cd ..
  - docker-compose up -d
  - sleep 10s
  - docker-compose logs
  - echo "Performing Newman callscripts tests against Rasa"
  - cd test/newman
  - sh ./setup.sh
  - sh ./run_callscripts_test_ci.sh
  - echo "Newman tests executed succesfully"
  artifacts:
    paths:
    - test/newman/reports/newman_report.html
 
test-gem-content:
  stage: test
  script:
  - echo "Peforming Gem-Content API & import tests"
  - apt-get update -qy
  - apt-get install -y python-dev python-pip git python-dev python-setuptools python3-dev python-virtualenv libpq-dev
  - cd gem-content
  - python bootstrap.py jenkins
  - . env/bin/activate
  - pip install -r requirements/test.txt
  - python src/manage.py test gem_content
  - echo "Gem-Content tests succeeded"

build-gem-content:
  stage: build
  script:
  - echo "Attempting to build Gem-content Docker image"
  - apt-get update -qy
  - apt-get install -y docker-compose docker
  - cd gem-content
  - docker build -t $GEM_CONTENT_TAG_NAME -t $GEM_CONTENT_REGISTRY_IMAGE:latest .
  - echo "Gem-content docker image succesfully built"
  - docker push $GEM_CONTENT_TAG_NAME
  - docker push $GEM_CONTENT_REGISTRY_IMAGE:latest
  - echo "Gem-content image pushed to container registry"
  only:
  - master

build-action-server:
  stage: build
  script:
  - echo "Attempting to build action server Docker image"
  - apt-get update -qy
  - apt-get install -y docker-compose docker
  - cd tilburg-action-server
  - docker build -t $ACTION_SERVER_TAG_NAME  -t $ACTION_SERVER_REGISTRY_IMAGE:latest .
  - echo "Action server docker image succesfully built"
  - echo "Running Action server and logging output"
  - docker run --name action-server -d -p 5055:5055 -v `pwd`/actions:/app/actions $ACTION_SERVER_TAG_NAME
  - sleep 10s
  - docker logs action-server
  - echo "Check if we get the expected 404 when performing a GET request."
# TODO: The action server only listens to POST so that might be a better check in the future to see if the AS is OK
#  - wget localhost:5055
#  - wget localhost:5055 2>&1 | grep "ERROR 404"
  - echo "Action server was succesfully started"
  - docker push $ACTION_SERVER_TAG_NAME
  - docker push $ACTION_SERVER_REGISTRY_IMAGE:latest
  - echo "Action server image pushed to container registry"
  only:
  - master

