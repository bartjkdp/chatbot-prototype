# Builds Rasa SDK with requests library using the Dockerfile
#docker build . --file docker/action-server/Dockerfile --tag tilburg-action-server:latest
docker build tilburg-action-server --no-cache --tag registry.gitlab.com/gemeente-tilburg/chatbot-prototype/tilburg-action-server:latest
#$(date '+%Y-%m-%d')
