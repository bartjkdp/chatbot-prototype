Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows::

    WSGIDaemonProcess gem_content-<target> threads=5 maximum-requests=1000 user=<user> group=staff
    WSGIRestrictStdout Off

    <VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/gem_content/log/apache2/error.log"
        CustomLog "/srv/sites/gem_content/log/apache2/access.log" common

        WSGIProcessGroup gem_content-<target>

        Alias /media "/srv/sites/gem_content/media/"
        Alias /static "/srv/sites/gem_content/static/"

        WSGIScriptAlias / "/srv/sites/gem_content/src/gem_content/wsgi/wsgi_<target>.py"
    </VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

.. code::

    [program:uwsgi-gem_content-<target>]
    user = <user>
    command = /srv/sites/gem_content/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/gem_content/src/gem_content/wsgi/wsgi_<target>.py
    home = /srv/sites/gem_content/env
    master = true
    processes = 8
    harakiri = 600
    autostart = true
    autorestart = true
    stderr_logfile = /srv/sites/gem_content/log/uwsgi_err.log
    stdout_logfile = /srv/sites/gem_content/log/uwsgi_out.log
    stopsignal = QUIT

Nginx
-----

.. code::

    upstream django_gem_content_<target> {
      ip_hash;
      server 127.0.0.1:8001;
    }

    server {
      listen :80;
      server_name  my.domain.name;

      access_log /srv/sites/gem_content/log/nginx-access.log;
      error_log /srv/sites/gem_content/log/nginx-error.log;

      location /500.html {
        root /srv/sites/gem_content/src/gem_content/templates/;
      }
      error_page 500 502 503 504 /500.html;

      location /static/ {
        alias /srv/sites/gem_content/static/;
        expires 30d;
      }

      location /media/ {
        alias /srv/sites/gem_content/media/;
        expires 30d;
      }

      location / {
        uwsgi_pass django_gem_content_<target>;
      }
    }
