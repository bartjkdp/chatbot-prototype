#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRESQL_USERNAME" --dbname "$POSTGRESQL_DATABASE" <<-EOSQL
         CREATE DATABASE gem_content;
         GRANT ALL PRIVILEGES ON DATABASE gem_content TO $POSTGRESQL_USERNAME;
EOSQL

