import json
import logging

from django.contrib import admin
from import_export.admin import ImportExportMixin
from .models import Event

logger = logging.getLogger(__name__)

class MultiDBModelAdmin(admin.ModelAdmin):
    # A handy constant for the name of the alternate database.
    using = 'tracker'

    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        obj.delete(using=self.using)

    def get_queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super().formfield_for_foreignkey(db_field, request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super().formfield_for_manytomany(db_field, request, using=self.using, **kwargs)


class EventAdmin(ImportExportMixin, MultiDBModelAdmin):
    list_display = ('sender_id', 'get_data_text', 'type_name', 'timestamp', 'intent_name', 'action_name', 'get_data_confidence')
    list_filter = ['type_name', 'intent_name']
    search_fields = ('sender_id', 'data')

    def get_data_text(self, obj):
        return obj.get_text('text')
    get_data_text.short_description = 'Text'

    def get_data_confidence(self, obj):
        return obj.get_text('confidence')
    get_data_confidence.short_description = 'Confidence'
    
admin.site.register(Event, EventAdmin)
