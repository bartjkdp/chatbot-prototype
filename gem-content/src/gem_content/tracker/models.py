import json
import logging

from django.db import models

logger = logging.getLogger(__name__)

class Event(models.Model):
    id = models.IntegerField(primary_key=True)
    sender_id = models.CharField(max_length=255)
    type_name = models.CharField(max_length=255)
    timestamp = models.CharField(max_length=255)
    intent_name = models.CharField(max_length=255)
    action_name = models.CharField(max_length=255)
    data = models.TextField()

    class Meta:
        managed = False
        db_table = 'events'

    def get_text(self, field):
        try:
            d = json.loads(self.data)
            if field in d:
                return d[field]
        except Exception as e:
            logger.exception(e)
        return ''
