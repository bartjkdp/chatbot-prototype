import os

from ..models import UtterResponse

from django.core.management import call_command
from django.test import TestCase

class UtterResponseTest(TestCase):
    file_prefix = os.path.join(os.path.dirname(__file__), 'files')
    domain_file = 'domain.yml'

    def setUp(self):
        call_command('import_yaml', os.path.join(self.file_prefix, self.domain_file))
    
    def test_response_data(self):
        utter = UtterResponse.objects.get(utter='utter_default')
        self.assertEqual(utter.data()['text'], 'Sorry, dat snapte ik niet.')
        utter = UtterResponse.objects.get(utter='utter_default')
        self.assertEqual(utter.data(slots={'name': 'Foo'})['text'], 'Sorry, dat snapte ik niet.')

    def test_all(self):
        for utter in UtterResponse.objects.all():
            print(utter.data()['text'])

