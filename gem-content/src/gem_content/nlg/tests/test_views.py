import os

from ..models import UtterResponse, Gemeente, Product
from rest_framework.test import APITestCase

from django.core.management import call_command
from django.test import TestCase

class UtterResponseViewTest(APITestCase):

    def setUp(self):
        UtterResponse.objects.create(utter='utter_default', response='Hoi!')
        self.dongen = Gemeente.objects.create(name='Dongen')
        self.tilburg = Gemeente.objects.create(name='Tilburg')
        self.paspoort = Product.objects.create(name='paspoort')
        self.id_kaart = Product.objects.create(name='id kaart')
        
        self.post_data = {'template': 'utter_default',
                          'channel': {'name': 'collector'},
                          'arguments': {},
                          'tracker': {'latest_message': {'intent_ranking': {"confidence":1.0,"name":"ask_weather"}}},
                          }
        
    def test_api_view_get(self):
        response = self.client.get('/nlg/')
        self.assertEqual(response.status_code, 200)

    def test_api_view_post(self):
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Hoi!')

    def test_api_view_post_gemeente(self):
        UtterResponse.objects.create(utter='utter_bye', response='Tot ziens!')
        UtterResponse.objects.create(utter='utter_bye', response='Doei!',
                                     gemeente=self.dongen)
        UtterResponse.objects.create(utter='utter_bye', response='Houdoe!',
                                     gemeente=self.tilburg)

        # # NL-wide case
        self.post_data['template'] = 'utter_bye'
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Tot ziens!')

        # # NL-wide, empty slots case
        self.post_data['tracker']['slots'] = {}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Tot ziens!')

        # # NL-wide, municipality slot empty case
        self.post_data['tracker']['slots'] = {'municipality': ''}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Tot ziens!')

        # Dongen-specific case
        self.post_data['tracker']['slots'] = {'municipality': 'Dongen'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Doei!')

        # Tilburg-specific case
        self.post_data['tracker']['slots'] = {'municipality': 'Tilburg'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Houdoe!')

    def test_api_view_post_product(self):
        UtterResponse.objects.create(utter='utter_info', response='Voor welk product?')
        UtterResponse.objects.create(utter='utter_info', response='Die is mooi om te hebben',
                                     product=self.paspoort)
        UtterResponse.objects.create(utter='utter_info', response='Alleen leuk voor in de EU',
                                     product=self.id_kaart)

        # Generic case
        self.post_data['template'] = 'utter_info'
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Voor welk product?')

        # Paspoort-specific case
        self.post_data['tracker']['slots'] = {'product': 'paspoort'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Die is mooi om te hebben')

        # ID-kaart-specific case
        self.post_data['tracker']['slots'] = {'product': 'id kaart'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Alleen leuk voor in de EU')

    def test_api_view_post_gemeente_product(self):
        UtterResponse.objects.create(utter='utter_paspoort', response='Die ken ik niet')
        UtterResponse.objects.create(utter='utter_paspoort', response='Welk product wil je?',
                                     gemeente=self.dongen)
        UtterResponse.objects.create(utter='utter_paspoort', response='Alsjeblieft!',
                                     product=self.paspoort, gemeente=self.dongen)

        # # NL-wide case
        self.post_data['template'] = 'utter_paspoort'
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Die ken ik niet')

        # Dongen-specific case
        self.post_data['tracker']['slots'] = {'municipality': 'Dongen'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Welk product wil je?')

        # Tilburg-specific case, no product slot
        self.post_data['tracker']['slots'] = {'municipality': 'Tilburg'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Die ken ik niet')

        # Tilburg-specific case, with product slot
        self.post_data['tracker']['slots'] = {'municipality': 'Tilburg', 'product': 'paspoort'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Die ken ik niet')

        # Dongen-specific case, with product slot
        self.post_data['tracker']['slots'] = {'municipality': 'Dongen', 'product': 'paspoort'}
        response = self.client.post('/nlg/', self.post_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text'], 'Alsjeblieft!')
