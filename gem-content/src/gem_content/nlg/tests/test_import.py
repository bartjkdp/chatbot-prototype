import os

from ..models import UtterResponse

from django.core.management import call_command
from django.test import TestCase

class ImportTest(TestCase):
    file_prefix = os.path.join(os.path.dirname(__file__), 'files')
    domain_file = 'domain.yml'

    def test_import_yaml(self):
        """
        Test if an import of a fixed domain.yml provides the correct number of UtterResponses.
        """
        call_command('import_yaml', os.path.join(self.file_prefix, self.domain_file))
        self.assertEqual(UtterResponse.objects.all().count(), 75)
        

