import requests
import time

from django.contrib import admin
from django.urls import path
from django.template.response import TemplateResponse
from django.utils.safestring import mark_safe
from import_export.admin import ImportExportMixin
from ordered_model.admin import OrderedModelAdmin

from .models import Channel, Gemeente, IntentLabel, Product, Question, UtterLog, UtterResponse, UtterResponseButton

admin.site.register(Channel)
admin.site.register(Gemeente)

class QuestionAdmin(ImportExportMixin, OrderedModelAdmin):
    list_display = ('question', 'vraag_chatbot', 'order', 'move_up_down_links')

    def vraag_chatbot(self, obj):
        return mark_safe("<a href='run_questions'>stel vragen aan chatbot</a>")

    
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('run_questions/', self.run_questions),
        ]
        return my_urls + urls

    def run_questions(self, request):
        questions = Question.objects.all()
        sender = "default-" + str(int(time.time()))
        output = []
        if request.method == 'POST':
            url = request.POST['url'] + "/webhooks/rest/webhook"
            for question in questions:
                data = {"message": question.question,
                        "sender": sender}
                print(data)
                response = requests.post(url, json=data)
                print(response.content)
                try:
                    answer = response.json()[0]['text']
                    verified = 'default'
                    if question.expected_answer:
                        if question.expected_answer == answer:
                            verified = 'yes'
                        else:
                            verified = 'no'
                    output += [{'question': question, 'answer': answer,
                                'verified': verified,
                                'buttons': response.json()[0].get('buttons')}]
                except Exception:
                    #print(e)
                    output += [{'question': question, 'answer': 'Ongeldig antwoord'}]
                
        context = dict(
            self.admin_site.each_context(request),
            questions=questions,
            output=output,
            sender=sender,
        )
        return TemplateResponse(request, "run_questions.html", context)

admin.site.register(Question, QuestionAdmin)

class IntentLabelAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('intent', 'label', 'gemeente', 'product')
    list_filter = ('gemeente', 'product')
    search_fields = ('intent', 'label')
admin.site.register(IntentLabel, IntentLabelAdmin)
    
class ProductAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('name', 'gemeente',)
    list_filter = ('gemeente', 'channels')
    search_fields = ('name',)
    filter_horizontal = ('channels',)
admin.site.register(Product, ProductAdmin)

class UtterLogAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('created_on', 'sender_id', 'template')
    search_fields = ('sender_id', 'template', 'content', 'response')

admin.site.register(UtterLog, UtterLogAdmin)

class ButtonInline(admin.StackedInline):
    model = UtterResponseButton
    extra = 0

class UtterResponseAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ('utter', 'response', 'gemeente', 'channel', 'product', 'created_on')
    list_filter = ('gemeente', 'channel')
    search_fields = ('utter', 'response')
    inlines = [ButtonInline]

admin.site.register(UtterResponse, UtterResponseAdmin)
