import logging

from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import UtterRequestSerializer
from .models import UtterLog, UtterResponse

logger = logging.getLogger(__name__)

class APIResponse(object):
    def __init__(self, text=None):
        self.text = text

    def data(self, *args, **kwargs):
        return {"text": self.text,
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": []}


class UtterResponseView(APIView):
    """
    Voorbeeld POST vanuit RASA:

{"tracker":{"latest_message":{"text":"/greet_first","intent_ranking":[{"confidence":1.0,"name":"greet"}],"intent":{"confidence":1.0,"name":"greet"},"entities":[]},"sender_id":"22ae96a6-85cd-11e8-b1c3-f40f241f6547","paused":false,"latest_event_time":1531397673.293572,"slots":{"name":null},"events":[{"timestamp":1531397673.291998,"event":"action","name":"action_listen"},{"timestamp":1531397673.293572,"parse_data":{"text":"/greet_first","intent_ranking":[{"confidence":1.0,"name":"greet"}],"intent":{"confidence":1.0,"name":"greet"},"entities":[]},"event":"user","text":"/greet_first"}]},"arguments":{},"template":"utter_greet","channel":{"name":"collector"}}
    """

    def get(self, request, format=None):
        return Response("Hoi")

    def post(self, request, format=None):
        slots = {}
        arguments = {}
        intent_ranking = []
        product = None
        gemeente = None
        log = UtterLog(content=request.data)
        serializer = UtterRequestSerializer(data=request.data)
        if serializer.is_valid():
            # Determine the Sender ID and provided slots
            try:
                log.sender_id = serializer.validated_data['tracker']['sender_id']
            except Exception as e:
                log.sender_id = '(ongeldig)'

            # Determine if any arguments were given with the intent
            try:
                arguments = serializer.validated_data['arguments']
            except Exception as e:
                logger.error("Error parsing arguments")
                logger.exception(e)

            # Determine the intent_ranking were given with the intent
            try:
                intent_ranking = serializer.validated_data['tracker']['latest_message']['intent_ranking']
            except Exception as e:
                logger.error("Error parsing intent ranking")
                logger.exception(e)
                
            # Make use of the filled in (or empty) slots sent by Rasa
            try:
                slots = serializer.validated_data['tracker']['slots']
                product = slots.get('product', None)
                gemeente = slots.get('municipality', None)
            except Exception as e:
                logger.error("Error parsing slots")
                logger.exception(e)

            try:
                channel = serializer.validated_data['channel']['name']
            except Exception as e:
                logger.error("Error determining channel name")
                logger.exception(e)
                channel = None

            if product == '':
                product = None
            if gemeente == '':
                gemeente = None
            if channel == '':
                channel = None

            utters = UtterResponse.objects.filter(utter=serializer.validated_data['template'])
            logger.info(utters)

            if not utters:
                logger.error("No match")
                UtterResponse.objects.create(utter=serializer.validated_data['template'],
                                             response="Sorry, mijn antwoord is nog niet bekend (utter is nog niet gevuld)")
                response = APIResponse("Sorry, ik weet het even niet (utter aangemaakt)")
            else:
                # TODO: use sorted() to do this properly and avoid ugly if/else
                exact_utter = utters.filter(product__name=product,
                                            gemeente__name=gemeente,
                                            channel__name=channel)
                if exact_utter:
                    logger.info("All incl channel match")
                    selected_utter = exact_utter.order_by('?')[0]
                else:
                    product_gemeente_utter = utters.filter(product__name=product,
                                                           gemeente__name=gemeente, channel=None)
                    if product_gemeente_utter:
                        logger.info("All but channel match")
                        selected_utter = product_gemeente_utter.order_by('?')[0]

                    else:
                        gemeente_utter = utters.filter(gemeente__name=gemeente, product=None, channel=None)
                        if gemeente_utter:
                            logger.info("Gemeente match")
                            selected_utter = gemeente_utter.order_by('?')[0]
                        else:
                            logger.info("No match, checking for generic answer")
                            generic_utter = utters.filter(gemeente=None, product=None)
                            if generic_utter:
                                logger.info("Generic answer found, returning")
                                selected_utter = generic_utter.order_by('?')[0]
                            else:
                                logger.warning("Fallback answer scenario, should only occur if no generic answer configured")
                                selected_utter = utters.order_by('?')[0]


                log.template = selected_utter.response
                response = selected_utter
                logger.info(selected_utter)
                logger.info(product)
                logger.info(gemeente)
                logger.info(channel)
        else:
            logger.error("Invalid call")
            response = APIResponse("Sorry, je verzoek is niet geldig (content API weigert je invoer)")

        response_data = response.data(gemeente, product, slots, arguments, intent_ranking)
        log.response = response_data
        log.save()
        return Response(response_data)

