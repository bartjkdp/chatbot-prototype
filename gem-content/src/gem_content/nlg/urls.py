from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import UtterResponseView

urlpatterns = [
    path('', UtterResponseView.as_view()),
]
