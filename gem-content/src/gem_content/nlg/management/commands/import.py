
import os
import yaml

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Load domain.yml file to extract and import utters"

    def handle(self, filename, *args, **options):
        with open(filename, 'r') as stream:
            data = yaml.safe_load(stream)
            print(data)
