
import os
import yaml

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand

from ...models import Channel, UtterResponse

class Command(BaseCommand):
    args = 'domain.yml'
    help = "Load domain.yml file to extract and import utters"

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str)

    def parse_templates(self, templates):
        for key, value in templates.items():
            utter = key
            for utterance in value:
                if not 'text' in utterance:
                    continue
                obj, created = UtterResponse.objects.get_or_create(utter=utter,
                                                                   gemeente=None, channel=None, product=None,
                                                                   response=utterance['text'])
                if created:
                    if 'image' in utterance:
                        obj.image = utterance['image']
                    if 'channel' in utterance:
                        channel, created = Channel.objects.get_or_create(name=utterance['channel'])
                        obj.channel = channel
                obj.save()
        
    def handle(self, *args, **options):
        filename = options['filename']
        print("Parsing {} file".format(filename))
        with open(filename, 'r') as stream:
            data = yaml.safe_load(stream)
            self.parse_templates(data['templates'])
