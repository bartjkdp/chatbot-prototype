import logging

from django.db import models

from ordered_model.models import OrderedModel

logger = logging.getLogger(__name__)

class UtterLog(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    sender_id = models.CharField(max_length=255, blank=True)
    template = models.TextField(blank=True)
    content = models.TextField(blank=True)
    response = models.TextField(blank=True)

    def __str__(self):
        return str(self.created_on)

class Gemeente(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Channel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    gemeente = models.ForeignKey(Gemeente, blank=True, null=True,
                                 on_delete=models.CASCADE, help_text="Indien niet ingevuld dan geldt dit product voor alle gemeenten")
    channels = models.ManyToManyField(Channel, blank=True, null=True, help_text="Bepaalt via welke kanalen het product aangeboden wordt")
    price = models.CharField(max_length=40, blank=True, help_text='Leeg = gratis.')

    term_info = models.TextField(blank=True, help_text="Voorbeeld: De aanvraag voor een paspoort heeft een doorlooptijd van vijf werkdagen")
    term_within_before = models.CharField(max_length=40, blank=True, help_text="Voorbeeld: P5D")
    term_within_after = models.CharField(max_length=40, blank=True, help_text="Voorbeeld: P5D")

    validity_info = models.TextField(blank=True, help_text="Voorbeeld: Voor personen vanaf 18 jaar is het paspoort 10 jaar geldig.")
    validity_duration = models.CharField(max_length=40, blank=True, help_text="Voorbeeld: P10Y")

    generic_channels_apply = models.BooleanField(default=True, help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de kanalen gelijk zijn voor alle gemeenten")
    generic_price = models.BooleanField(default=True, help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de prijs gelijk is voor alle gemeenten")
    generic_validity = models.BooleanField(default=True, help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de geldigheid gelijk is voor alle gemeenten")
    generic_term = models.BooleanField(default=True, help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de algemene gegevens gelijk zijn voor alle gemeenten")
    
    def __str__(self):
        if self.gemeente:
            return "{} ({})".format(self.name, self.gemeente)
        return self.name


class Question(OrderedModel):
    question = models.TextField()
    expected_answer = models.TextField(blank=True)
    comments = models.TextField(blank=True)

    class Meta(OrderedModel.Meta):
        verbose_name = 'Vraag'
        verbose_name_plural = 'Vragen'
    
    def __str__(self):
        return self.question[:30]


class IntentLabel(models.Model):
    intent = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    gemeente = models.ForeignKey(Gemeente, blank=True, null=True,
                                 on_delete=models.CASCADE, help_text="In te vullen indien het antwoord gemeente-specifiek is")
    product = models.ForeignKey(Product, blank=True, null=True,
                                on_delete=models.CASCADE, help_text="In te vullen indien het antwoord product-specifiek is")

    def __str__(self):
        return self.label
    
class UtterResponse(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    utter = models.CharField(max_length=2550)
    response = models.TextField(help_text="Antwoord van de chatbot")
    image = models.URLField(blank=True, null=True, help_text="Verwijzing naar een afbeelding wat de chatbot doorstuurt, optioneel")
    gemeente = models.ForeignKey(Gemeente, blank=True, null=True,
                                 on_delete=models.CASCADE, help_text="In te vullen indien het antwoord gemeente-specifiek is")
    channel = models.ForeignKey(Channel, blank=True, null=True,
                                on_delete=models.CASCADE, help_text="In te vullen indien het antwoord kanaal-specifiek is")
    product = models.ForeignKey(Product, blank=True, null=True,
                                on_delete=models.CASCADE, help_text="In te vullen indien het antwoord product-specifiek is")
    custom_html_answer = models.BooleanField(default=False, help_text="Of het antwoord in HTML is opgemaakt (custom utter template)")
    type = models.CharField(max_length=50, blank=True, choices=[('', 'default'), ('ask_affirmation', 'ask_affirmation (add top 2 intents as buttons)')])

    class Meta:
        verbose_name = 'Antwoord'
        verbose_name_plural = 'Antwoorden'
    
    def __str__(self):
        return "{}: {}".format(self.utter, self.response)

    def get_button_data(self):
        buttons = list(self.buttons.values('title', 'payload'))
        return buttons

    def get_intent_label(self, intent, gemeente, product):
        """
        Based on an intent from Rasa ("product_paspoort_aanvragen"), determine the correct
        IntentLabel and return the textual representation of that label.

        If the intent is not available, create a new IntentLabel and use the intent-name as
        the default label
        """
        existing_labels = IntentLabel.objects.filter(intent=intent)

        # Gemeente+product-specific check
        gem_prod_label = existing_labels.filter(gemeente=gemeente, product=product)
        if gem_prod_label:
            return gem_prod_label[0].label

        # Gemeente-specific check
        gem_label = existing_labels.filter(gemeente=gemeente)
        if gem_label:
            return gem_label[0].label

        # Product-specific label check
        prod_label = existing_labels.filter(product=product)
        if prod_label:
            return prod_label[0].label

        # No specific label found
        intent_label, created = IntentLabel.objects.get_or_create(intent=intent, defaults={'label': intent})
        return intent_label.label

    def get_affirmation_buttons(self, intent_ranking, gemeente, product):
        """
        Based on the intent_ranking from Rasa, return one or two possible intent-buttons with a sufficient confidence level.
        """
        if len(intent_ranking) > 1:
            diff_intent_confidence = intent_ranking[0].get(
                "confidence"
            ) - intent_ranking[1].get("confidence")
            if diff_intent_confidence < 0.2:
                intent_ranking = intent_ranking[:2]
            else:
                intent_ranking = intent_ranking[:1]

        first_intent_names = [
            intent.get("name", "")
            for intent in intent_ranking
            if intent.get("name", "") != "out_of_scope"
        ]
        
        #entities = tracker.latest_message.get("entities", [])
        #entities = {e["entity"]: e["value"] for e in entities}
        #logger.info(entities)
        entities_json = {} #json.dumps(entities)

        buttons = []
        buttons_test = buttons
        for intent in first_intent_names:
            logger.info(intent)
            buttons.append(
                {
                    "title": "{}".format(self.get_intent_label(intent, gemeente, product)),
                    "payload": "/{}".format(intent) # entities_json?
                }
            )

        buttons.append({"title": "Iets anders", "payload": "/negative"})
        return buttons

    def fill_in_slots(self, text, slots):
        """
        Based on a textual response, fill in any slots (or arguments) we have received from Rasa.
        Ignore any empty slots.

        Eg.:

        Wil je je {product} aanvragen?

        with slots: {product: 'verhuizing'} will be converted to:

        Wil je je verhuizing aanvragen?
        """
        if not slots:
            return text
        for key, value in slots.items():
            if key and value:
                if isinstance(value, list):
                    value = value[0]
                if not isinstance(value, str):
                    continue
                text = text.replace("{%s}" % key, value)
        return text
    
    def data(self, gemeente=None, product=None, slots=None, arguments=None, intent_ranking=None):
        if self.custom_html_answer:
            return {"custom": {"html": self.fill_in_slots(self.fill_in_slots(self.response, slots), arguments)}}

        buttons = self.get_button_data()
        if self.type == 'ask_affirmation':
            buttons = self.get_affirmation_buttons(intent_ranking, gemeente, product)
        
        response = {"text": self.fill_in_slots(self.fill_in_slots(self.response, slots), arguments),
                    "buttons": buttons,
                    "image": self.image,
                    "elements": [],
                    "attachments": []}
        if gemeente:
            response['gemeente'] = gemeente
        return response

class UtterResponseButton(models.Model):
    response = models.ForeignKey(UtterResponse, related_name='buttons', on_delete=models.CASCADE)
    title = models.CharField(max_length=2550, help_text="Titel van de knop, bijvoorbeeld: ja dat is het!")
    payload = models.CharField(max_length=2550, help_text="Rasa payload, bijvoorbeeld: /affirmative of /cancel")

    class Meta:
        verbose_name = 'Antwoord knop'
        verbose_name_plural = 'Antwoord knoppen'
    
    def __str__(self):
        return "{} - {} -> {}".format(self.response.utter, self.title, self.payload)

