# Generated by Django 2.2.4 on 2019-08-20 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nlg', '0003_auto_20190820_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='utterresponse',
            name='image',
            field=models.URLField(blank=True, null=True),
        ),
    ]
