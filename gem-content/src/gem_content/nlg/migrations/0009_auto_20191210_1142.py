# Generated by Django 2.2.4 on 2019-12-10 10:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nlg', '0008_utterresponse_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ('order',), 'verbose_name': 'Vraag', 'verbose_name_plural': 'Vragen'},
        ),
        migrations.AlterModelOptions(
            name='utterresponse',
            options={'verbose_name': 'Antwoord', 'verbose_name_plural': 'Antwoorden'},
        ),
        migrations.AlterModelOptions(
            name='utterresponsebutton',
            options={'verbose_name': 'Antwoord knop', 'verbose_name_plural': 'Antwoord knoppen'},
        ),
        migrations.RemoveField(
            model_name='question',
            name='utter',
        ),
        migrations.AddField(
            model_name='question',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=1, editable=False, verbose_name='order'),
            preserve_default=False,
        ),
    ]
