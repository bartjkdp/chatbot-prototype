#!/bin/bash

# Load up .env
set -o allexport; source .env; set +o allexport 
# Start Rasa in Interactive training mode
echo To start interactive training make sure you "run_action_server.sh"
docker run -it -p 5006:5006 -v $(pwd)/training:/app -v $(pwd)/models:/app/models rasa/rasa:$RASA_VERSION interactive --debug --endpoints endpoints-interactive.yml --model models --data data/stories
