rem Builds Rasa SDK with requests library using the Dockerfile
docker build tilburg-action-server --no-cache --tag registry.gitlab.com/gemeente-tilburg/chatbot-prototype/tilburg-action-server:latest