Chatbot Prototype
=================

[![pipeline status](https://gitlab.com/gemeente-tilburg/chatbot-prototype/badges/master/pipeline.svg)](https://gitlab.com/gemeente-tilburg/chatbot-prototype/commits/master)

# Requirements

* Docker
* git

# Notes

* All scripts used in development environments are also available in .bat versions for Windows.

# Rasa
## Setting up environment
### Create a connection between rasa and rasax containers

    docker network create my-rasa-network

### Get all the files needed to run Rasa with the following command:

    git clone https://gitlab.com/gemeente-tilburg/chatbot-prototype.git
    
### First step is to train RASA core and NLU, using the following command:
    
    ./train.sh
    
### Build action server (one time action): 
    
    ./build_tilburg-action-server.sh
    
### Extend you hostsfile

Mac OS:

     sudo vi /private/etc/hosts
    
Windows:

     notepad c:\Windows\System32\Drivers\etc\hosts
     
and add the following:

    #Chatbot-prototype
    127.0.0.1       utrecht.chatbot.local
    ::1             utrecht.chatbot.local
    127.0.0.1       tilburg.chatbot.local
    ::1             tilburg.chatbot.local
    127.0.0.1       dongen.chatbot.local
    ::1             dongen.chatbot.local
    127.0.0.1       chatbot.local
    ::1             chatbot.local
    127.0.0.1       digid.local
    ::1             digid.local

### Setting up .env 

    touch .env
    
Then you your favorite editor to insert the following content into the file:

    BAG_API_KEY=
    RASA_VERSION=1.3.6
    #ENDPOINTS_FILE=endpoints-nlg.yml

## Starting Rasa 
### Then you can start Rasa using, this starts docker using docker-compse.yml:

    ./run.sh
  
or if you want to run it as a server (daemon)

    ./run_server.sh
    
### Then open a browser and request http://localhost

    http://localhost
    
or one of the above hostnames added to the hostfile

    http://hostname
    
## Stopping Rasa
To stop Rasa
 
    Press CTRL+C
    
or if you are running in server mode, this can also be used when CTRL+C doesn't shows an ERROR
  
    ./stop_server.sh
    
    
## Interactive training
    
First run action server in separate shell:

    ./run_action_server.sh

Then start interactive training in another shell:

    ./train_interactive.sh
    
And finally open a browser and open:
    
    http://localhost:5006/visualization.html


## Enable/disabled NLG 
FIXIT: there is also a endpoints-gemcontent.yml and endpoints-simple.yml which need to be explained here.

If you want to use the NLG / gem-content for utters edit your .env file and enable the following line by removed in the #:

    ENDPOINTS_FILE=endpoints-nlg.yml
    
Then restart Rasa.


# How to use GIT

## Committing changes

    git add .               // commit current folder 
    git add %folder% 
    git add %filename%
    git add -a              // commit all
    git commit -m "comment"
  
Or the hard way using VI:

    i // switch vi to insert mode
    [type comment]
    [ESC]
    :x // quit VI

    git push

## Undo commit

    git reset HEAD %filename%
    git reset HEAD %folder%
    git reset HEAD wildcard

## Getting changes

    git checkout -- <file> // undo changes
    git pull

## Merging changes

    git commit -m "comment"
    git pull
    git merge // This also commits if automerge works

## Ignore some folders 

Some files or folders like logs/cache/tmp you don't want in your repository. There are a few options:

* Place a .gitignore file in a folder containing "*" to ignore all files in that folder.
* Place a .gitignore file in the parent folder containing "foldername" to ignore the folder completly.
* Place a .gitignore file in the project root if you want to handle everything from a single file.

Example:

    cd <path-to-img-directory>
    git rm * // remove any files if they have already been added in GIT.
    echo "*" >> .gitignore
    git add .gitignore
    git commit -m "you message"


# Docker
## Mangage Docker environment

To remove all containers and images you can resize (make smaller) the Disc Image.
    
    docker images // view downloaded images
    docker rmi <image id> // remove image

## Build custom action server: 
    
    ./build_tilburg-action-server.sh

Dockerfile:

    # Extend the official Rasa Core SDK image
    FROM rasa/rasa_core_sdk:latest

    # Add a custom system library (e.g. git)
    RUN apt-get update && \
        apt-get install -y git python3-requests
        
    RUN pip install requests isodate
    
## Push custom action server
FIXIT: Why and how?

    ./push_tilburg-action-server.sh

## Restart action server:
You can restart only the action server from a second console if you only changed some Python code and you don't want to restart everything using the following:

    ./restart_tilburg-action-server.sh
    
## Restarting any container manually:
    
    docker-compose restart <containername from docker-compose.yml>
    
    docker-compose restart rasa
    docker-compose restart action_server
    docker-compose restart web

## Viewing the log from an specific container:

    docker-compose logs <containername from docker-compose.yml>
    
    docker-compose logs rasa
    docker-compose logs action_server
    docker-compose logs web
    
    
# Rasa X
FIXIT: Move this bit to rasax/README.md

Creating user:

    python rasax/rasa_x_commands.py create admin me pass
    
Running Rasa X:

     ./rasax/run.sh
     
Bypass missing username field on login by passing login data in get request : http://localhost:81/login?username=me&password=pass //


# Database

To check what is happening in the database you have multiple options, one of them is using the command line interface that's included in the docker container:

## Existing dev DB

To use the db-rasa.tar.gz PostgreSQL files, unpack with:

     tar zxvf db-rasa.tar.gz

After doing a docker-compose up the gem-content interface is available via:

     http://localhost:82/admin (login: alex / alex)

## Accessing docker container shell:

     docker exec -oit [container_name] bash
     
## Containernames can be found using the following command

     docker ps
     
## Run CLI

     psql -U rasa
     [password]
     
## Some handy command for Postgres
* \l - List databases
* \c [database] - Connect to a [database]
* \dt - Display tables in current database

## Some handy SQL examples for Postgres
* select * from story;
* update story set filename = 'data/stories.md';
     
## Exiting

    exit 
    exit
    
