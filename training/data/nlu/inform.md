## intent:inform
- [Tilburg](municipality)
- [tilburg](municipality:Tilburg)
- Gemeente [Tilburg](municipality)
- [Utrecht](municipality)
- Gemeente [Rotterdam](municipality)
- Dat is [Dongen](municipality)
- Voor [Amsterdam](municipality)
- In [Ede](municipality)
- [almere](municipality:Almere)
- [12345678](bsn)
- [853539528](bsn)
- [74524255](bsn)
- Mijn BSN is [34567890](bsn)
- Mijn BSN is [424208021](bsn)
- Mijn Sofinummer is [128135330](bsn)
- [1234 AB](postcode) 
- Mijn postcode is [4253ab](postcode)
- Dat is [5143 KK](postcode)
- [2553 wg](postcode)
- [5353zf](postcode)
- [9452 zf](postcode)
- dat is [5853 ft](postcode)
- [123](huisnummer)
- [9](huisnummer)
- [432](huisnummer)
- [43a](huisnummer)
- [12](huisnummer)
- [6b](huisnummer)
- [5036br](postcode) [92](huisnummer)
- Postcode [2345CD](postcode) Huisnummer [3](huisnummer)


## regex:postcode
- [1-9][0-9]{3}[\s]?[A-Za-z]{2}

## regex:huisnummer
- [1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?

