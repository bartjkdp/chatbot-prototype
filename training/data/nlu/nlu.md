## intent:affirmative
- ja
- ja, graag
- graag
- aub
- yes please!
- ok
- akkoord
- prima
- dat zou mooi zijn
- helemaal geweldig
- alsjeblieft

## intent:ask_price
- Wat kost het?
- Wat zijn de tarieven?
- Wat is het tarief voor een [parkeervergunning](product)
- Wat kost een [paspoort](product)
- Wat is de prijs van een [parkeervergunning](product) in [Tilburg](municipality)
- Hoeveel kost een [id kaart](product) in [Rotterdam](municipality)
- Wat kost een [geboorteakte](product) in [Utrecht](municipality)

## intent:ask_product_info
- Wat is een [paspoort](product)
- Wat heb je nodig voor een [rijbewijs](product)

## intent:ask_product_processed
- Is mijn [verhuismelding](product) al verwerkt?
- Hoe hoor ik of mijn [verhuizing](product:verhuismelding) is verwerkt?

## intent:ask_product_validity
- Hoe lang is mijn [paspoort](product) geldig?
- Tot wanneer is mijn [paspoort](product) geldig?

## intent:ask_openinghours
- Wat zijn de openingstijden van de stadswinkel Udenhout
- Wat zijn de openingstijden van de gemeente udenhout
- wat zijn de openingstijden van de stadswinkel Berkel-Enschot
- Wat zijn de openingstijden van de stadswinkel Berkel
- wanneer kan ik terecht op de mileustraat?
- wat zijn de openingstijden van de mileustraat?

## intent:ask_directeurvdp
- Wie is de directeur van de VDP?
- wie is de directeur van VDP?
- wie is de directeur van de vdp?
- directeur VDP
- Directuer vdp

## intent:ask_burgemeestertilburg
- Wie is de burgemeester in Tilburg?
- Burgemeerster Tilburg?
- wie is de burgermeester van Tilburg?
- wie is de burgermeester van tilburg?
- wie is de burgervader van Tilburg?
- wie is de voorzitten van de gemeenteraad in tilburg?
- Wie is de baas van de politie in Tilburg?
- Wie is de baas van de politie in Dongen?

## intent:ask_burgemeesterdongen
- Wie is de burgemeester in Dongen?
- Wie is de burgemeester van Dongen?
- Weet jij wie de burgemeester is in Dongen
- wie is de burgervader van dongen?
- wie is de voorzitten van de gemeenteraad in dongen?
- wie zit de gemeenteraad voor in Dongen?

## intent:ask_burgemeesterutrecht
- Wie is de burgemeester in Utrecht?
- Wie is de burgemeester in utrecht
- Wie is de burgemeester van utrecht?
- Weet jij wie de burgemeester is in Utrecht
- wie is de burgervader van utrecht?
- wie is de voorzitten van de gemeenteraad in utrecht?
- Wie is de baas van de politie in Utrecht?

## intent:ask_weather
- wat voor weer is het?
- gaat het regenen?
- Lekker weertje hè?
- Mooi weer buiten
- weet je wat voor weer het wordt?
- wat is de weersvoorspelling?
- wat voor weer is het daar?
- regent het in [Tilburg](municipality)?

## intent:chitchat
- Borrelen?
- Ga je mee naar de Efteling?
- kom we gaan naar de stad
- ik wil naar de kermis

## intent:goodbye
- doei
- houdoe
- fijne dag
- vaarwel
- tot ziens
- 👋
- :wave:

## intent:thanks
- Bedankt!
- bedankt
- Super!
- dank je
- dank u
- dank u vriendelijk
- enorm bedankt!
- fijn, dankje
- bedankt he
- bedankt voor de hulp

## intent:greet
- hallo [Tilburg](municipality)!
- Hallo [Utrecht](municipality)! Mijn naam is [Piet](name)
- Hoi mijn naam is [Wim](name)
- Hi, ik ben [Harvey](name)
- Hallo hier [Ali](name)
- Hey, ik ben [Jeroen](name)
- Hallo, [Emma](name) hier!
- Hallo [Ede](municipality), ik ben [Wendy](name)
- Hallo
- hoi
- hi
- goede morgen
- goede middag
- goedenavond
- goedenavond [Utrecht](municipality)
- Hoi [Dongen](municipality)
- Hoi [Uden](municipality)

## intent:negative
- nee
- nee laat maar
- alsjeblieft niet
- nee bedankt
- hoeft niet
- is goed zo
- het is genoeg
- genoeg

## intent:cancel
- annuleren
- stop
- ik wil stoppen
- ik wil afbreken aub
- kunnen we stoppen?

## intent:dontknow
- dat weet ik niet
- geen idee
- ?
- zou het niet weten
- daar vraag je me wat
- Joost mag het weten

## intent:restart
- Opnieuw
- Herstart
- Reboot
- Ik wil opnieuw beginnen
- Hoe start ik opnieuw?
- opnieuw invoeren

## intent:ask_for_human
- Ik wil met een medewerker spreken
- Ga weg robot
- Ga weg Gem
- ga weg gem
- ik wil een mens spreken
- kan ik een mensen spreken
- kan ik een echt gesprek hebben
- Ik wil een mens aan de lijn
- Verbind me even door met een echt mens
- Ga weg Gem
- echt mens aub
- ik praat niet met chatbots
- Ik praat niet met bots
- Ik praat niet met robots
- praat met medewerker
- ik wil iemand spreken van burgerzaken
- Ik wil met een mens chatten

## intent:roll_dice
- heb je een dobbelsteen?
- kun je een dobbelsteen rollen
- dobbelen
- dobbelsteen

## intent:tell_joke
- vertel een grap
- vertel een mop
- maak een grapje
- weet je een mop?

## intent:unfriendly
- klote bot
- stomme bot
- stomme chatbot
- klote chatbot
- dit werkt voor geen meter!
- rot op
- fuck you

## intent:you_too
- jij ook
- hetzelfde
- eensgelijks
- van hetzelfde
- jij ook een fijne dag nog

## synonym:Almere
- almere

## synonym:Utrecht
- utrecht

## synonym:Dongen
- dongen
- 's Gravenmoer
- Vaart
- Klein Dongen
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer

## synonym:Tilburg
- tilburg
- udenhout
- berkel
- enschot
- Berkel-Enschot
- Biezenmortel
- biezenmortel
- Udenhout

## synonym:hondenbelasting
- hond

## regex:bsn
- [0-9]{8}
- [0-9]{9}

## lookup:municipality
  data/municipalities.txt
  
## lookup:bot_name
- Gem
- gem
- Tilly
- tilly
- Joost
- joost
- Jan
- jan
