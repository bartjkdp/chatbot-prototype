## tell_me_joke three times
* tell_joke
  - action_joke
  - utter_another_joke
* affirmative
  - action_joke
  - utter_another_joke
* affirmative
  - action_joke
  - utter_another_joke
* negative
  - utter_helpmore
  - action_listen
  
  
## tell_me_joke twice
* tell_joke
  - action_joke
  - utter_another_joke
* affirmative
  - action_joke
  - utter_another_joke
* negative
  - utter_helpmore
  - action_listen
  
## tell_me_joke once
* tell_joke
  - action_joke
  - utter_another_joke
* negative
  - utter_helpmore
  - action_listen
  