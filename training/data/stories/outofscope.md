## fallback story
* out_of_scope
  - action_default_fallback
  - action_listen
  
## affirmation
* negative
  - action_default_ask_affirmation
* negative
  - utter_ask_rephrase
  
## escalate to live chat
* ask_for_human
  - action_talk_to_human
  

  